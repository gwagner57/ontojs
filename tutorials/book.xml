<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.oasis-open.org/docbook/xml/5.0/rng/docbookxi.rng" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="http://docbook.org/xml/5.0/rng/docbook.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<book xmlns="http://docbook.org/ns/docbook" xmlns:xi="http://www.w3.org/2001/XInclude"
 xmlns:xlink="http://www.w3.org/1999/xlink" version="5.0" xmlns:svg="http://www.w3.org/2000/svg"
 xml:lang="en">
 <!--	include single root element or several elements wrapped in a html element
 <?dbhtml-include href="mycode.html"?>
-->
 <info>
  <title>Building Web Apps with mODELvIEWjs</title>
  <subtitle>An incremental in-depth tutorial about building front-end and distributed web
   applications with the model-based JavaScript framework mODELvIEWjs</subtitle>
  <titleabbrev>mODELvIEWjs Web Apps</titleabbrev>
  <authorgroup>
   <author>
    <personname><firstname>Gerd</firstname><surname>Wagner</surname></personname>
    <email>G.Wagner@b-tu.de</email>
   </author>
  </authorgroup>
  <org>
   <orgname>Chair of Internet Technology, Brandenburg University of Technology</orgname>
   <address><city>Cottbus</city><street>Walther-Pauer Street 2</street><postcode>03046</postcode><country>Germany</country></address>
  </org>
  <pubdate>2015-10-01</pubdate>
  <releaseinfo>Warning: This tutorial book is a draft version, so it may still contain errors and
   may still be incomplete in certain respects. Please report any issue to
   G.Wagner@b-tu.de.</releaseinfo>
  <releaseinfo>This tutorial book is available as an open access online book (HTML) and as an e-book
   (PDF) via its <link xlink:href="http://web-engineering.info/JsFrontendApp-Book">book
    webpage</link>. </releaseinfo>
  <copyright>
   <year>2014-2015</year>
   <holder>Gerd Wagner</holder>
  </copyright>
  <legalnotice>
   <para>This book, along with any associated source code, is licensed under <link
     xlink:href="http://www.codeproject.com/info/cpol10.aspx">The Code Project Open License
     (CPOL)</link>, implying that the associated code is provided "as-is", can be modified to create
    derivative works, can be redistributed, and can be used in commercial applications, but the book
    must not be distributed or republished without the author's consent.</para>
  </legalnotice>
  <revhistory>
   <revision>
    <date>20151001</date>
    <authorinitials>gw</authorinitials>
    <revremark>Create first version.</revremark>
   </revision>
  </revhistory>
  <cover>
   <para role="tagline">Understanding and Implementing Information Management Concepts and
    Techniques</para>
   <mediaobject>
    <imageobject>
     <imagedata fileref="../fig/machine-gears.svg" scale="80">
      <info>
       <legalnotice>
        <para>Image made by Lorc under CC BY 3.0 (available on http://game-icons.net).</para>
       </legalnotice>
      </info>
     </imagedata>
    </imageobject>
   </mediaobject>
  </cover>
 </info>
 <preface>
  <title>Foreword</title>
  <para>This tutorial book shows how to build front-end and distributed web applications with the
   model-based JavaScript framework mODELvIEWjs. It follows a "learning by doing" approach, which
   means that you don't have to read lots of text, but rather you just read the minimum needed for
   starting to code your first app. By learning from the examples provided in the book, you can
   quickly improve your understanding of basic concepts and techniques, and by downloading the
   complete code of these example apps and using it as a starting point for your own projects, you
   learn best practices and experience the joy of quickly building something that really
   works.</para>
  <sidebar>
   <para>Do not use third party libraries or frameworks such as jQuery or Angular. They are normally
    not worth the time and effort you would need for getting familiar with them. They impose their
    own technical concepts on JavaScript and require you to learn their peculiar technical jargon.
    You don't need their black-box functionality. They just prevent you from learning how to do it
    yourself and how to master JavaScript.</para>
  </sidebar>
  <section>
   <title>Why is JavaScript a Good Choice for Building Web Apps?</title>
   <para>Today, JavaScript is not just a programming language, but rather a platform, which offers
    many advantages over other platforms: <orderedlist>
     <listitem>
      <para>It's the only language that enjoys native support in web browsers.</para>
     </listitem>
     <listitem>
      <para>It's the only language that allows building web apps with just one programming language.
       All other languages (like Java and C#) can only be used on the back-end and need to be
       combined with front-end JavaScript, so developers need to master at least two programming
       languages.</para>
     </listitem>
     <listitem>
      <para>It's the only language that allows executing the same code (e.g., for constraint
       validation) on the back-end and the front-end.</para>
     </listitem>
     <listitem>
      <para>It's the only language that allows dynamic distribution, that is, executing the same
       code (e.g., for business computations) either in the back-end or the front-end, depending on
       run-time conditions such as the available front-end resources.</para>
     </listitem>
     <listitem>
      <para>It combines object-oriented with functional programming.</para>
     </listitem>
     <listitem>
      <para>Its dynamism allows various forms of meta-programming, which means it gives you the
       liberty to program your own programming concepts like classes and enumerations.</para>
     </listitem>
    </orderedlist></para>
  </section>
  <section>
   <title>Run the Apps and Get Their Code</title>
   <para>For running any of the six example apps from our web server, and for getting their code,
    please visit <link xlink:href="http://web-engineering.info/JsFrontendApp-CodeLinks">
     http://web-engineering.info/JsFrontendApp-CodeLinks</link>. </para>
  </section>
 </preface>
 <part>
  <title>Getting Started</title>
  <partintro>
   <para>In this first part of the book we summarize the web's foundations and show how to build a
    front-end web application using plain JavaScript and the Local Storage API. It shows how to
    build such an app with minimal effort, not using any (third-party) framework or library. A
    front-end web app can be provided by any web server, but it is executed on the user's computer
    device (smartphone, tablet or notebook), and not on the remote web server. Typically, but not
    necessarily, a front-end web app is a single-user application, which is not shared with other
    users.</para>
   <para>The minimal version of a JavaScript front-end data management application discussed in this
    tutorial only includes a minimum of the overall functionality required for a complete app. It
    takes care of only one object type ("books") and supports the four standard data management
    operations (<emphasis role="bold">C</emphasis>reate/<emphasis role="bold"
     >R</emphasis>ead/<emphasis role="bold">U</emphasis>pdate/<emphasis role="bold"
    >D</emphasis>elete), but it needs to be enhanced by styling the user interface with CSS rules,
    and by adding further important parts of the app's overall functionality.</para>
  </partintro>
  <xi:include href="../01-GetStarted/Foundations.xml">
   <xi:fallback>
    <emphasis>MISSING XINCLUDE CONTENT</emphasis>
   </xi:fallback>
  </xi:include>
  <chapter>
   <title>More on JavaScript</title>
   <xi:include href="../01-GetStarted/JavaScript-Basics.xml">
    <xi:fallback>
     <emphasis>MISSING XINCLUDE CONTENT</emphasis>
    </xi:fallback>
   </xi:include>
   <xi:include href="../01-GetStarted/JavaScript-LocalStorage.xml">
    <xi:fallback>
     <emphasis>MISSING XINCLUDE CONTENT</emphasis>
    </xi:fallback>
   </xi:include>
  </chapter>
  <xi:include href="../01-GetStarted/PlainJavaScript.xml">
   <xi:fallback>
    <emphasis>MISSING XINCLUDE CONTENT</emphasis>
   </xi:fallback>
  </xi:include>
  <xi:include href="../01-GetStarted/PlainJavaScript-PracticeProjects-Tutorial.xml">
   <xi:fallback>
    <emphasis>MISSING XINCLUDE CONTENT</emphasis>
   </xi:fallback>
  </xi:include>
 </part>
 <part>
  <title>Integrity Constraints</title>
  <partintro>
   <para>For catching various cases of flawed data, we need to define suitable integrity constraints
    that can be used by the application's <emphasis role="bold"><emphasis role="italic">data
      validation</emphasis></emphasis> mechanisms. Integrity constraints may take many different
    forms. The most important type of integrity constraints are <emphasis role="bold"><emphasis
      role="italic">property constraints</emphasis></emphasis>, which define conditions on the
    admissible property values of an object of a certain type.</para>
  </partintro>
  <xi:include href="../02-IntegrityConstraints/Concepts.xml">
   <xi:fallback>
    <emphasis>MISSING XINCLUDE CONTENT</emphasis>
   </xi:fallback>
  </xi:include>
  <xi:include href="../02-IntegrityConstraints/PlainJavaScript.xml">
   <xi:fallback>
    <emphasis>MISSING XINCLUDE CONTENT</emphasis>
   </xi:fallback>
  </xi:include>
  <xi:include href="../02-IntegrityConstraints/PlainJavaScript-PracticeProjects-Tutorial.xml">
   <xi:fallback>
    <emphasis>MISSING XINCLUDE CONTENT</emphasis>
   </xi:fallback>
  </xi:include>
 </part>
 <part>
  <title>Enumerations</title>
  <partintro>
   <para>In virtually any app, we need to deal with string-valued attributes with a fixed set of
    possible string values. These attributes are called <emphasis role="bold"><emphasis
      role="italic">enumeration attributes</emphasis></emphasis>, and the fixed value sets defining
    their possible string values are called <emphasis role="bold"><emphasis role="italic"
      >enumerations</emphasis></emphasis>.</para>
  </partintro>
  <xi:include href="../03-Enumerations/Enumerations.xml">
   <xi:fallback>
    <emphasis>MISSING XINCLUDE CONTENT</emphasis>
   </xi:fallback>
  </xi:include>
  <xi:include href="../03-Enumerations/PlainJavaScript.xml">
   <xi:fallback>
    <emphasis>MISSING XINCLUDE CONTENT</emphasis>
   </xi:fallback>
  </xi:include>
  <xi:include href="../03-Enumerations/PlainJavaScript-PracticeProjects-Tutorial.xml">
   <xi:fallback>
    <emphasis>MISSING XINCLUDE CONTENT</emphasis>
   </xi:fallback>
  </xi:include>
 </part>
 <part>
  <title>Associations</title>
  <partintro>
   <para>Associations are important elements of information models. Software applications have to
    implement them in a proper way, typically as part of their <emphasis role="italic"
     >model</emphasis> layer within a <emphasis role="italic">model-view-controller</emphasis> (MVC)
    architecture. Unfortunately, application development frameworks do often not provide much
    support for dealing with associations.</para>
  </partintro>
  <xi:include href="../04-Associations/UnidirectionalAssociations.xml">
   <xi:fallback>
    <emphasis>MISSING XINCLUDE CONTENT</emphasis>
   </xi:fallback>
  </xi:include>
  <xi:include href="../04-Associations/PlainJS-UnidirFuncAssoc.xml">
   <xi:fallback>
    <emphasis>MISSING XINCLUDE CONTENT</emphasis>
   </xi:fallback>
  </xi:include>
  <xi:include href="../04-Associations/PlainJS-UnidirNonFuncAssoc.xml">
   <xi:fallback>
    <emphasis>MISSING XINCLUDE CONTENT</emphasis>
   </xi:fallback>
  </xi:include>
  <xi:include href="../04-Associations/BidirectionalAssociations.xml">
   <xi:fallback>
    <emphasis>MISSING XINCLUDE CONTENT</emphasis>
   </xi:fallback>
  </xi:include>
  <xi:include href="../04-Associations/PlainJS-BidirAssoc.xml">
   <xi:fallback>
    <emphasis>MISSING XINCLUDE CONTENT</emphasis>
   </xi:fallback>
  </xi:include>
  <xi:include href="../04-Associations/PartWholeAssociations.xml">
   <xi:fallback>
    <emphasis>MISSING XINCLUDE CONTENT</emphasis>
   </xi:fallback>
  </xi:include>
 </part>
 <part>
  <title>Inheritance in Class Hierarchies</title>
  <partintro>
   <para>Subtypes and inheritance are important elements of information models. Software
    applications have to implement them in a proper way, typically as part of their <emphasis
     role="italic">model</emphasis> layer within a <emphasis role="italic"
     >model-view-controller</emphasis> (MVC) architecture. Unfortunately, application development
    frameworks do often not provide much support for dealing with subtypes and inheritance.</para>
  </partintro>
  <xi:include href="../05-Subtyping/Subtypes.xml">
   <xi:fallback>
    <emphasis>MISSING XINCLUDE CONTENT</emphasis>
   </xi:fallback>
  </xi:include>
  <xi:include href="../05-Subtyping/PlainJavaScript.xml">
   <xi:fallback>
    <emphasis>MISSING XINCLUDE CONTENT</emphasis>
   </xi:fallback>
  </xi:include>
 </part>
 <part>
  <title>Model-Based App Development</title>
  <partintro>
   <para>In this last part we show how to avoid repetitive code structures ("boilerplate code") by
    using the model-based development framework mODELcLASSjs, which has been developed as a result
    of analyzing the boilerplate code patterns of the six plain JS example apps discussed in this
    book.</para>
  </partintro>
  <xi:include href="../0x-mODELcLASSjs/mODELcLASS.xml">
   <xi:fallback>
    <emphasis>MISSING XINCLUDE CONTENT</emphasis>
   </xi:fallback>
  </xi:include>
  <xi:include href="../0x-mODELcLASSjs/ValidationWithMODELcLASS.xml">
   <xi:fallback>
    <emphasis>MISSING XINCLUDE CONTENT</emphasis>
   </xi:fallback>
  </xi:include>
  <!--	 
		<chapter><title>Associations and Subtyping with mODELcLASS</title>
			<para>Coming Soon!</para>
		</chapter>
-->
 </part>
 <xi:include href="../Glossary.xml">
  <xi:fallback>
   <emphasis>MISSING XINCLUDE CONTENT</emphasis>
  </xi:fallback>
 </xi:include>
 <?dbhtml-include href="GoogleWebsiteTrackingScript.html"?>
</book>
