# mODELcLASSjs #

***mODELcLASSjs*** can be used for the model layer of a MVC JavaScript web app, which may be built, for instance, with the help of a view-oriented library such as *AngularJS*, *KnockoutJS* or *ReactJS*. It provides a meta-class for creating JavaScript model classes (like `Book`, `Author` and `Publisher`) with declarative constraint validation, storage management, and the option of multiple inheritance and object pools. See [this tutorial](http://web-engineering.info/JsFrontendApp/mODELcLASS-validation-tutorial.html) for more. 

mODELcLASSjs is based on the following libraries and metaclasses of the oNTOjs framework: errorTypes.js, util.js, dom.js, xhr.js, eNUMERATION.js, eNTITYtYPE.js, sTORAGEmANAGER.js and mODELcLASS.js.

Currently, the following storage technologies are supported by mODELcLASSjs: 

1. local storage with the Local Storage API, 
2. remote storage with the Parse.com cloud storage service via XHR, 
3. remote storage with MariaDB/MySQL/NodeJS via XHR. 

Support of local storage with IndexedDB as well as support for other storage technologies and object pools will come soon.

### Contribution guidelines ###

If you would like to join this project and help, e.g., with writing tests, please contact G.Wagner (at) b-tu.de