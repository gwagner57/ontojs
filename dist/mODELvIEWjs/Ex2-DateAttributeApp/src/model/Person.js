/**
 * @fileOverview  The model class Person with attribute definitions
 * @author Gerd Wagner
 * @copyright Copyright 2014-2015 Gerd Wagner, Chair of Internet Technology, Brandenburg University of Technology, Germany.
 * @license This code is licensed under The Code Project Open License (CPOL), implying that the code is provided "as-is", 
 * can be modified to create derivative works, can be redistributed, and can be used in commercial applications.
 */
/**
 * Model class Person 
 * @class
 */
try {

  Person = new mODELcLASS({
    name: "Person",
    properties: {
      "personId": {range:"NonNegativeInteger", isStandardId: true, label:"Person ID"},
      "name": {range:"NonEmptyString", min: 2, max: 20, label:"Name"},
      "birthDate": {range: "Date", label:"Date of birth"},
      "deathDate": {range: "Date", optional: true, label:"Date of death"}
    },
    //attributesToDisplayInLists: ["lastName","firstName"],
  });

} catch (e) {
  console.log( e.constructor.name +": "+ e.message);
}
