/**
 * @fileOverview  App-level controller code
 * @author Gerd Wagner
 */
// main namespace and subnamespace definitions
var pl = { model:{}, view:{}, ctrl:{}};
var $appNS = pl;

pl.ctrl = {
  title: "Example App 5-0: Unidirectional Associations",
  storageManager: new sTORAGEmANAGER(),  // define a localStorage manager
  validateOnInput: false
};

pl.ctrl.createTestData = function() {
  try {
    pl.ctrl.storageManager.add( Publisher, {name:"Bantam Books", address:"New York, USA"});
    pl.ctrl.storageManager.add( Publisher, {name:"Basic Books", address:"New York, USA"});
    pl.ctrl.storageManager.add( Author, {authorId: 1, name:"Daniel Dennett"});
    pl.ctrl.storageManager.add( Author, {authorId: 2, name:"Douglas Hofstadter"});
    pl.ctrl.storageManager.add( Author, {authorId: 3, name:"Immanuel Kant"});
    pl.ctrl.storageManager.add( Book,{isbn:"0553345842", title:"The Mind's I", year:1982,
        authors:[1,2], publisher:"Bantam Books"});
    pl.ctrl.storageManager.add( Book,{isbn:"1463794762", title:"The Critique of Pure Reason",
        authors:[3], year:2011});
    pl.ctrl.storageManager.add( Book,{isbn:"1928565379", title:"The Critique of Practical Reason",
        authors:[3], year:2009});
    pl.ctrl.storageManager.add( Book,{isbn:"0465030793", title:"I Am A Strange Loop",
        year:2000, authors:[2], publisher:"Basic Books"});
  } catch (e) {
    if (e.constructor.name) {
      console.log( e.constructor.name + ": " + e.message);
    } else {
      console.log( e.message);
    }
  }
};
