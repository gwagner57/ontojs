/**
 * @fileOverview  The model class Publisher with attribute definitions
 * @author Gerd Wagner
 * @copyright Copyright 2015 Gerd Wagner, Chair of Internet Technology, Brandenburg University of Technology, Germany.
 * @license This code is licensed under The Code Project Open License (CPOL), implying that the code is provided "as-is", 
 * can be modified to create derivative works, can be redistributed, and can be used in commercial applications.
 */
/**
 * Model class Publisher
 * @class
 */
var Publisher = null;
try {
  Publisher = new mODELcLASS({
    name: "Publisher",
    properties: {
      "name": {range:"NonEmptyString", isStandardId: true, min: 2, max: 20, label:"Name"},
      "address": {range:"NonEmptyString", min: 5, max: 50, label:"Address"}
    }
    //attributesToDisplayInLists: ["lastName","firstName"],
  });

} catch (e) {
  console.log( e.constructor.name +": "+ e.message);
}
