/**
 * @fileOverview  The model class Author with attribute definitions
 * @author Gerd Wagner
 * @copyright Copyright 2013-2014 Gerd Wagner, Chair of Internet Technology, Brandenburg University of Technology, Germany.
 * @license This code is licensed under The Code Project Open License (CPOL), implying that the code is provided "as-is", 
 * can be modified to create derivative works, can be redistributed, and can be used in commercial applications.
 */
/**
 * Model class Author 
 * @class
 */
var Author = null;
try {
  Author = new mODELcLASS({
    name: "Author",
    properties: {
      "authorId": {range:"NonNegativeInteger", isStandardId: true, label:"Author ID"},
      "name": {range:"NonEmptyString", min: 2, max: 20, label:"Name"}
    }
  });
} catch (e) {
  console.log( e.constructor.name +": "+ e.message);
}
