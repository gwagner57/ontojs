/**
 * @fileOverview  The model class Book with attribute definitions
 * @author Gerd Wagner
 * @copyright Copyright 2015 Gerd Wagner, Chair of Internet Technology, Brandenburg University of Technology, Germany.
 * @license This code is licensed under The Code Project Open License (CPOL), implying that the code is provided "as-is", 
 * can be modified to create derivative works, can be redistributed, and can be used in commercial applications.
 */
/**
 * Object type Book 
 * @class
 */
Book = new mODELcLASS({
  name: "Book",
  properties: {
    "isbn": {range:"NonEmptyString", isStandardId: true, label:"ISBN"},
    "title": {range:"NonEmptyString", min: 2, max: 50, label:"Title"},
    "year": {range:"Integer", min: 1459, max: util.nextYear(), label:"Year"},
    "publisher": {range: Publisher, optional: true, label:"Publisher"},
    "authors": {range: Author, minCard: 1, maxCard: Infinity, label:"Authors"}
  },
  attributesToDisplayInLists: ["title"]
});
