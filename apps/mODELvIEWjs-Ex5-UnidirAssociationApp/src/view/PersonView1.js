try { 
  
  PersonView1 = new vIEWcLASS({
    modelClass: Person,
    fields: ["personId",
             ["firstName","lastName"],
             ["birthDate","deathDate"]
    ]
  });

} catch (e) {
  console.log( e.constructor.name +": "+ e.message);
}
