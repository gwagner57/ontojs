CREATE TABLE IF NOT EXISTS persons (
  person_id INT NOT NULL PRIMARY KEY,
  name VARCHAR(20) NOT NULL,
  gender ENUM('male', 'female', 'undetermined') NOT NULL,
  favorite_month ENUM('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'),
  music_preferences SET('Jazz','Classical','Rock','Country','Pop')
);
SET SESSION sql_mode='STRICT_ALL_TABLES';

DELIMITER //
CREATE TRIGGER checkConstraints
  BEFORE INSERT ON persons
  FOR EACH ROW
BEGIN
  IF NEW.person_id < 1 THEN  
    SIGNAL SQLSTATE '22003' SET MESSAGE_TEXT = 'person_id must be a positive integer value';
  END IF;
  IF length(NEW.name) < 2 OR length(NEW.name) > 20 THEN  
    SIGNAL SQLSTATE '22026' SET MESSAGE_TEXT = 'name length must have between 2 and 20 chars';
  END IF;
  IF length(NEW.gender) < 1 THEN  
    SIGNAL SQLSTATE '22026' SET MESSAGE_TEXT = 'gender cannot be empty';
  END IF;
END; //
DELIMITER ;