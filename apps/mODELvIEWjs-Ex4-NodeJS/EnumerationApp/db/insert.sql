INSERT INTO persons( person_id, name, gender, favorite_month, music_preferences) VALUES
  ( 1, 'John Wayne', 'male', 'Jan', 'Jazz,Country'),
  ( 2, 'Elly Walker', 'female', NULL, NULL),
  ( 3, 'Teo Emmer', 'male', 'Aug', 'Pop');      