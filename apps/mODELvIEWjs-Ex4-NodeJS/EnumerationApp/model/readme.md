This folder contains all the model JavaScript classes.
As an example, lets consider the Book model class which is supposed to 
use MariaDB as storage solution and define a set of properties as follows:

var Book = new mODELcLASS({
  typeName: "Book",
  properties: {
    "isbn": { range:"NonEmptyString", isStandardId: true, label:"ISBN", pattern:/\b\d{9}(\d|X)\b/, 
        patternMessage:'The ISBN must be a 10-digit string or a 9-digit string followed by "X"!'},
    "title": { range:"NonEmptyString", min: 2, max: 50, label:"Title"}, 
    "year": { range:"Integer", min: 1459, max: util.nextYear(), label:"Year"},
    "edition": { range:"PositiveInteger", optional: true, label:"Edition"}
  }
});