const app = require( './app'),
      router = require( './router'),
      express = require( 'express')(),
      bodyParser = require( 'body-parser');
      
/*********************************************************
 * NOTE: 'app' can be used to access the model classes,  *
 * e.g., app.Book, core classes, e.g., app.mODELcLASS    *
 * or utilities/libraries, e.g., util.getTableName().    *
 *********************************************************/ 
 
// parsing application/json data for POST and PUT
express.use( bodyParser.json()); 
// parsing application/x-www-form-urlencoded for POST and PUT
express.use( bodyParser.urlencoded( { extended: true })); 
 
// create default routes for application model classes
router.createDefaultRoutes( express);

/***********************************************************
 * Additional/custom routes can be created and added here. *
 *                ADD YOUR CUSTOM CODE HERE                *
 ***********************************************************/

// Web Application starts listen on port 3000.
// NOTE: change this to whatever needed port
express.listen( 3000);