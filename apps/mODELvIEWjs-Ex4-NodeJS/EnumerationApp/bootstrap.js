// $appNS, $appNS.model, $appNS.view and $appNS.ctrl are fixed names
// and changing them may cause the application to run incorrect!
var $appNS = { model:{}, view:{}, ctrl:{}};
$appNS.ctrl = {
  // use this flag to enable/disable HTML5 form input auto-validation feature
  validateOnInput: false,
  // optional, used to define the app path (e.g., /nodeapps/myApp), used with proxied app paths
  appPath: "",
  // define the storage manager for this application
  storageManager: new sTORAGEmANAGER( 
    { name: "MariaDB", // required
      host: 'localhost', // optional host, defaults to localhost
      port: 3306, // optional DB engine port, defaults to 3306
      user: 'enumerationapp', // required
      database: 'enumerationapp', // required
      password: 'enumerationapp' // required
    })
   /** additional/custom code comes here, e.g., the createTestData method, etcetera **/
};
/** The following code is injected by the gulp script (create-app task)**/
/** NOTE: changing this code may cause the application to run incorrect! **/
/** Initialize model vies **/
if ( typeof exports != "object") {
  window.addEventListener( "load", mODELvIEW.setupUI);
  /** title creation code: added at application creation time by the gulp script **/
  $appNS.ctrl.title = "EnumerationApp";
  document.title = $appNS.ctrl.title;
} else {
  exports.$appNS = $appNS;
}
/************* End: gulp injected code (create-app task) *************/