CREATE TABLE IF NOT EXISTS books (
  isbn CHAR(10) NOT NULL PRIMARY KEY,
  title VARCHAR(50) NOT NULL,
  year SMALLINT NOT NULL,
  edition TINYINT DEFAULT 1
);

DELIMITER //
CREATE TRIGGER checkConstraints
  BEFORE INSERT ON books
  FOR EACH ROW
BEGIN
  IF (NEW.isbn REGEXP '\\b\\d{9}(\\d|X)\\b') = 0  THEN  
    SIGNAL SQLSTATE '40002' SET MESSAGE_TEXT = 'isbn must be a 10 digits or nine digits followed by an X value';
  END IF;
  IF NEW.title = '' THEN  
    SIGNAL SQLSTATE '22026' SET MESSAGE_TEXT = 'empty title not allowed';
  END IF;
  IF NEW.year < 1454 OR NEW.year > (YEAR( CURDATE()) + 1) THEN 
    SIGNAL SQLSTATE '22003' SET MESSAGE_TEXT = 'year value must be in the range [1454, nextYear]';
  END IF;
  IF NEW.edition <= 0 THEN 
    SIGNAL SQLSTATE '22003' SET MESSAGE_TEXT = 'edition must be a positive integer value';
  END IF;
END; //
DELIMITER ;