
var mariaDBConnector = require( "mysql");/* jshint browser: true */
'use strict';
/**
 * Implements the trim function for browsers 
 * that don't support it natively
 */
if (!String.prototype.trim) {  
  String.prototype.trim = function () {  
    return this.replace(/^\s+|\s+$/g,'');  
  };  
}
/**
 * Implement some ECMASCRIPT6 methods for browsers 
 * that don't support them natively
 */
if (!Number.isInteger) {
  Number.isInteger = function isInteger (nVal) {
    return typeof nVal === "number" && isFinite(nVal) && 
        nVal > -9007199254740992 && nVal < 9007199254740992 && 
        Math.floor(nVal) === nVal;
  };
}
if (!String.prototype.includes ) {
  String.prototype.includes = function () {
    return String.prototype.indexOf.apply( this, arguments ) !== -1;
  };
}
if (!Array.prototype.includes ) {
  Array.prototype.includes = function () {
    return Array.prototype.indexOf.apply( this, arguments ) !== -1;
  };
}
/**
 * Compute the max/min of an array
 * Notice that apply requires a context object, which is not really used
 * in the case of a static function such as Math.max
 */
Array.max = function (array) {
  return Math.max.apply( Math, array);
}; 
Array.min = function (array) {
  return Math.min.apply( Math, array);
};
/**
 * Clone an array
 */
Array.prototype.clone = function () {
  return this.slice(0);
}; 
/**
 * Test if an array is equal to another
 */
Array.prototype.isEqualTo = function (a2) {
  return (this.length === a2.length) && this.every( function( el, i) {
    return el === a2[i]; });
};
/**
 * Return an array of the values of an object
 */
Object.values = function (obj) {
  return Object.keys(obj).map( function (key) {
      return obj[key];
    });
};

/**
 * @fileOverview  A library of utility methods.
 * @author Gerd Wagner
 * @copyright Copyright 2015 Gerd Wagner, Chair of Internet Technology,
 *   Brandenburg University of Technology, Germany.
 * @license The MIT License (MIT)
 */
var util = {
  languages: { "de":"Deutsch", "en":"English", "es":"Español", "fr":"Français", 
    "pt":"Português", "ru":"Русский", "zh":"中文", "ro":"Romanian"
  },
  /** REGEX to check if valid JS identifier **/
  identifierPattern: /^(?!(?:do|if|in|for|let|new|try|var|case|else|enum|eval|false|null|this|true|void|with|break|catch|class|const|super|throw|while|yield|delete|export|import|public|return|static|switch|typeof|default|extends|finally|package|private|continue|debugger|function|arguments|interface|protected|implements|instanceof)$)[$A-Z\_a-z\xaa\xb5\xba\xc0-\xd6\xd8-\xf6\xf8-\u02c1\u02c6-\u02d1\u02e0-\u02e4\u02ec\u02ee\u0370-\u0374\u0376\u0377\u037a-\u037d\u0386\u0388-\u038a\u038c\u038e-\u03a1\u03a3-\u03f5\u03f7-\u0481\u048a-\u0527\u0531-\u0556\u0559\u0561-\u0587\u05d0-\u05ea\u05f0-\u05f2\u0620-\u064a\u066e\u066f\u0671-\u06d3\u06d5\u06e5\u06e6\u06ee\u06ef\u06fa-\u06fc\u06ff\u0710\u0712-\u072f\u074d-\u07a5\u07b1\u07ca-\u07ea\u07f4\u07f5\u07fa\u0800-\u0815\u081a\u0824\u0828\u0840-\u0858\u08a0\u08a2-\u08ac\u0904-\u0939\u093d\u0950\u0958-\u0961\u0971-\u0977\u0979-\u097f\u0985-\u098c\u098f\u0990\u0993-\u09a8\u09aa-\u09b0\u09b2\u09b6-\u09b9\u09bd\u09ce\u09dc\u09dd\u09df-\u09e1\u09f0\u09f1\u0a05-\u0a0a\u0a0f\u0a10\u0a13-\u0a28\u0a2a-\u0a30\u0a32\u0a33\u0a35\u0a36\u0a38\u0a39\u0a59-\u0a5c\u0a5e\u0a72-\u0a74\u0a85-\u0a8d\u0a8f-\u0a91\u0a93-\u0aa8\u0aaa-\u0ab0\u0ab2\u0ab3\u0ab5-\u0ab9\u0abd\u0ad0\u0ae0\u0ae1\u0b05-\u0b0c\u0b0f\u0b10\u0b13-\u0b28\u0b2a-\u0b30\u0b32\u0b33\u0b35-\u0b39\u0b3d\u0b5c\u0b5d\u0b5f-\u0b61\u0b71\u0b83\u0b85-\u0b8a\u0b8e-\u0b90\u0b92-\u0b95\u0b99\u0b9a\u0b9c\u0b9e\u0b9f\u0ba3\u0ba4\u0ba8-\u0baa\u0bae-\u0bb9\u0bd0\u0c05-\u0c0c\u0c0e-\u0c10\u0c12-\u0c28\u0c2a-\u0c33\u0c35-\u0c39\u0c3d\u0c58\u0c59\u0c60\u0c61\u0c85-\u0c8c\u0c8e-\u0c90\u0c92-\u0ca8\u0caa-\u0cb3\u0cb5-\u0cb9\u0cbd\u0cde\u0ce0\u0ce1\u0cf1\u0cf2\u0d05-\u0d0c\u0d0e-\u0d10\u0d12-\u0d3a\u0d3d\u0d4e\u0d60\u0d61\u0d7a-\u0d7f\u0d85-\u0d96\u0d9a-\u0db1\u0db3-\u0dbb\u0dbd\u0dc0-\u0dc6\u0e01-\u0e30\u0e32\u0e33\u0e40-\u0e46\u0e81\u0e82\u0e84\u0e87\u0e88\u0e8a\u0e8d\u0e94-\u0e97\u0e99-\u0e9f\u0ea1-\u0ea3\u0ea5\u0ea7\u0eaa\u0eab\u0ead-\u0eb0\u0eb2\u0eb3\u0ebd\u0ec0-\u0ec4\u0ec6\u0edc-\u0edf\u0f00\u0f40-\u0f47\u0f49-\u0f6c\u0f88-\u0f8c\u1000-\u102a\u103f\u1050-\u1055\u105a-\u105d\u1061\u1065\u1066\u106e-\u1070\u1075-\u1081\u108e\u10a0-\u10c5\u10c7\u10cd\u10d0-\u10fa\u10fc-\u1248\u124a-\u124d\u1250-\u1256\u1258\u125a-\u125d\u1260-\u1288\u128a-\u128d\u1290-\u12b0\u12b2-\u12b5\u12b8-\u12be\u12c0\u12c2-\u12c5\u12c8-\u12d6\u12d8-\u1310\u1312-\u1315\u1318-\u135a\u1380-\u138f\u13a0-\u13f4\u1401-\u166c\u166f-\u167f\u1681-\u169a\u16a0-\u16ea\u16ee-\u16f0\u1700-\u170c\u170e-\u1711\u1720-\u1731\u1740-\u1751\u1760-\u176c\u176e-\u1770\u1780-\u17b3\u17d7\u17dc\u1820-\u1877\u1880-\u18a8\u18aa\u18b0-\u18f5\u1900-\u191c\u1950-\u196d\u1970-\u1974\u1980-\u19ab\u19c1-\u19c7\u1a00-\u1a16\u1a20-\u1a54\u1aa7\u1b05-\u1b33\u1b45-\u1b4b\u1b83-\u1ba0\u1bae\u1baf\u1bba-\u1be5\u1c00-\u1c23\u1c4d-\u1c4f\u1c5a-\u1c7d\u1ce9-\u1cec\u1cee-\u1cf1\u1cf5\u1cf6\u1d00-\u1dbf\u1e00-\u1f15\u1f18-\u1f1d\u1f20-\u1f45\u1f48-\u1f4d\u1f50-\u1f57\u1f59\u1f5b\u1f5d\u1f5f-\u1f7d\u1f80-\u1fb4\u1fb6-\u1fbc\u1fbe\u1fc2-\u1fc4\u1fc6-\u1fcc\u1fd0-\u1fd3\u1fd6-\u1fdb\u1fe0-\u1fec\u1ff2-\u1ff4\u1ff6-\u1ffc\u2071\u207f\u2090-\u209c\u2102\u2107\u210a-\u2113\u2115\u2119-\u211d\u2124\u2126\u2128\u212a-\u212d\u212f-\u2139\u213c-\u213f\u2145-\u2149\u214e\u2160-\u2188\u2c00-\u2c2e\u2c30-\u2c5e\u2c60-\u2ce4\u2ceb-\u2cee\u2cf2\u2cf3\u2d00-\u2d25\u2d27\u2d2d\u2d30-\u2d67\u2d6f\u2d80-\u2d96\u2da0-\u2da6\u2da8-\u2dae\u2db0-\u2db6\u2db8-\u2dbe\u2dc0-\u2dc6\u2dc8-\u2dce\u2dd0-\u2dd6\u2dd8-\u2dde\u2e2f\u3005-\u3007\u3021-\u3029\u3031-\u3035\u3038-\u303c\u3041-\u3096\u309d-\u309f\u30a1-\u30fa\u30fc-\u30ff\u3105-\u312d\u3131-\u318e\u31a0-\u31ba\u31f0-\u31ff\u3400-\u4db5\u4e00-\u9fcc\ua000-\ua48c\ua4d0-\ua4fd\ua500-\ua60c\ua610-\ua61f\ua62a\ua62b\ua640-\ua66e\ua67f-\ua697\ua6a0-\ua6ef\ua717-\ua71f\ua722-\ua788\ua78b-\ua78e\ua790-\ua793\ua7a0-\ua7aa\ua7f8-\ua801\ua803-\ua805\ua807-\ua80a\ua80c-\ua822\ua840-\ua873\ua882-\ua8b3\ua8f2-\ua8f7\ua8fb\ua90a-\ua925\ua930-\ua946\ua960-\ua97c\ua984-\ua9b2\ua9cf\uaa00-\uaa28\uaa40-\uaa42\uaa44-\uaa4b\uaa60-\uaa76\uaa7a\uaa80-\uaaaf\uaab1\uaab5\uaab6\uaab9-\uaabd\uaac0\uaac2\uaadb-\uaadd\uaae0-\uaaea\uaaf2-\uaaf4\uab01-\uab06\uab09-\uab0e\uab11-\uab16\uab20-\uab26\uab28-\uab2e\uabc0-\uabe2\uac00-\ud7a3\ud7b0-\ud7c6\ud7cb-\ud7fb\uf900-\ufa6d\ufa70-\ufad9\ufb00-\ufb06\ufb13-\ufb17\ufb1d\ufb1f-\ufb28\ufb2a-\ufb36\ufb38-\ufb3c\ufb3e\ufb40\ufb41\ufb43\ufb44\ufb46-\ufbb1\ufbd3-\ufd3d\ufd50-\ufd8f\ufd92-\ufdc7\ufdf0-\ufdfb\ufe70-\ufe74\ufe76-\ufefc\uff21-\uff3a\uff41-\uff5a\uff66-\uffbe\uffc2-\uffc7\uffca-\uffcf\uffd2-\uffd7\uffda-\uffdc][$A-Z\_a-z\xaa\xb5\xba\xc0-\xd6\xd8-\xf6\xf8-\u02c1\u02c6-\u02d1\u02e0-\u02e4\u02ec\u02ee\u0370-\u0374\u0376\u0377\u037a-\u037d\u0386\u0388-\u038a\u038c\u038e-\u03a1\u03a3-\u03f5\u03f7-\u0481\u048a-\u0527\u0531-\u0556\u0559\u0561-\u0587\u05d0-\u05ea\u05f0-\u05f2\u0620-\u064a\u066e\u066f\u0671-\u06d3\u06d5\u06e5\u06e6\u06ee\u06ef\u06fa-\u06fc\u06ff\u0710\u0712-\u072f\u074d-\u07a5\u07b1\u07ca-\u07ea\u07f4\u07f5\u07fa\u0800-\u0815\u081a\u0824\u0828\u0840-\u0858\u08a0\u08a2-\u08ac\u0904-\u0939\u093d\u0950\u0958-\u0961\u0971-\u0977\u0979-\u097f\u0985-\u098c\u098f\u0990\u0993-\u09a8\u09aa-\u09b0\u09b2\u09b6-\u09b9\u09bd\u09ce\u09dc\u09dd\u09df-\u09e1\u09f0\u09f1\u0a05-\u0a0a\u0a0f\u0a10\u0a13-\u0a28\u0a2a-\u0a30\u0a32\u0a33\u0a35\u0a36\u0a38\u0a39\u0a59-\u0a5c\u0a5e\u0a72-\u0a74\u0a85-\u0a8d\u0a8f-\u0a91\u0a93-\u0aa8\u0aaa-\u0ab0\u0ab2\u0ab3\u0ab5-\u0ab9\u0abd\u0ad0\u0ae0\u0ae1\u0b05-\u0b0c\u0b0f\u0b10\u0b13-\u0b28\u0b2a-\u0b30\u0b32\u0b33\u0b35-\u0b39\u0b3d\u0b5c\u0b5d\u0b5f-\u0b61\u0b71\u0b83\u0b85-\u0b8a\u0b8e-\u0b90\u0b92-\u0b95\u0b99\u0b9a\u0b9c\u0b9e\u0b9f\u0ba3\u0ba4\u0ba8-\u0baa\u0bae-\u0bb9\u0bd0\u0c05-\u0c0c\u0c0e-\u0c10\u0c12-\u0c28\u0c2a-\u0c33\u0c35-\u0c39\u0c3d\u0c58\u0c59\u0c60\u0c61\u0c85-\u0c8c\u0c8e-\u0c90\u0c92-\u0ca8\u0caa-\u0cb3\u0cb5-\u0cb9\u0cbd\u0cde\u0ce0\u0ce1\u0cf1\u0cf2\u0d05-\u0d0c\u0d0e-\u0d10\u0d12-\u0d3a\u0d3d\u0d4e\u0d60\u0d61\u0d7a-\u0d7f\u0d85-\u0d96\u0d9a-\u0db1\u0db3-\u0dbb\u0dbd\u0dc0-\u0dc6\u0e01-\u0e30\u0e32\u0e33\u0e40-\u0e46\u0e81\u0e82\u0e84\u0e87\u0e88\u0e8a\u0e8d\u0e94-\u0e97\u0e99-\u0e9f\u0ea1-\u0ea3\u0ea5\u0ea7\u0eaa\u0eab\u0ead-\u0eb0\u0eb2\u0eb3\u0ebd\u0ec0-\u0ec4\u0ec6\u0edc-\u0edf\u0f00\u0f40-\u0f47\u0f49-\u0f6c\u0f88-\u0f8c\u1000-\u102a\u103f\u1050-\u1055\u105a-\u105d\u1061\u1065\u1066\u106e-\u1070\u1075-\u1081\u108e\u10a0-\u10c5\u10c7\u10cd\u10d0-\u10fa\u10fc-\u1248\u124a-\u124d\u1250-\u1256\u1258\u125a-\u125d\u1260-\u1288\u128a-\u128d\u1290-\u12b0\u12b2-\u12b5\u12b8-\u12be\u12c0\u12c2-\u12c5\u12c8-\u12d6\u12d8-\u1310\u1312-\u1315\u1318-\u135a\u1380-\u138f\u13a0-\u13f4\u1401-\u166c\u166f-\u167f\u1681-\u169a\u16a0-\u16ea\u16ee-\u16f0\u1700-\u170c\u170e-\u1711\u1720-\u1731\u1740-\u1751\u1760-\u176c\u176e-\u1770\u1780-\u17b3\u17d7\u17dc\u1820-\u1877\u1880-\u18a8\u18aa\u18b0-\u18f5\u1900-\u191c\u1950-\u196d\u1970-\u1974\u1980-\u19ab\u19c1-\u19c7\u1a00-\u1a16\u1a20-\u1a54\u1aa7\u1b05-\u1b33\u1b45-\u1b4b\u1b83-\u1ba0\u1bae\u1baf\u1bba-\u1be5\u1c00-\u1c23\u1c4d-\u1c4f\u1c5a-\u1c7d\u1ce9-\u1cec\u1cee-\u1cf1\u1cf5\u1cf6\u1d00-\u1dbf\u1e00-\u1f15\u1f18-\u1f1d\u1f20-\u1f45\u1f48-\u1f4d\u1f50-\u1f57\u1f59\u1f5b\u1f5d\u1f5f-\u1f7d\u1f80-\u1fb4\u1fb6-\u1fbc\u1fbe\u1fc2-\u1fc4\u1fc6-\u1fcc\u1fd0-\u1fd3\u1fd6-\u1fdb\u1fe0-\u1fec\u1ff2-\u1ff4\u1ff6-\u1ffc\u2071\u207f\u2090-\u209c\u2102\u2107\u210a-\u2113\u2115\u2119-\u211d\u2124\u2126\u2128\u212a-\u212d\u212f-\u2139\u213c-\u213f\u2145-\u2149\u214e\u2160-\u2188\u2c00-\u2c2e\u2c30-\u2c5e\u2c60-\u2ce4\u2ceb-\u2cee\u2cf2\u2cf3\u2d00-\u2d25\u2d27\u2d2d\u2d30-\u2d67\u2d6f\u2d80-\u2d96\u2da0-\u2da6\u2da8-\u2dae\u2db0-\u2db6\u2db8-\u2dbe\u2dc0-\u2dc6\u2dc8-\u2dce\u2dd0-\u2dd6\u2dd8-\u2dde\u2e2f\u3005-\u3007\u3021-\u3029\u3031-\u3035\u3038-\u303c\u3041-\u3096\u309d-\u309f\u30a1-\u30fa\u30fc-\u30ff\u3105-\u312d\u3131-\u318e\u31a0-\u31ba\u31f0-\u31ff\u3400-\u4db5\u4e00-\u9fcc\ua000-\ua48c\ua4d0-\ua4fd\ua500-\ua60c\ua610-\ua61f\ua62a\ua62b\ua640-\ua66e\ua67f-\ua697\ua6a0-\ua6ef\ua717-\ua71f\ua722-\ua788\ua78b-\ua78e\ua790-\ua793\ua7a0-\ua7aa\ua7f8-\ua801\ua803-\ua805\ua807-\ua80a\ua80c-\ua822\ua840-\ua873\ua882-\ua8b3\ua8f2-\ua8f7\ua8fb\ua90a-\ua925\ua930-\ua946\ua960-\ua97c\ua984-\ua9b2\ua9cf\uaa00-\uaa28\uaa40-\uaa42\uaa44-\uaa4b\uaa60-\uaa76\uaa7a\uaa80-\uaaaf\uaab1\uaab5\uaab6\uaab9-\uaabd\uaac0\uaac2\uaadb-\uaadd\uaae0-\uaaea\uaaf2-\uaaf4\uab01-\uab06\uab09-\uab0e\uab11-\uab16\uab20-\uab26\uab28-\uab2e\uabc0-\uabe2\uac00-\ud7a3\ud7b0-\ud7c6\ud7cb-\ud7fb\uf900-\ufa6d\ufa70-\ufad9\ufb00-\ufb06\ufb13-\ufb17\ufb1d\ufb1f-\ufb28\ufb2a-\ufb36\ufb38-\ufb3c\ufb3e\ufb40\ufb41\ufb43\ufb44\ufb46-\ufbb1\ufbd3-\ufd3d\ufd50-\ufd8f\ufd92-\ufdc7\ufdf0-\ufdfb\ufe70-\ufe74\ufe76-\ufefc\uff21-\uff3a\uff41-\uff5a\uff66-\uffbe\uffc2-\uffc7\uffca-\uffcf\uffd2-\uffd7\uffda-\uffdc0-9\u0300-\u036f\u0483-\u0487\u0591-\u05bd\u05bf\u05c1\u05c2\u05c4\u05c5\u05c7\u0610-\u061a\u064b-\u0669\u0670\u06d6-\u06dc\u06df-\u06e4\u06e7\u06e8\u06ea-\u06ed\u06f0-\u06f9\u0711\u0730-\u074a\u07a6-\u07b0\u07c0-\u07c9\u07eb-\u07f3\u0816-\u0819\u081b-\u0823\u0825-\u0827\u0829-\u082d\u0859-\u085b\u08e4-\u08fe\u0900-\u0903\u093a-\u093c\u093e-\u094f\u0951-\u0957\u0962\u0963\u0966-\u096f\u0981-\u0983\u09bc\u09be-\u09c4\u09c7\u09c8\u09cb-\u09cd\u09d7\u09e2\u09e3\u09e6-\u09ef\u0a01-\u0a03\u0a3c\u0a3e-\u0a42\u0a47\u0a48\u0a4b-\u0a4d\u0a51\u0a66-\u0a71\u0a75\u0a81-\u0a83\u0abc\u0abe-\u0ac5\u0ac7-\u0ac9\u0acb-\u0acd\u0ae2\u0ae3\u0ae6-\u0aef\u0b01-\u0b03\u0b3c\u0b3e-\u0b44\u0b47\u0b48\u0b4b-\u0b4d\u0b56\u0b57\u0b62\u0b63\u0b66-\u0b6f\u0b82\u0bbe-\u0bc2\u0bc6-\u0bc8\u0bca-\u0bcd\u0bd7\u0be6-\u0bef\u0c01-\u0c03\u0c3e-\u0c44\u0c46-\u0c48\u0c4a-\u0c4d\u0c55\u0c56\u0c62\u0c63\u0c66-\u0c6f\u0c82\u0c83\u0cbc\u0cbe-\u0cc4\u0cc6-\u0cc8\u0cca-\u0ccd\u0cd5\u0cd6\u0ce2\u0ce3\u0ce6-\u0cef\u0d02\u0d03\u0d3e-\u0d44\u0d46-\u0d48\u0d4a-\u0d4d\u0d57\u0d62\u0d63\u0d66-\u0d6f\u0d82\u0d83\u0dca\u0dcf-\u0dd4\u0dd6\u0dd8-\u0ddf\u0df2\u0df3\u0e31\u0e34-\u0e3a\u0e47-\u0e4e\u0e50-\u0e59\u0eb1\u0eb4-\u0eb9\u0ebb\u0ebc\u0ec8-\u0ecd\u0ed0-\u0ed9\u0f18\u0f19\u0f20-\u0f29\u0f35\u0f37\u0f39\u0f3e\u0f3f\u0f71-\u0f84\u0f86\u0f87\u0f8d-\u0f97\u0f99-\u0fbc\u0fc6\u102b-\u103e\u1040-\u1049\u1056-\u1059\u105e-\u1060\u1062-\u1064\u1067-\u106d\u1071-\u1074\u1082-\u108d\u108f-\u109d\u135d-\u135f\u1712-\u1714\u1732-\u1734\u1752\u1753\u1772\u1773\u17b4-\u17d3\u17dd\u17e0-\u17e9\u180b-\u180d\u1810-\u1819\u18a9\u1920-\u192b\u1930-\u193b\u1946-\u194f\u19b0-\u19c0\u19c8\u19c9\u19d0-\u19d9\u1a17-\u1a1b\u1a55-\u1a5e\u1a60-\u1a7c\u1a7f-\u1a89\u1a90-\u1a99\u1b00-\u1b04\u1b34-\u1b44\u1b50-\u1b59\u1b6b-\u1b73\u1b80-\u1b82\u1ba1-\u1bad\u1bb0-\u1bb9\u1be6-\u1bf3\u1c24-\u1c37\u1c40-\u1c49\u1c50-\u1c59\u1cd0-\u1cd2\u1cd4-\u1ce8\u1ced\u1cf2-\u1cf4\u1dc0-\u1de6\u1dfc-\u1dff\u200c\u200d\u203f\u2040\u2054\u20d0-\u20dc\u20e1\u20e5-\u20f0\u2cef-\u2cf1\u2d7f\u2de0-\u2dff\u302a-\u302f\u3099\u309a\ua620-\ua629\ua66f\ua674-\ua67d\ua69f\ua6f0\ua6f1\ua802\ua806\ua80b\ua823-\ua827\ua880\ua881\ua8b4-\ua8c4\ua8d0-\ua8d9\ua8e0-\ua8f1\ua900-\ua909\ua926-\ua92d\ua947-\ua953\ua980-\ua983\ua9b3-\ua9c0\ua9d0-\ua9d9\uaa29-\uaa36\uaa43\uaa4c\uaa4d\uaa50-\uaa59\uaa7b\uaab0\uaab2-\uaab4\uaab7\uaab8\uaabe\uaabf\uaac1\uaaeb-\uaaef\uaaf5\uaaf6\uabe3-\uabea\uabec\uabed\uabf0-\uabf9\ufb1e\ufe00-\ufe0f\ufe20-\ufe26\ufe33\ufe34\ufe4d-\ufe4f\uff10-\uff19\uff3f]*$/,
  /**
   * Returns a random integer that is uniformly distributed between min and max
   * @param {number} min
   * @param {number} max
   * @return {number}
   */
  randomInt: function (min, max) {
    return Math.floor( Math.random() * (max - min + 1)) + min;
  },
  /**
   * Gets the user's preferred language from the browser settings 
   * @return {string}
   */
  getUserLanguage: function () {
    var lang = window.navigator.userLanguage || window.navigator.language;
    return lang.substring(0,2);
  },
 /**
  * Verifies if a value represents an integer
  * @param {number} x
  * @return {boolean}
  */
  isNonEmptyString: function (x) {
    return typeof(x) === "string" && x.trim() !== "";
  },
 /**
  * Verifies if a value represents an integer string
  * @param {string} x
  * @return {boolean}
  */
  isIntegerString: function (x) {
    return typeof(x) === "string" && x.search(/^-?[0-9]+$/) === 0;
  }
};
/**
 * Serialize a Date object as an ISO date string
 * @return  YYYY-MM-DD
 */
util.createIsoDateString = function (d) {
  return d.toISOString().substring(0,10);
};
 /**
  * Return the next year value (e.g. if now is 2013 the function will return 2014)
  * @return {number}  the integer representing the next year value
  */
util.nextYear = function () {
  var date = new Date();
  return (date.getFullYear() + 1);
}; 
/**
 * Compute the age of s.o. or s.th.
 * @param bd  the birth date (as string or date object)
 * @return {number}  The age
 */
util.getAge = function (bd) {
  var today = new Date(), age, m;
  if (typeof bd === "string") bd = new Date(bd);
  age = today.getFullYear() - bd.getFullYear();
  m = today.getMonth() - bd.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < bd.getDate())) {
    age--;
  }
  return age;
};

/**
 * Given a class name (e.g. Book) create the corresponding database 
 * table name (e.g., books) which is the lower case plural version 
 * of the used (and provided as parameter) class name.
 * @param {string} className
 * @return the corresponding table name for the given class name
 */
util.getTableName = function ( className) {
  return util.camelToUnderscore( className, true);
};

/**
 * Given a property name (e.g. dateOfBirth) create the corresponding  
 * table column name (e.g., date_of_birth) which is the underscore 
 * notation version of the property name.
 * @param {string} propertyName
 * @return the corresponding column name for the given property name
 */
util.getColumnName = function ( propertyName) {
  return util.camelToUnderscore( propertyName);
};

/**
 * Given a table name (e.g. books) create the corresponding class  
 * name (e.g., Book) which is the camel case version of the table
 * name but without the ending 's' and with first letter capitalized. 
 * @param {string} tableName
 * @return the corresponding class name for the given table name
 */
util.getClassName = function ( tableName) {
  return util.underscoreToCamel( tableName, true);
};

/**
 * Given a column name (e.g. date_of_birth) create the corresponding   
 * property name (e.g., dateOfBirth) which is the camel case version 
 * of the column name (first letter lower case also). 
 * @param {string} columnName
 * @return the corresponding property name for the given column name
 */
util.getPropertyName = function ( columnName) {
  return util.underscoreToCamel( columnName);
};

/**
 * Given a camel case notation identifier, the method
 * transform it to underscore-notation.
 * E.g.: dateOfBirth => date_of_birth, 
 *       TemperatureSensor => temperature_sensors (if isClass = true)
 * @param {string} identifier
 *                the identifier(class name, property name, etc) to transform
 * @param {boolean} isClass
 *                a flag which specify if the identifier is a class name
 *                because in this case, the underscore notation contains 
 *                the 's' char at the end.
 * @return the underscore-notation string for the identifier
 */
util.camelToUnderscore = function ( identifier, isClass) {
  var result = '';
  // null or undefined identifier
  if ( !identifier) {
    throw "util.camelToUnderscore: the 'identifier' can't be null or undefined!";
  } 
  // invalid JS identifier
  if ( !util.identifierPattern.test( identifier)) {
    throw "util.camelToUnderscore: the provided 'identifier' (" + identifier + ") is not a JS valid identifier!";   
  } 
  // if the first is a A-Z char, replace it with its lower case equivalent
  // that's much easier than to create a regular expression to consider this 
  // specific exception case (which occurs normally with class names)
  identifier = identifier.charAt( 0).toLowerCase() + identifier.slice( 1);
  // replace upper case letter with '_' followed by the lower case equivalent leter
  result = identifier.replace( /([A-Z])/g, function( $1) {
    return "_" + $1.toLowerCase();
  });
  // if is a class, add the 's' at the end
  if ( isClass === true) {
    result += 's';
  }
  return result;
};

/**
 * Given a underscore-notation identifier, the method
 * transform it to camel case notation.
 * E.g.: date_of_birth => dateOfBirth
 *       temperature_sensors => TemperatureSensor (if isClass = true)
 * @param {string} identifier
 *                the identifier to transform
 * @param {boolean} isClass
 *                a flag which specify if the identifier is a class name
 *                because in this case, the underscore notation contains 
 *                the 's' char at the end which has to be cut-out and also 
 *                the first letter has to be capitalized.
 * @return the camel case notation string for the identifier
 */
util.underscoreToCamel = function ( identifier, isClass) {
  var result = '';
  // null or undefined identifier
  if ( !identifier) {
    throw "util.underscoreToCamel: the 'identifier' can't be null or undefined!";
  } 
  // invalid JS identifier
  if ( !util.identifierPattern.test( identifier)) {
    throw "util.underscoreToCamel: the provided 'identifier' is not a JS valid identifier!";   
  } 
  // replace upper case letter with '_' followed by the lower case equivalent letter
  result = identifier.replace( /(\_[a-z])/g, function ( $1) {
    return $1.toUpperCase().replace( '_', '');
  });
  // if is a class name, delete the 's' from the end
  // and capitalize the first letter
  if ( isClass === true) {
     result = result.charAt( 0).toUpperCase() + result.slice( 1);
     // if it has an 's' at the end, then cut it out
     // this should be in general the case...but anyway check it
     if ( result.charAt( result.length - 1) === 's') { 
      result = result.slice( 0, result.length - 1);
     }
  }
  return result;
};

/**
 * Extracts the data part of an object, where the extracted property values
 * are either primitive data values, Date objects, or arrays of primitive 
 * data values. 
 * @param {object} obj
 */
util.createRecordFromObject = function (obj) {
  var record={}, p="", val;
  for (p in obj) {
    val = obj[p];
    if (obj.hasOwnProperty(p) && (typeof val === "string" ||
        typeof val === "number" || typeof val === "boolean" ||
        val instanceof Date ||
        Array.isArray( val) &&  // array list of data values
          !val.some( function (el) {
            return typeof el === "object";
          })
       )) {
      if (val instanceof Date) record[p] = val.toISOString();
      else if (Array.isArray( val)) record[p] = val.slice(0);
      else record[p] = val;
    }
  }   
  return record;
};
/**
 * Creates a "clone" of an object that is an instance of a model class
 *
 * @param {object} obj
 */
util.cloneObject = function (obj) {
  var p="", val, 
      clone = Object.create( Object.getPrototypeOf(obj));
  for (p in obj) {
    if (obj.hasOwnProperty(p)) {
      val = obj[p];
      if (typeof val === "number" ||
          typeof val === "string" ||
          typeof val === "boolean" ||
          val instanceof Date ||
          // typed object reference
          typeof val === "object" && !!val.constructor ||
          Array.isArray( val) &&  // list of data values
            !val.some( function (el) {
              return typeof el === "object";
            }) ||
          Array.isArray( val) &&  // list of typed object references
            val.every( function (el) {
              return (typeof el === "object" && !!el.constructor);
            })
          ) {
        if (Array.isArray( val)) clone[p] = val.slice(0);
        else clone[p] = val;
      }
      // else clone[p] = cloneObject(val);
    }
  }
  return clone;
};
/**
 * Copy all own (property and method) slots of a number of untyped objects 
 * to a new untyped object.
 * @author Gerd Wagner
 * @return {object}  The merge result.
 */
util.mergeObjects = function () {  
  var i=0, k=0, obj=null, mergeObj={}, keys=[], key="";
  for (i=0; i < arguments.length; i++) {
    obj = arguments[i];
    if (obj && typeof obj === "object") {
      keys = Object.keys( obj);
      for (k=0; k < keys.length; k++) {
        key = keys[k]; 
        mergeObj[key] = obj[key];
      }      
    }
  }
  return mergeObj;
};

'use strict';
/**
 * @fileOverview  Defines error classes (also called "exception" classes)
 * @author Gerd Wagner
 */

function ConstraintViolation( msg, culprit) {
  this.message = msg;
  if (culprit) this.culprit = culprit;
}
function NoConstraintViolation( v) {
  if (v !== undefined) this.checkedValue = v;
  this.message = "";
}
NoConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
NoConstraintViolation.prototype.constructor = NoConstraintViolation;

/*
 * Property Constraint Violations
 */
function MandatoryValueConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
MandatoryValueConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
MandatoryValueConstraintViolation.prototype.constructor = MandatoryValueConstraintViolation;

function RangeConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
RangeConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
RangeConstraintViolation.prototype.constructor = RangeConstraintViolation;

function StringLengthConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
StringLengthConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
StringLengthConstraintViolation.prototype.constructor = StringLengthConstraintViolation;

function IntervalConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
IntervalConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
IntervalConstraintViolation.prototype.constructor = IntervalConstraintViolation;

function PatternConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
PatternConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
PatternConstraintViolation.prototype.constructor = PatternConstraintViolation;

function UniquenessConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
UniquenessConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
UniquenessConstraintViolation.prototype.constructor = UniquenessConstraintViolation;

function CardinalityConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
CardinalityConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
CardinalityConstraintViolation.prototype.constructor = CardinalityConstraintViolation;

function ReferentialIntegrityConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
};
ReferentialIntegrityConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
ReferentialIntegrityConstraintViolation.prototype.constructor = ReferentialIntegrityConstraintViolation;

function FrozenValueConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
};
FrozenValueConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
FrozenValueConstraintViolation.prototype.constructor = FrozenValueConstraintViolation;

/*
 * Entity Type Constraint Violations
 */
function cOMPLEXtYPEconstraintViolation(msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
cOMPLEXtYPEconstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
cOMPLEXtYPEconstraintViolation.prototype.constructor = cOMPLEXtYPEconstraintViolation;

function cOMPLEXdATAtYPEconstraintViolation(msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
cOMPLEXdATAtYPEconstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
cOMPLEXdATAtYPEconstraintViolation.prototype.constructor = cOMPLEXdATAtYPEconstraintViolation;

function eNTITYtYPEconstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
eNTITYtYPEconstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
eNTITYtYPEconstraintViolation.prototype.constructor = eNTITYtYPEconstraintViolation;

function ModelClassConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
ModelClassConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
ModelClassConstraintViolation.prototype.constructor = ModelClassConstraintViolation;

function ViewConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
ViewConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
ViewConstraintViolation.prototype.constructor = ViewConstraintViolation;

function ObjectTypeConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
ObjectTypeConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
ObjectTypeConstraintViolation.prototype.constructor = ObjectTypeConstraintViolation;

function AgentTypeConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
AgentTypeConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
AgentTypeConstraintViolation.prototype.constructor = AgentTypeConstraintViolation;

function KindConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
KindConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
KindConstraintViolation.prototype.constructor = KindConstraintViolation;

function RoleConstraintViolation( msg, culprit) {
  ConstraintViolation.call( this, msg, culprit);
}
RoleConstraintViolation.prototype = Object.create( ConstraintViolation.prototype);
RoleConstraintViolation.prototype.constructor = RoleConstraintViolation;

/**
 * @fileOverview  This file contains the definition of the library class
 * sTORAGEmANAGER.
 * @author Gerd Wagner
 * @copyright Copyright 2015 Gerd Wagner, Chair of Internet Technology,
 *   Brandenburg University of Technology, Germany.
 * @license The MIT License (MIT)
 */
/**
 * Library class providing storage management methods for a number of predefined
 * storage adapters
 *
 * @constructor
 * @this {sTORAGEmANAGER}
 * @param storageAdapter: object
 */
function sTORAGEmANAGER( storageAdapter) {
  if (!storageAdapter) this.adapter = {name:"LocalStorage"};
  else if (typeof( storageAdapter) === 'object' &&
      storageAdapter.name !== undefined &&
      ["LocalStorage","ParseRestAPI","MariaDB"].includes( storageAdapter.name)) {
    this.adapter = storageAdapter;
  } else {
    throw new ConstraintViolation("Invalid storageAdapter name");
  }
  // copy storage adapter to corresponding storage management method library
  sTORAGEmANAGER.adapters[this.adapter.name].currentAdapter = storageAdapter;
}
/**
 * Generic method for loading/retrieving a model object
 * @method
 * @param {object} mc  The model class concerned
 * @param {string|number} id  The object ID value
 * @param {function} continueProcessing  The continueProcessing function
 */
sTORAGEmANAGER.prototype.retrieve = function (mc, id, continueProcessing) {
  if (typeof( continueProcessing) !== 'function') {
    throw "sTORAGEmANAGER.prototype.retrieve: " +
        "the 'continueProcessing' parameter is required!";
  }
  sTORAGEmANAGER.adapters[this.adapter.name].retrieve( mc, id, continueProcessing);
};
/**
 * Generic method for loading all table rows and converting them
 * to model objects
 *
 * @method
 * @param {object} mc  The model class concerned
 * @param {function} continueProcessing  The continueProcessing function
 */
sTORAGEmANAGER.prototype.retrieveAll = function (mc, continueProcessing) {
  mc.instances = {};  // clear main memory cache
  if (typeof( continueProcessing) !== 'function') {
    throw "sTORAGEmANAGER.prototype.retrieveAll: " +
        "the 'continueProcessing' parameter is required!";
  }
  sTORAGEmANAGER.adapters[this.adapter.name].retrieveAll( mc, continueProcessing);
};
/**
 * Generic method for creating and "persisting" new model objects
 * @method
 * @param {object} mc  The model class concerned
 * @param {object} slots  The object creation slots
 * @param {function} continueProcessing  The continueProcessing function
 */
sTORAGEmANAGER.prototype.add = function (mc, slots, continueProcessing) {
  var newObj = null;
  if (!continueProcessing) {
    continueProcessing = function (obj, error) {
      if (error) {
        if (typeof(error) === 'string') console.log( error);
        else console.log( error.constructor.name +": "+ error.message);
      } else {
        console.log("New "+ obj.toString() +" saved.");
      }
    };
  } else if (typeof( continueProcessing) !== 'function') {
    throw "sTORAGEmANAGER.prototype.add: 'continueProcessing' must be a function!";
  }
  try {
    newObj = mc.create( slots);
    if (newObj) {
      sTORAGEmANAGER.adapters[this.adapter.name].add( mc,
          slots, newObj, continueProcessing);
    }
  } catch (e) {
    if (e instanceof ConstraintViolation) {
      console.log( e.constructor.name +": "+ e.message);
    } else console.log( e);
  }
};
/**
 * Generic method for updating model objects
 * @method
 * @param {object} mc  The model class concerned
 * @param {string|number} id  The object ID value
 * @param {object} slots  The object's update slots
 * @param {function} continueProcessing  The continueProcessing function
 */
sTORAGEmANAGER.prototype.update =
    function (mc, id, slots, continueProcessing) {
  var objectBeforeUpdate = null,
      properties = mc.properties, updatedProperties=[],
      noConstraintViolated=true, adapter = this.adapter.name;
  function afterRetrieveDo (objToUpdate, error) {
    if (error) {
      if (continueProcessing) {
        continueProcessing( null, "There is no "+ mc.name +
            " with "+ mc.standardId +" "+ id +" in the database!");
      } else {
        console.log("There is no "+ mc.name +" with "+
            mc.standardId +" "+ id +" in the database!");
      }
    } else {
      // this is a special case when the object comes as serialized as for
      // example from an XHR request. In this case we create the corresponding
      // model instance from the slots ( serialized object)
      if( typeof objToUpdate === "object" && !objToUpdate.type) {
        objToUpdate = mc.convertRec2Obj( objToUpdate);
      }
      objectBeforeUpdate = util.cloneObject( objToUpdate);
      try {
        Object.keys( slots).forEach( function (attr) {
          var oldVal = objToUpdate[attr],
              newVal = slots[attr];
          if (properties[attr] && !properties[attr].isStandardId) {
            if (properties[attr].maxCard === undefined ||
                properties[attr].maxCard === 1) {  // single-valued
              if (Number.isInteger( oldVal) && newVal !== "") {
                newVal = parseInt( newVal);
              } else if (typeof( oldVal)==="number" && newVal !== "") {
                newVal = parseFloat( newVal);
              } else if (oldVal===undefined && newVal==="") {
                newVal = undefined;
              }
              if (newVal !== oldVal) {
                updatedProperties.push( attr);
                objToUpdate.set( attr, newVal);
              }
            } else {   // multi-valued
              if (oldVal.length !== newVal.length ||
                  oldVal.some( function (vi,i) { return (vi !== newVal[i]);})) {
                objToUpdate.set(attr, newVal);
                updatedProperties.push(attr);
              }
            }
          }
        });
      } catch (e) {
        console.log( e.constructor.name +": "+ e.message);
        noConstraintViolated = false;
        // restore object to its state before updating
        objToUpdate = objectBeforeUpdate;
      }
      if (noConstraintViolated) {
        if (updatedProperties.length > 0) {
          console.log( "Properties " + updatedProperties.toString() +
              " modified for "+ mc.name +" "+ id);
          sTORAGEmANAGER.adapters[adapter].update( mc, id, slots,
              objToUpdate, continueProcessing);
        } else {
          console.log("No property value changed for "+ mc.name +" "+
              id +" !");
        }
      }
    }
  }
  // first check if object exists
  this.retrieve( mc, id, afterRetrieveDo);
};
/**
 * Generic method for deleting model objects
 * @method
 * @param {object} mc  The model class concerned
 * @param {string|number} id  The object ID value
 * @param {function} continueProcessing  The continueProcessing function
 */
sTORAGEmANAGER.prototype.destroy = function (mc, id, continueProcessing) {
  var adapter = this.adapter.name;
  this.retrieve( mc, id, function( object, err){
    if (object) {
      sTORAGEmANAGER.adapters[adapter].destroy( mc, id, continueProcessing);
    } else {
      err = "There is no "+ mc.name +" with "+ mc.standardId +
          " "+ id +" in the database!";
      console.log( err);
    }
  });
};
/**
 * Generic method for clearing the database table, or object store,
 * of a model class
 * @method
 */
sTORAGEmANAGER.prototype.clearData = function (mc, continueProcessing) {
  if (typeof confirm === "function") {
    if (confirm("Do you really want to delete all data?")) {
      sTORAGEmANAGER.adapters[this.adapter.name].clearData( mc, continueProcessing);
    }
  } else sTORAGEmANAGER.adapters[this.adapter.name].clearData( mc, continueProcessing);
};

sTORAGEmANAGER.adapters = {};

/**************************************************************************
 * Storage management methods for the "LocalStorage" adapter
 **************************************************************************/
sTORAGEmANAGER.adapters["LocalStorage"] = {
  //------------------------------------------------
  retrieve: function (mc, id, continueProcessing) {
  //------------------------------------------------
    //this.retrieveAll();             //TODO: Needed?
    continueProcessing( mc.instances[id]);
  },
  //------------------------------------------------
  retrieveAll: function (modelClass, continueProcessing) {
  //------------------------------------------------
    function retrieveAll (mc) {
      var key = "", keys = [], i = 0,
          tableString = "", table={},
          tableName = util.getTableName( mc.name);
      Object.keys( mc.properties).forEach( function (p) {
        var range = mc.properties[p].range;
        if (range instanceof mODELcLASS) retrieveAll( range);
      });
      try {
        if (localStorage[tableName]) {
          tableString = localStorage[tableName];
        }
      } catch (e) {
        console.log( "Error when reading from Local Storage\n" + e);
      }
      if (tableString) {
        table = JSON.parse( tableString);
        keys = Object.keys( table);
        console.log( keys.length + " " + mc.name + " records loaded.");
        for (i=0; i < keys.length; i++) {
          key = keys[i];
          mc.instances[key] = mc.convertRec2Obj( table[key]);
        }
      }
    }
    retrieveAll( modelClass);
    continueProcessing();
  },
  //------------------------------------------------
  add: function (mc, slots, newObj, continueProcessing) {
  //------------------------------------------------
    mc.instances[newObj[mc.standardId]] = newObj;
    this.saveAll( mc);
    //console.log( newObj.toString() + " created!");
    continueProcessing( newObj, null);  // no error
  },
  //------------------------------------------------
  /***** newObj includes validated update slots *****/
  update: function (mc, id, slots, newObj, continueProcessing) {
  //------------------------------------------------
    this.saveAll( mc);  //TODO: save only on leaving page or closing browser tab/window
  },
  //------------------------------------------------
  destroy: function (mc, id, continueProcessing) {
  //------------------------------------------------
    delete mc.instances[id];
    this.saveAll( mc);  //TODO: save only on app exit
  },
  //------------------------------------------------
  clearData: function (mc, continueProcessing) {
    //------------------------------------------------
    var tableName = util.getTableName( mc.name);
    mc.instances = {};
    try {
      localStorage[tableName] = JSON.stringify({});
      console.log("Table "+ tableName +" cleared.");
    } catch (e) {
      console.log("Error when writing to Local Storage\n" + e);
    }
  },
  //------------------------------------------------
  saveAll: function (mc) {
  //------------------------------------------------
    var id="", table={}, rec={}, obj=null, i=0,
        keys = Object.keys( mc.instances),
        tableName = util.getTableName( mc.name);
    // convert to a 'table' as a map of entity records
    for (i=0; i < keys.length; i++) {
      id = keys[i];
      obj = mc.instances[id];
      table[id] = obj.toRecord();
    }
    try {
      localStorage[tableName] = JSON.stringify( table);
      console.log( keys.length +" "+ mc.name +" records saved.");
    } catch (e) {
      console.log("Error when writing to Local Storage\n" + e);
    }
  }
};
sTORAGEmANAGER.adapters["MariaDB"] = {
  pool: null,
  /**
   * Get the connection with the database (async method).
   * @method
   * @param {function} callback The function that is invoked after
   * the async operation has completed.
   */
  getConnection: function ( callback) {
    var sqlStatement = '', storageAdapter = this.currentAdapter;
    if ( !this.pool) {
      this.pool = mariaDBConnector.createPool({
        host     : this.currentAdapter.host || 'localhost',
        port     : this.port || 3306,
        user     : this.currentAdapter.user,
        password : this.currentAdapter.password
      });
    } 
    this.pool.getConnection( function ( err, connection) {
      if ( err) {
        callback( err, null);
      } else {
        sqlStatement = 'USE ' + storageAdapter.database + ';';
        connection.query( sqlStatement, function ( err) {
          if ( err) {
            callback( err, null);
          } else {
            callback( null, connection);
          }
        });
      }
    });
  },
  
  /**
   * Helper method which transforms the DB enum values into 
   * values required by the eNUMERATION.
   * @param enumClass 
   *    the specific eNUMERATION class
   * @param labels 
   *    the DB enumeration literals to be converted
   * @param multivalued 
   *    flag that specifies if the property is multivalued or not
   */
  convertDBEnumToJSEnum: function (enumClass, literals, multivalued) {
    var tLits = null, indexes = [];
    if (!literals) return multivalued ? [] : undefined;
    // multivalued
    if (multivalued) {
      tLits = literals.split(',');
      tLits.forEach( function (lit) {
        indexes.push(enumClass[lit.toUpperCase()]);
      });
      return indexes;
    } 
    // single value
    else {
      return enumClass[literals.toUpperCase()];
    }
  },
  
   /**
   * Helper method which transforms the eNUMERATION enum 
   * values into DB enum values.
   * @param enumClass 
   *    the specific eNUMERATION class
   * @param ordinals 
   *    the JS enumeration ordinals to be converted
   * @param multivalued 
   *    flag that specifies if the property is multivalued or not
   */
  convertJSEnumToDBEnum: function (enumClass, ordinals, multivalued) {
    var tLits = null, litListAsString = "";
    if (!ordinals) return null;
    if (Array.isArray(ordinals) && ordinals.length < 1) return null;
    // multivalued
    if (multivalued) {
      litListAsString = enumClass.enumIndexesToNames(ordinals);
      litListAsString = litListAsString.replace(/ /g,'');
      return litListAsString;
    } else {
      return enumClass.enumLitNames[ordinals - 1];
    }
  },

  /**
   * Generic method for loading/retrieving a model object
   * @method
   * @param {object} mc  The model class concerned
   * @param {string|number} id  The object ID value
   * @param {function} continueProcessing  The function that is invoked after
   * the async operation has completed. Its first parameter is the resulting data,
   * if any, and its second optional parameter is an error message/object.
   */
  //------------------------------------------------
  retrieve: function ( mc, id, continueProcessing) {
  //------------------------------------------------
    var tableName = util.getTableName( mc.name),
      stdid = mc.standardId, url = '', 
      appPath = $appNS && $appNS.ctrl && $appNS.ctrl.appPath ? $appNS.ctrl.appPath : '';
    if ( id === null || id === undefined) {
      throw "sTORAGEmANAGER['MariaDB'].retrieve: undefined or null object ID!";
    }
    // client side: make GET request
    if ( typeof exports !== 'object') {
      if ( typeof continueProcessing !== 'function') {
        continueProcessing = function ( obj, error) {};
      }
      url = window.location.protocol + '//' + window.location.hostname;
      if ( window.location.port > 0 && window.location.port !== 80) {    
        url += ':' + window.location.port;
      }
      url += appPath;
      xhr.GET({
        url: url + '/' + tableName + '/' + id,
        handleResponse: function ( r) {
          var response = null, obj = null;
          try {
            if ( r.status === 200 || r.status === 201 || r.status === 304) {
              response = JSON.parse( r.responseText);
              obj = mc.convertRec2Obj( response);
              continueProcessing( obj, null);
            } else {
              console.log( "Error " + r.status + ": " + r.statusText);
              continueProcessing( null, "Error " + r.status + ": " + r.statusText);
            }
          } catch ( e) {
            console.log( e);
            throw "sTORAGEmANAGER['MariaDB'].retrieve: fatal exception " 
              + "occurred during the 'retrieve' MariaDB request!";
          } 
        }
      });
    } 
    // server side: make MariaDB query
    else {
      // makes no sense to execute SQL statements if is nothing to do with the response...
      if ( typeof continueProcessing === 'function') {
        this.getConnection( function ( err, dbConnection) {
          var sqlStatement = '';
          if ( err) {
            continueProcessing( null, err);
          } else {
            sqlStatement = "SELECT * FROM " + tableName + " WHERE `" + util.getColumnName( stdid) + "` = ?;";
            dbConnection.query( sqlStatement, [id], function ( err, rows) {
              var row = {}, p = null, pName = "", multivalued = false, enumIndexes = null;
              if ( err) {
                continueProcessing( null, err);
              } else {
                try {
                  if ( rows.length > 0) {
                    // convert property names from underscore notation to CamelCase notation
                    Object.keys( rows[0]).forEach( function( pName) { 
                      row[util.getPropertyName( pName)] = rows[0][pName];
                    });
                     // take care of enumeration conversion
                    for ( pName in mc.properties) {
                      p = mc.properties[pName];
                      multivalued = p.maxCard > 1 || p.minCard > 1;
                      enumIndexes = null;
                      if (p.range.enumLitNames) {
                        enumIndexes = sTORAGEmANAGER.adapters["MariaDB"]
                          .convertDBEnumToJSEnum(p.range, row[pName], multivalued);
                        row[pName] = enumIndexes;
                      }
                    };
                    // *** Enumeration Properties Parse END ***/
                    continueProcessing( row, null);
                  } else {
                    continueProcessing( null, null);
                  }
                } catch ( e) {
                  continueProcessing( null, e);
                }
              }
            });
          }
        });
      }
    }
  },
 /**
  * Generic method for loading/retrieving all instances for a model object
  * @method
  * @param {object} mc  The model class concerned
  * @param {function} continueProcessing  The function that is invoked after
  * the async operation has completed. Its first parameter is the resulting data,
  * if any, and its second optional parameter is an error message/object.
  */
  //------------------------------------------------
  retrieveAll: function ( mc, continueProcessing) {
  //------------------------------------------------
    var tableName = util.getTableName( mc.name),
      stdid = mc.standardId, url = '', 
      appPath = $appNS && $appNS.ctrl && $appNS.ctrl.appPath ? $appNS.ctrl.appPath : '';
    // client side: make GET request
    if ( typeof exports !== 'object') {
      if ( typeof continueProcessing !== 'function') {
        continueProcessing = function ( obj, error) {};
      }
      url = window.location.protocol + '//' + window.location.hostname;
      if ( window.location.port > 0 && window.location.port !== 80) {    
        url += ':' + window.location.port;
      }
      url += appPath;
      xhr.GET({
        url: url + '/' + tableName,
        handleResponse: function ( r) {
          var response = null, objs = [], p = null, 
              pName = "", multivalued = false, enumIndexes = null;
          try {
            if ( r.status === 200 || r.status === 201 || r.status === 304) {
              response = JSON.parse( r.responseText);
              for ( i = 0; i < response.length; i++) {
                // take care of enumeration conversion
                for ( pName in mc.properties) {
                  p = mc.properties[pName];
                  multivalued = p.maxCard > 1 || p.minCard > 1;
                  enumIndexes = null;
                  if (p.range.enumLitNames) {
                    enumIndexes = sTORAGEmANAGER.adapters["MariaDB"]
                      .convertDBEnumToJSEnum(p.range, response[i][pName], multivalued);
                    response[i][pName] = enumIndexes;
                  }
                };
                // *** Enumeration Properties Parse END ***/
                objs[i] = mc.convertRec2Obj( response[i]);
                if ( objs[i]) mc.instances[response[i][stdid]] = objs[i];
              }
              continueProcessing( objs, null);
            } else {
              console.log( "Error " + r.status + ": " + r.statusText);
              continueProcessing( null, "Error " + r.status + ": " + r.statusText);
            }
          } catch ( e) {
            console.log( e);
            throw "sTORAGEmANAGER['MariaDB'].retrieveAll: fatal exception " 
              + "occurred during the 'retrieveAll' MariaDB request!";
          } 
        }
      });
    } 
    // server side: make MariaDB query
    else {
      // makes no sense to execute SQL statements if is nothing to do with the response...
      if ( typeof continueProcessing === 'function') {
        this.getConnection( function ( err, dbConnection) {
          var sqlStatement = '';
          if ( err) {
            continueProcessing( null, err);
          } else {
            sqlStatement = "SELECT * FROM " + tableName + ";";
            dbConnection.query( sqlStatement, function ( err, rows) {
              var objs = [];
              if ( err) {
                continueProcessing( null, err);
              } else {
                for ( i = 0; i < rows.length; i++) {
                  // convert property names from underscore notation to CamelCase notation
                  objs[i] = {};
                  Object.keys( rows[i]).forEach( function ( pName) {
                    objs[i][util.getPropertyName( pName)] = rows[i][pName];                 
                  });
                }
                continueProcessing( objs, null);
              }
            });
          }
        });
      }
    }
  },
  /**
   * Generic method for creating a new instance for a model object
   * @method
   * @param {object} mc  The model class concerned
   * @param {object} slots  The slots for object creation
   * @param {object} newObj  The new object
   * @param {function} continueProcessing  The function that is invoked after
   * the async operation has completed. Its first parameter is the resulting data,
   * if any, and its second optional parameter is an error message/object.
   */
  //------------------------------------------------
  add: function ( mc, slots, newObj, continueProcessing) {
  //------------------------------------------------    
    var tableName = util.getTableName( mc.name), url = ''
        tSlots = JSON.parse(JSON.stringify( slots)), 
        appPath = $appNS && $appNS.ctrl && $appNS.ctrl.appPath ? $appNS.ctrl.appPath : '';
    // client side: make POST request
    if ( typeof exports !== 'object') {
      url = window.location.protocol + '//' + window.location.hostname;
      if ( window.location.port > 0 && window.location.port !== 80) {    
        url += ':' + window.location.port;
      }
      url += appPath;
      xhr.POST({
        url: url + '/' + tableName,
        msgBody: JSON.stringify( tSlots),
        reqFormat: 'application/json',
        handleResponse: function ( r) {
          try {
            if ( r.status === 200 || r.status === 201 || r.status === 304) {
              if ( typeof continueProcessing === 'function') continueProcessing( newObj, null);
            } else {
              console.log( "Error " + r.status + ": " + r.statusText);
              if ( typeof continueProcessing === 'function') {
                continueProcessing( newObj, "Error " + r.status + ": " + r.statusText);
              }
            }
          } catch ( e) {
            console.log( e);
            throw "sTORAGEmANAGER['MariaDB'].add: fatal exception " 
                + "occurred during the 'add' MariaDB request!";
          }
        } 
      });
    } 
    // server side: make MariaDB query
    else {
      // makes no sense to execute SQL statements if is nothing to do with the response...
      if ( typeof continueProcessing === 'function') {
        this.getConnection( function (err, dbConnection) {
          var sqlStatement = '', params = [], qM = '',
              p = null, pName = "", multivalued = false, enumLits = null;
          if ( err) {
            continueProcessing( null, err);
          } else {
            /** take care of JS to DB enumeration conversion */
            for ( pName in newObj.type.properties) {
              p = newObj.type.properties[pName];
              multivalued = p.maxCard > 1 || p.minCard > 1;
              enumLits = null;
              if (p.range.enumLitNames) {
                enumLits = sTORAGEmANAGER.adapters["MariaDB"]
                  .convertJSEnumToDBEnum(p.range, newObj[pName], multivalued);
                newObj[pName] = enumLits;
              }
            };
            /********* END JS to DB Enum conversion **********/
            // create the INSERT statement
            sqlStatement = "INSERT INTO " + tableName + " ( ";
            Object.keys( newObj.type.properties).forEach( function ( propName, index) {
              if ( index > 0) {
                sqlStatement += ', ';
                qM += ', ';
              }
              sqlStatement += "`" + util.getColumnName( propName) + "`";
              qM += '?';
              // property value
              params.push( newObj[propName]);
            });
            sqlStatement += ') VALUE ( ';
            sqlStatement += qM + ');';
            dbConnection.query( sqlStatement, params, function ( err) {
              if ( err) {
                continueProcessing( null, err);
              } else {
                continueProcessing( null, null);
              }
            });
          }
        });
      }
    }
  },
  /**
   * Generic method for update a instance for a model object
   * @method
   * @param {object} mc  The model class concerned
   * @param {string|number} id  The object ID value
   * @param {object} slots  The slots for object update
   * @param {object} newObj  The updated object
   * @param {function} continueProcessing  The function that is invoked after
   * the async operation has completed. Its first parameter is the resulting data,
   * if any, and its second optional parameter is an error message/object.
   */
  //------------------------------------------------
  /***** newObj includes validated update slots *****/
  update: function ( mc, id, slots, newObj, continueProcessing) {
  //------------------------------------------------
    var tableName = util.getTableName( mc.name), url = '', 
        appPath = $appNS && $appNS.ctrl && $appNS.ctrl.appPath ? $appNS.ctrl.appPath : '';
    // client side: make PUT request
    if ( typeof exports !== 'object') {
      url = window.location.protocol + '//' + window.location.hostname;
      if ( window.location.port > 0 && window.location.port !== 80) {    
        url += ':' + window.location.port;
      }
      url += appPath;
      xhr.PUT({
        url: url + '/' + tableName + '/' + id,
        msgBody: JSON.stringify( slots),
        reqFormat: 'application/json',
        handleResponse: function ( r) {
          try {
            if ( r.status === 200 || r.status === 201 || r.status === 304) {
              if ( typeof continueProcessing === 'function') continueProcessing( newObj, null);
            } else {
              console.log( "Error " + r.status +": "+ r.statusText);
              if ( typeof continueProcessing === 'function') {
                continueProcessing( newObj, "Error " + r.status +": "+ r.statusText);
              }
            }
          } catch ( e) {
            console.log( e);
            throw "sTORAGEmANAGER['MariaDB'].update: fatal exception " 
                + "occurred during the 'update' MariaDB request!";
          }
        }
      });
    } 
    // server side: make MariaDB query
    else {
      // makes no sense to execute SQL statements if is nothing to do with the response...
      if ( typeof continueProcessing === 'function') {
        this.getConnection( function (err, dbConnection) {
          var sqlStatement = '', params = [], qM = '', i = 0, 
              p = null, pName = "", multivalued = false, enumLits = null;
          if ( err) {
            continueProcessing( null, err);
          } else {
            /** take care of JS to DB enumeration conversion */
            for ( pName in slots) {
              p = newObj.type.properties[pName];
              multivalued = p.maxCard > 1 || p.minCard > 1;
              enumLits = null;
              if (p.range.enumLitNames) {
                enumLits = sTORAGEmANAGER.adapters["MariaDB"]
                  .convertJSEnumToDBEnum(p.range, slots[pName], multivalued);
                slots[pName] = enumLits;
              }
            };
            /********* END JS to DB Enum conversion **********/
            // create the UPDATE statement
            sqlStatement = "UPDATE " + tableName + " SET ";
            Object.keys( slots).forEach( function ( propName, index) {
              if ( typeof slots[propName] !== 'function') {
                if ( i > 0) {
                  sqlStatement += ', ';
                }
                sqlStatement += "`" + util.getColumnName( propName) + "`" + ' = ?';
                // property value
                params.push( slots[propName]);
                i++;
              }
            });
            sqlStatement += " WHERE `" + util.getColumnName( mc.standardId) + "`= ?" ;
            params.push( id);
            dbConnection.query( sqlStatement, params, function ( err) {
              if ( err) {
                continueProcessing( null, err);
              } else {
                continueProcessing( null, null);
              }
            });
          }
        });
      }
    }
  },
  /**
   * Generic method for deletion of an instance for a model object
   * @method
   * @param {object} mc  The model class concerned
   * @param {string|number} id  The object ID value
   * @param {function} continueProcessing  The function that is invoked after
   * the async operation has completed. Its first parameter is the resulting data,
   * if any, and its second optional parameter is an error message/object.
   */
  //------------------------------------------------
  destroy: function ( mc, id, continueProcessing) {
  //------------------------------------------------
    var tableName = util.getTableName( mc.name), url = '', 
        appPath = $appNS && $appNS.ctrl && $appNS.ctrl.appPath ? $appNS.ctrl.appPath : '';
    if ( id === null || id === undefined) {
      throw "sTORAGEmANAGER['MariaDB'].destroy: undefined or null object ID!";
    }
    // client side: make DELETE request
    if ( typeof exports !== 'object') {
      if ( typeof continueProcessing !== 'function') {
        continueProcessing = function ( obj, error) {};
      }
      url = window.location.protocol + '//' + window.location.hostname;
      if ( window.location.port > 0 && window.location.port !== 80) {    
        url += ':' + window.location.port;
      }
      url += appPath;
      xhr.DELETE({
        url: url + '/' + tableName + '/' + id,
        handleResponse: function ( r) {
          var response = null;
          try {
            if ( r.status === 200 || r.status === 201 || r.status === 304) {
              continueProcessing( null, null);
            } else {
              console.log( "Error " + r.status + ": " + r.statusText);
              continueProcessing( null, "Error " + r.status + ": " + r.statusText);
            }
          } catch ( e) {
            console.log( e);
            throw "sTORAGEmANAGER['MariaDB'].destroy: fatal exception " 
                + "occurred during the 'destroy' MariaDB request!";
          }
        }
      });
    } 
    // server side: make MariaDB query
    else {
      // makes no sense to execute SQL statements if is nothing to do with the response...
      if ( typeof continueProcessing === 'function') {
        this.getConnection( function ( err, dbConnection) {
          var sqlStatement = '';
          if ( err) {
            continueProcessing( null, err);
          } else {
            sqlStatement = "DELETE FROM " + tableName + " WHERE `" + util.getColumnName( mc.standardId) + "` = ?;";
            dbConnection.query( sqlStatement, [id], function ( err, row) {
              if ( err) {
                continueProcessing( null, err);
              } else {
                continueProcessing( row, null);
              }
            });
          }
        });
      }
    }
  },
  /**
   * Generic method for deletion of al instances for a model object
   * @method
   * @param {object} mc  The model class concerned
   * @param {function} continueProcessing  The function that is invoked after
   * the async operation has completed. Its first parameter is the resulting data,
   * if any, and its second optional parameter is an error message/object.
   */
  //------------------------------------------------
  /* Delete all the entries for the specified type.
   */
  clearData: function ( mc, continueProcessing) {
  //------------------------------------------------
  var tableName = util.getTableName( mc.name), url = '', 
      appPath = $appNS && $appNS.ctrl && $appNS.ctrl.appPath ? $appNS.ctrl.appPath : '';
    // client side: make DELETE request
    if ( typeof exports !== 'object') {
      if ( typeof continueProcessing !== 'function') {
        continueProcessing = function ( obj, error) {};
      }
      url = window.location.protocol + '//' + window.location.hostname;
      if ( window.location.port > 0 && window.location.port !== 80) {    
        url += ':' + window.location.port;
      }
      url += appPath;
      xhr.DELETE({
        url: url + '/' + tableName + '/',
        handleResponse: function ( r) {
          var response = null;
          try {
            if ( r.status === 200 || r.status === 201 || r.status === 304) {
              continueProcessing( null, null);
            } else {
              console.log( "Error " + r.status + ": " + r.statusText);
              continueProcessing( null, "Error " + r.status + ": " + r.statusText);
            }
          } catch ( e) {
            console.log( e);
            throw "sTORAGEmANAGER['MariaDB'].clearData: fatal exception " 
                + "occurred during the 'clearData' MariaDB request!";
          }
        }
      });
    } 
    // server side: make MariaDB query
    else {
      // makes no sense to execute SQL statements if is nothing to do with the response...
      this.getConnection( function ( err, dbConnection) {
        var sqlStatement = '';
        if ( err) {
          continueProcessing( null, err);
        } else {
          sqlStatement = "DELETE FROM " + tableName + ";";
          dbConnection.query( sqlStatement, function ( err, row) {
            if ( err) {
              if ( typeof continueProcessing === 'function') continueProcessing( null, err);
            } else {
              if ( typeof continueProcessing === 'function') continueProcessing( null, null);
            }
          });
        }
      }); 
    }
  }
};
/**
 * Predefined class for creating enumerations as special JS objects.
 * @copyright Copyright 2014 Gerd Wagner, Chair of Internet Technology,
 *   Brandenburg University of Technology, Germany.
 * @license The MIT License (MIT)
 * @author Gerd Wagner
 * @constructor
 * @this {mODELcLASS}
 * @param {string} name  The name of the new enumeration data type.
 * @param {array} enumArg  The labels array or code list map of the enumeration
 */
function eNUMERATION( name, enumArg) {
    var i = 0, lbl = "", LBL = "";
    if (typeof name !== "string") {
        throw new ConstraintViolation(
            "The first constructor argument of an enumeration must be a string!");
    }
    this.name = name;
    if (Array.isArray(enumArg)) {
        // a simple enum defined by a list of labels
        if (!enumArg.every(function (n) {
                return (typeof(n) === "string");
            })) {
            throw new ConstraintViolation("A list of enumeration labels as the second " +
                "constructor argument must be an array of strings!");
        }
        this.labels = enumArg;
        this.enumLitNames = this.labels;
        this.codeList = null;
    } else if (typeof(enumArg) === "object" && Object.keys(enumArg).length > 0) {
        // a code list defined by a map
        if (!Object.keys(enumArg).every(function (code) {
                return (typeof( enumArg[code]) === "string");
            })) {
            throw new ConstraintViolation(
                "All values of a code list map must be strings!");
        }
        this.codeList = enumArg;
        // use the codes as the names of enumeration literals
        this.enumLitNames = Object.keys(this.codeList);
        this.labels = this.enumLitNames.map(function (c) {
            return enumArg[c] + " (" + c + ")";
        });
    } else {
        throw new ConstraintViolation(
            "Invalid Enumeration constructor argument: " + enumArg);
    }
    this.MAX = this.enumLitNames.length;
    // generate the enumeration literals by capitalizing/normalizing the names
    for (i = 1; i <= this.enumLitNames.length; i++) {
        // replace " " and "-" with "_"
        lbl = this.enumLitNames[i - 1].replace(/( |-)/g, "_");
        // convert to array of words, capitalize them, and re-convert
        LBL = lbl.split("_").map(function (lblPart) {
            return lblPart.toUpperCase();
        }).join("_");
        // assign enumeration index
        this[LBL] = i;
    }
    Object.freeze(this);
    // add new enumeration to the population of all enumerations
    eNUMERATION.instances[this.name] = this;
}
/*
 * Check if a value represents an enumeration literal or a valid index
 */
eNUMERATION.prototype.isValidEnumLitOrIndex = function (v) {
    return (Number.isInteger(v) && v > 0 && v < this.MAX);
};
/*
 * Serialize a list of enumeration literals/indexes as a list of
 * enumeration literal names
 */
eNUMERATION.prototype.enumIndexesToNames = function (a) {
    if (!Array.isArray(a)) {
        throw new ConstraintViolation(
            "The argument must be an Array!");
    }
    var listStr = a.map(function (enumInt) {
        return this.enumLitNames[enumInt - 1];
    }, this).join(", ");
    return listStr;
};
/*
 * Define a map of all enumerations as a class-level property
 */
eNUMERATION.instances = {};

/**
 * @fileOverview  This file contains the definition of the abstract
 * meta-class cOMPLEXtYPE.
 * @author Gerd Wagner
 * @copyright Copyright 2015 Gerd Wagner, Chair of Internet Technology,
 *   Brandenburg University of Technology, Germany.
 * @license The MIT License (MIT)
 */
/**
 * Abstract meta-class subsuming cOMPLEXdATAtYPE and eNTITYtYPE.
 * cOMPLEXtYPE defines the following type features:
 * name, supertype(s), properties, methods (including the
 * predefined methods toDisplayString, toRecord, set and isInstanceOf)
 * TODO: staticMethods
 *
 * By default, properties are mandatory, if there is no declaration slot
 * "optional:true". The property declaration parameter "minCard" is only
 * meaningful for multi-valued properties, for which "maxCard" has been
 * set to a value greater than 1.
 *
 * On cOMPLEXtYPE creation, the constructor argument "methods" may include
 * a custom "validate" method for validating a complex data value or entity
 * not property-wise, but as a whole, returning an error message in case
 * of a complex-value-level constraint violation.
 *
 * @constructor
 * @this {cOMPLEXtYPE}
 * @param  slots  The complex type definition slots.
 */
function cOMPLEXtYPE( slots) {
  var k=0, keys=[], supertype=null,
      featureMaps = {properties:{}, methods:{}, staticMethods:{}};
  this.name = slots.name;
  Object.keys( slots.properties).forEach( function (prop) {
    var propDeclParams = slots.properties[prop],  // the property declaration slots
        minCard = propDeclParams.minCard,
        maxCard = propDeclParams.maxCard;
    // check if cardinality constraints are meaningful
    if (minCard !== undefined) {
      if (!Number.isInteger( minCard) || minCard < 0)
      throw new cOMPLEXtYPEconstraintViolation(
          "minCard value for "+ prop +" ("+ JSON.stringify(maxCard)) +
          ") is invalid (not a non-negative integer)!";
      else if (maxCard === undefined)
        throw new cOMPLEXtYPEconstraintViolation(
            "A minCard value for "+ prop +" is only meaningful when maxCard is defined!");
      else if ((!Number.isInteger( maxCard) || maxCard < 2) && maxCard !== Infinity)
      throw new cOMPLEXtYPEconstraintViolation(
        "Invalid maxCard value for "+ prop +": "+ JSON.stringify(maxCard));
      else if (maxCard < minCard)
        throw new cOMPLEXtYPEconstraintViolation(
            "maxCard value for "+ prop +" is less than mincCard value! ");
    }
    // add default property descriptors
    featureMaps.properties[prop] = util.mergeObjects(
        {writable: true, enumerable: true}, propDeclParams);
  }, this);
  slots.properties = featureMaps.properties;  // completed own properties
  if (slots.supertype || slots.supertypes) {  // non-root class
    if (slots.supertype) {  // only one direct supertype
      this.supertype = slots.supertype;
      Object.keys( featureMaps).forEach( function (fMK) {  // fMK = featureMapKey
        // assign featureMap by merging the supertype's featureMap with own featureMap
        if (slots[fMK] || slots.supertype[fMK]) {
          this[fMK] = util.mergeObjects( slots.supertype[fMK], slots[fMK]);
        }
      }, this);  // passing the context object reference to the forEach function
    } else {  // multiple direct supertypes
      this.supertypes = slots.supertypes;
      // collect features for all featureMaps of all direct supertypes
      for (k=0; k < slots.supertypes.length; k++) {
        supertype = slots.supertypes[k];
        Object.keys( featureMaps).forEach( function (fMK) {
          if (supertype[fMK]) {
            featureMaps[fMK] = util.mergeObjects( featureMaps[fMK], supertype[fMK]);
          }
        });
      }
      Object.keys( featureMaps).forEach( function (fMK) {
        // assign merged featureMaps to object
        if (featureMaps[fMK] || slots[fMK]) {
          this[fMK] = util.mergeObjects( featureMaps[fMK], slots[fMK]);
        }
      }, this);  // passing the context object reference to the forEach function
    }
  } else {  // root class (no inheritance)
    Object.keys( featureMaps).forEach( function (fMK) {
      // assign merged featureMaps to object
      if (slots[fMK]) this[fMK] = slots[fMK];
    }, this);  // passing the context object reference to the forEach function
    if (!this.methods) this.methods = {};
    // add predefined methods such that they will be inherited by all subtypes
    // **************************************************
    this.methods.toString = function () {
    // **************************************************
   	  var str = this.type.name + "{ ";
   	  Object.keys( this).forEach( function (key,i) {
        if (key !== "type" && this[key] !== undefined) {
          str += (i>0 ? ", " : "") + key +": "+ this[key];
        }
   	  }, this);  // pass context object reference
   	  return str +"}";
   	};
    // **************************************************
    this.methods.set = function ( prop, val) {
    // **************************************************
      // this = object
      var constrViol = this.type.check( prop, val);
      if (constrViol instanceof NoConstraintViolation) {
        this[prop] = constrViol.checkedValue;
      } else {
        throw constrViol;
      }
    };
    // **************************************************
    this.methods.isInstanceOf = function (DataType) {
    // **************************************************
      if (!this.type) return false;  // not an object created by a factory
      else return (this.type === DataType) ? true :
                    this.type.isSubTypeOf( DataType);
    };
  }
}
/**
 * Check if this type is a subtype of another type.
 * @method
 * @author Gerd Wagner
 * @param {cOMPLEXtYPE} Class  The class to be tested as superclass.
 * @return {boolean}
 */
cOMPLEXtYPE.prototype.isSubTypeOf = function (DataType) {
  if (!this.supertype && !this.supertypes) return false;
  if (this.supertype) {
    return (this.supertype === DataType) ? true :
                  this.supertype.isSubTypeOf( DataType);
  } else {
    return (this.supertypes.indexOf( DataType) > -1) ? true :
                this.supertypes.some( function (sT) {return sT.isSubTypeOf( DataType);});
  }
};
/**
 * Determine if a property is integer-valued.
 * @method
 * @author Gerd Wagner
 * @param {cOMPLEXdATAtYPE,name|eNUMERATION|cOMPLEXdATAtYPE} T  The type to be checked.
 * @return {boolean}
 */
cOMPLEXtYPE.isIntegerType = function (T) {
  return (typeof(T)==="string" &&
  ["Integer", "NonNegativeInteger","PositiveInteger"].indexOf( T) > -1 ||
  T instanceof eNUMERATION);
};
/**
 * Generic method for checking the integrity constraints defined in property declarations.
 * The values to be checked are first parsed/deserialized if provided as strings.
 *
 * min/max: numeric (or string length) minimum/maximum
 * optional: true if property is single-valued and optional (false by default)
 * range: String|NonEmptyString|Integer|...
 * pattern: a regular expression to be matched
 * minCard/maxCard: minimum/maximum cardinality of a multi-valued property
 *     By default, maxCard is 1, implying that the property is single-valued, in which
 *     case minCard is meaningless/ignored. maxCard may be Infinity.
 *
 * @method
 * @author Gerd Wagner
 * @param {string} prop  The property for which a value is to be checked.
 * @param {string|number|boolean|object} val  The value to be checked.
 * @return {ConstraintViolation}  The constraint violation object.
 */
cOMPLEXtYPE.prototype.check = function (prop, val) {
  var propDeclParams = this.properties[prop],
      constrVio=null, valuesToCheck=[], i=0, keys=[], msg="", label="",
      minCard = 0,  // by default, a multi-valued property is optional
      maxCard = 1,  // by default, a property is single-valued
      card, min=0, max, range, pattern="";
  if (propDeclParams) {
    range = propDeclParams.range;
    min = propDeclParams.min || min;
    max = propDeclParams.max;
    minCard = propDeclParams.minCard || minCard;
    maxCard = propDeclParams.maxCard || maxCard;
    pattern = propDeclParams.pattern;
    msg = propDeclParams.patternMessage;
    label = propDeclParams.label || prop;
  } else {
    return new cOMPLEXtYPEconstraintViolation(
        "Property to be checked ("+ prop +
        ") is not among the type's properties: "+
        Object.keys( this.properties).toString());
  }
  if (!val) {
    if (propDeclParams.optional) return new NoConstraintViolation();
    else {
      return new MandatoryValueConstraintViolation(
          "A value for "+ prop +" is required!");
    }
  }
  if (maxCard === 1) {  // single-valued property
    valuesToCheck = [val];
  } else {  // multi-valued property
    // can be array-valued or map-valued
    if (Array.isArray( val) ) {
      valuesToCheck = val;
    } else if (typeof( val) === "string") {
      valuesToCheck = val.split(",").map(function (el) {
        return el.trim();
      });
    } else {
      return new RangeConstraintViolation("Values for "+ prop +
      " must be arrays!");
    }
  }
  // convert integer strings to integers
  if (cOMPLEXtYPE.isIntegerType( range)) {
    valuesToCheck.forEach( function (v,i) {
      if (typeof( v) === "string") valuesToCheck[i] = parseInt( v);
    });
  }
  /*********************************************************************
   ***  Convert value strings to values and check range constraints ****
   ********************************************************************/
  switch (range) {
  case "String":
    valuesToCheck.forEach( function (v) {
      if (typeof(v) !== "string") {
        constrVio = new RangeConstraintViolation("Values for "+ prop +
          " must be strings!");
      }
    });
    break;
  case "NonEmptyString":
    valuesToCheck.forEach( function (v) {
      if (typeof(v) !== "string" || v.trim() === "") {
        constrVio = new RangeConstraintViolation("Values for "+ prop +
            " must be non-empty strings!");
      }
    });
    break;
  case "Integer":
    valuesToCheck.forEach( function (v) {
      if (!Number.isInteger(v)) {
        constrVio = new RangeConstraintViolation("The value of "+ prop +
            " must be an integer!");
      }
    });
    break;
  case "NonNegativeInteger":
    valuesToCheck.forEach( function (v) {
      if (!Number.isInteger(v) || v < 0) {
        constrVio = new RangeConstraintViolation("The value of "+ prop +
            " must be a non-negative integer!");
      }
    });
    break;
  case "PositiveInteger":
    valuesToCheck.forEach( function (v) {
      if (!Number.isInteger(v) || v < 1) {
        constrVio = new RangeConstraintViolation("The value of "+ prop +
            " must be a positive integer (and not "+ v +")!");
      }
    });
    break;
  case "Decimal":
    valuesToCheck.forEach( function (v,i) {
      if (typeof( v) === "string") valuesToCheck[i] = parseFloat( v);
      if (typeof(v) !== "number") {
        constrVio = new RangeConstraintViolation("The value of "+ prop +
            " must be a (decimal) number!");
      }
    });
    break;
  case "Boolean":
    valuesToCheck.forEach( function (v) {
      if (typeof(v) !== "boolean") {
        constrVio = new RangeConstraintViolation("The value of "+ prop +
            " must be either 'true' or 'false'!");
      }
    });
    break;
  case "Date":
    valuesToCheck.forEach( function (v) {
      if (typeof(v) === "string" &&
          (/\d{4}-(0\d|1[0-2])-([0-2]\d|3[0-1])/.test(v) ||
          !isNaN( Date.parse(v)))) {
        valuesToCheck[i] = new Date(v);
      } else if (!(v instanceof Date)) {
            constrVio = new RangeConstraintViolation("The value of "+ prop +
                " must be either a Date value or an ISO date string. "+
                v +" is not admissible!");
        }
    });
    break;
  default:
    if (range instanceof eNUMERATION) {
      valuesToCheck.forEach( function (v) {
        if (!Number.isInteger( v) || v < 1 || v > range.MAX) {
          constrVio = new RangeConstraintViolation("The value "+ v +
              " is not an admissible enumeration integer for "+ prop);
        }
      });
    } else if (range instanceof cOMPLEXdATAtYPE) {
      valuesToCheck.forEach( function (v,i) {
        try {
          valuesToCheck[i] = range.create(v);
        } catch (e) {
          constrVio = new cOMPLEXdATAtYPEconstraintViolation("in "+ range.name +
              ": "+ e.constructor.name +": "+ e.message);
        }
      });
    } else if (Array.isArray( range)) {
      // *** Ad-hoc enumeration ***
      valuesToCheck.forEach( function (v) {
        if (range.indexOf(v) === -1) {
          constrVio = new RangeConstraintViolation("The "+ prop +" value "+ v +
              " is not in value list "+ range.toString());
        }
      });
    } else if (typeof(range) == "function") {
      // constructor-based class
      valuesToCheck.forEach( function (v) {
        if (!(v instanceof range)) {
          constrVio = new RangeConstraintViolation("The value of "+ prop +
              " must be a(n) "+ range.name +"! "+ JSON.stringify(v) +" is not admissible!");
        }
      });
    }
  }
  // return constraint violation found in range switch
  if (constrVio) return constrVio;

  /********************************************************
   ***  Check constraints that apply to several ranges  ***
   ********************************************************/
  if (range === "String" || range === "NonEmptyString") {
    valuesToCheck.forEach( function (v) {
      if (min !== undefined && v.length < min) {
        constrVio = new StringLengthConstraintViolation("The length of "+
            prop + " must not be smaller than "+ min);
      } else if (max !== undefined && v.length > max) {
        constrVio = new StringLengthConstraintViolation("The length of "+
            prop + " must not be greater than "+ max);
      } else if (pattern !== undefined && !pattern.test( v)) {
          constrVio = new PatternConstraintViolation( msg || v +
              "does not comply with the pattern defined for "+ prop);
      }
    });
  }
  if (range === "Integer" || range === "NonNegativeInteger" ||
      range === "PositiveInteger") {
    valuesToCheck.forEach( function (v) {
      if (min !== undefined && v < min) {
        constrVio = new IntervalConstraintViolation( prop +
            " must be greater than "+ min);
      } else if (max !== undefined && v > max) {
        constrVio = new IntervalConstraintViolation( prop +
            " must be smaller than "+ max);
      }
    });
  }
  if (constrVio) return constrVio;

  /********************************************************
   ***  Check cardinality constraints  *********************
   ********************************************************/
  if (maxCard > 1) { // (a multi-valued property can be array-valued or map-valued)
    // check minimum cardinality constraint
    if (minCard > 0 && valuesToCheck.length < minCard) {
      return new CardinalityConstraintViolation("A collection of at least "+
          minCard +" values is required for "+ prop);
    }
    // check maximum cardinality constraint
    if (valuesToCheck.length > maxCard) {
      return new CardinalityConstraintViolation("A collection value for "+
          prop +" must not have more than "+ maxCard +" members!");
    }
  }
  val = maxCard === 1 ? valuesToCheck[0] : valuesToCheck;
  return new NoConstraintViolation( val);
};

/**
 * @fileOverview  This file contains the definition of the meta-class cOMPLEXdATAtYPE.
 * @author Gerd Wagner
 * @copyright Copyright 2015 Gerd Wagner, Chair of Internet Technology,
 *   Brandenburg University of Technology, Germany.
 * @license The MIT License (MIT)
 */
 /**
 * Meta-class for creating (complex) datatypes with a create method.
 * cOMPLEXdATAtYPE defines the following type features:
 * name, supertype(s), properties, methods (including the
 * predefined methods toDisplayString, toRecord, set and isInstanceOf)
 * TODO: staticMethods
 *
 * By default, properties are mandatory, if there is no declaration slot
 * "optional:true". The property declaration parameter "minCard" is only
 * meaningful for multi-valued properties, for which "maxCard" has been
 * set to a value greater than 1.
 *
 * On datatype creation, the constructor argument "methods" may include
 * a custom data-value-level "validate" method returning an error message 
 * in case of an data-value-level constraint violation.
 *
 * @constructor
 * @this {cOMPLEXdATAtYPE}
 * @param {{name: string, supertype: string?, supertypes: string?,
 *          properties: Map, methods: Map?, staticMethods: Map?,
 *          reactionRules: Map?, internalEventTypes: Map?}}
 *        slots  The entity type definition slots.
 */
function cOMPLEXdATAtYPE( slots) {
  var k=0, keys=[], supertype=null,
      featureMaps = {properties:{}, methods:{}, staticMethods:{}};

  // Any supertype of a datatype must be a datatype
  if (slots.supertype && !(slots.supertype instanceof cOMPLEXdATAtYPE)) {
    throw new cOMPLEXdATAtYPEconstraintViolation("Supertype "+ slots.supertype +
        " of cOMPLEXdATAtYPE "+ this.name +" is not a cOMPLEXdATAtYPE!");
  }
  // Check datatype-specific constraints for property declarations
  Object.keys( slots.properties).forEach( function (prop) {
    var propDeclParams = slots.properties[prop];  // the property declaration slots
    // A datatype does not have an ID attribute
    if (propDeclParams.isStandardId || prop === "id")
      throw new cOMPLEXdATAtYPEconstraintViolation("A datatype must not have an ID attribute");
    // check if datatype property has a meaningful range
    if (!propDeclParams.range)
      throw new cOMPLEXdATAtYPEconstraintViolation(
          "There must be a range definition for "+ prop +" !");
    if (eNTITYtYPE && propDeclParams.range instanceof eNTITYtYPE)
      throw new cOMPLEXdATAtYPEconstraintViolation(
          "A datatype property must not have an entity type as range!");
  }, this);

  // invoke the cOMPLEXtYPE constructor since a cOMPLEXdATAtYPE is a cOMPLEXtYPE
  cOMPLEXtYPE.call( this, slots);

  // add predefined methods such that they will be inherited by all subtypes
  if (!slots.supertype && !slots.supertypes) {  // root class (no inheritance)
    // add predefined methods such that they will be inherited by all subtypes
    // ***** toDisplayString **********************
    this.methods.toDisplayString = function () {
      var str="";
      Object.keys( this).forEach( function (p) {
        if (p !== "type" && this[p] !== undefined) {
          str += this.getValAsString( p) + ", ";
        }
      }, this);  // pass context object reference
      return str;
    };
    // **************************************************
    this.methods.getValAsString = function (p) {
    // **************************************************
      var obj = this, valuesToConvert=[], displayStr="", k=0,
          listSep = ", ",
          propDecl = obj.type.properties[p],
          range = propDecl.range,
          val = obj[p];
      if (val === undefined) return "";
      if (propDecl.maxCard && propDecl.maxCard > 1) {
        if (Array.isArray( val)) {
          valuesToConvert = val.slice(0);  // clone;
        } else console.log("The value of a multi-valued " +
            "datatype property must be an array!");
      } else valuesToConvert = [val];
      valuesToConvert.forEach( function (v,i) {
        if (range instanceof eNUMERATION) {
          valuesToConvert[i] = range.labels[v-1];
        } else if (["number","string","boolean"].includes( typeof(v)) || !v) {
          valuesToConvert[i] = String( v);
        } else if (range === "Date") {
          valuesToConvert[i] = util.createIsoDateString( v);
        } else if (Array.isArray( v)) {  // JSON-compatible array
          valuesToConvert[i] = v.slice(0);  // clone
        } else valuesToConvert[i] = JSON.stringify( v);
      });
      displayStr = valuesToConvert[0];
      if (propDecl.maxCard && propDecl.maxCard > 1) {
        displayStr = "[" + displayStr;
        for (k=1; k < valuesToConvert.length; k++) {
          displayStr += listSep + valuesToConvert[k];
        }
        displayStr = displayStr + "]";
      }
      return displayStr;
    };
    // **************************************************
    this.methods.toRecord = function () {
    // **************************************************
      var obj = this, rec={}, propDecl={}, valuesToConvert=[], range, val;
      Object.keys( obj).forEach( function (p) {
        if (p !== "type" && obj[p] !== undefined) {
          val = obj[p];
          propDecl = obj.type.properties[p];
          range = propDecl.range;
          if (propDecl.maxCard && propDecl.maxCard > 1) {
            if (Array.isArray( val)) {
              valuesToConvert = val.slice(0);  // clone;
            } else console.log("The value of a multi-valued " +
                "datatype property must be an array!");
          } else valuesToConvert = [val];
          valuesToConvert.forEach( function (v,i) {
            // alternatively: enum literals as labels
            // if (range instanceof eNUMERATION) rec[p] = range.labels[val-1];
            if (["number","string","boolean"].includes( typeof(v)) || !v) {
              valuesToConvert[i] = String( v);
            } else if (range === "Date") {
              valuesToConvert[i] = util.createIsoDateString( v);
            } else if (Array.isArray( v)) {  // JSON-compatible array
              valuesToConvert[i] = v.slice(0);  // clone
            } else valuesToConvert[i] = JSON.stringify( v);
          });
          if (!propDecl.maxCard || propDecl.maxCard <= 1) {
            rec[p] = valuesToConvert[0];
          } else {
            rec[p] = valuesToConvert;
          }
        }
      });
      return rec;
    };
  }
  // add new datatype to the cOMPLEXdATAtYPE.types map
  cOMPLEXdATAtYPE.types[this.name] = this;
}

cOMPLEXdATAtYPE.prototype = Object.create( cOMPLEXtYPE.prototype);
cOMPLEXdATAtYPE.prototype.constructor = cOMPLEXdATAtYPE;

cOMPLEXdATAtYPE.types = {};  // a map of all datatypes

/**
 * Factory method for creating new complex data values as instances
 * of a cOMPLEXdATAtYPE.
 *
 * Create new complex data value by assigning the methods defined
 * for the cOMPLEXdATAtYPE to the data value's prototype and using the
 * property descriptors of the property declarations of the
 * cOMPLEXdATAtYPE for defining its property slots.
 *
 * @method
 * @author Gerd Wagner
 * @param {object} initSlots  The object initialization slots.
 * @return {object}  The new object.
 */
cOMPLEXdATAtYPE.prototype.create = function (initSlots) {
  var dv = Object.create( this.methods, this.properties),
      props = this.properties, msg="";
  // add predefined property "type" for direct type
  Object.defineProperty( dv, "type",
      {value: this, writable: false, enumerable: true});
  // initialize object
  Object.keys( initSlots).forEach( function (prop) {
    var val = initSlots[prop];
    // check property constraints only when property is defined in cOMPLEXdATAtYPE
    if (props[prop]) dv.set( prop, val);
    else if (prop !== 'type') {  // 'type' is ignored
      console.log("Undefined property: "+ prop);
    }
  });
  // validation at the aggregate level of the complex data value
  if (this.methods.validate) {
    msg = dv.validate();
    if (msg) throw new ConstraintViolation( this.name +
        dv.getValAsString +": "+ msg);
  }
  // assign initial value to unassigned properties
  Object.keys( props).forEach( function (prop) {
    if (dv[prop] === undefined &&
        props[prop].initialValue !== undefined) {
      dv[prop] = props[prop].initialValue;
    }
  });
  return dv;
};
/**
 * @fileOverview  This file contains the definition of the abstract
 * meta-class eNTITYtYPE.
 * @author Gerd Wagner
 * @copyright Copyright 2015 Gerd Wagner, Chair of Internet Technology,
 *   Brandenburg University of Technology, Germany.
 * @license The MIT License (MIT)
 */
 /**
 * Abstract meta-class for deriving the meta-classes mODELcLASS, oBJECTtYPE, etc.
 * with a create method for entity creation. eNTITYtYPE defines the following
 * type features: name, supertype(s), properties, methods (including the
 * predefined methods toDisplayString, toRecord, set and isInstanceOf),
 * isNonPersistent,
 * TODO: storageAdapter
 * TODO: staticMethods,
 * and the agent type features reactionRules and internalEventTypes.
 *
 * By default, properties are mandatory, if there is no declaration slot
 * "optional:true". The property declaration parameter "minCard" is only
 * meaningful for multi-valued properties, for which "maxCard" has been
 * set to a value greater than 1.
 *
 * On entity type creation, the constructor argument "methods" may include
 * a custom object-level "validate" method returning an error message string
 * in case of an object-level constraint violation.
 *
 * TODO: New features for managing events in the context of observer entities:
 * (preferably return context entity, so calls can be chained)
 * + addEventListener( eventTypeName,[ targetEntity,] listenerMethod)
 *       If no targetEntity provided: targetEntity = observerEntity
 * + removeEventListener( eventTypeName,[ targetEntity,] listenerMethod)
 * + eventListeners( eventTypeName)
 *       Returns an array of listeners for the specified event type.
 * + emitEvent( eventTypeName[, arg1][, arg2][, ...])
 *       Execute all listeners in order with the supplied arguments.
 * + Built-in events:
 *   + Entity lifecycle events (Creation, Change, Destruction)
 *   + Collection events (Addition, Removal)
 *   + I/O events ...?
 *
 * @constructor
 * @this {eNTITYtYPE}
 * @param {{name: string, supertype: string?, supertypes: string?,
 *          properties: Map, methods: Map?, staticMethods: Map?,
 *          reactionRules: Map?, internalEventTypes: Map?}}
 *        slots  The entity type definition slots.
 */
function eNTITYtYPE( slots) {
  var k=0, keys=[], supertype=null,
      featureMaps = {properties:{}, methods:{}, staticMethods:{},
          reactionRules:{}, internalEventTypes:{}};
  // check entity property range and set standardId
  Object.keys( slots.properties).forEach( function (prop) {
    var propDeclParams = slots.properties[prop];  // the property declaration slots
    // check if properties are declared with a range
    if (!propDeclParams.range && !(slots.methods && "validate" in slots.methods)) 
      throw new eNTITYtYPEconstraintViolation(
          "Either there must be a range definition for "+ prop +
          " or its's range must be checked in a 'validate' method!");
    if (propDeclParams.isStandardId || prop === "id") this.standardId = prop;
  }, this);

  // invoke the cOMPLEXtYPE constructor since an eNTITYtYPE is a cOMPLEXtYPE
  cOMPLEXtYPE.call( this, slots);

  // TODO: Is this really needed? Or are non-persistent cases complex datatypes?
  if (slots.isNonPersistent) {
    this.isNonPersistent = slots.isNonPersistent;
  }
  // add predefined methods such that they will be inherited by all subtypes
  if (!slots.supertype && !slots.supertypes) {  // root class (no inheritance)
    // **************************************************
    this.methods.getValAsString = function (p) {
    // **************************************************
      var mODELcLASS = mODELcLASS || undefined;
      var valuesToConvert=[], displayStr="", k=0, listSep = ", ",
	      obj = this, val = obj[p],
          propDecl = obj.type.properties[p], 
		  range = propDecl.range;
      if (val === undefined) return "";
      if (propDecl.maxCard && propDecl.maxCard > 1) {
        if (mODELcLASS && range instanceof mODELcLASS) { // object reference(s)
          if (Array.isArray( val)) {
            valuesToConvert = val.slice(0);  // clone;
          } else {  // map from ID refs to obj refs
            valuesToConvert = Object.values( val);
          }
        } else if (Array.isArray( val)) {
          valuesToConvert = val.slice(0);  // clone;
        } else console.log("Invalid non-array collection in to Record!");
      } else {
        valuesToConvert = [val];
      }
      valuesToConvert.forEach( function (v,i) {
        if (range instanceof eNUMERATION) {
          valuesToConvert[i] = range.labels[v-1];
        } else if (["number","string","boolean"].includes( typeof(v)) || !v) {
          valuesToConvert[i] = String( v);
        } else if (range === "Date") {
          valuesToConvert[i] = util.createIsoDateString( v);
        } else if (Array.isArray( v)) {  // JSON-compatible array
          valuesToConvert[i] = v.slice(0);  // clone
        } else if (range instanceof cOMPLEXdATAtYPE) {
          valuesToConvert[i] = v.toDisplayString();
        } else if (mODELcLASS && range instanceof mODELcLASS) { // object reference(s)
          valuesToConvert[i] = v[range.standardId];
        } else valuesToConvert[i] = JSON.stringify( v);
      });
      displayStr = valuesToConvert[0];
      if (propDecl.maxCard && propDecl.maxCard > 1) {
        for (k=1; k < valuesToConvert.length; k++) {
          displayStr += listSep + valuesToConvert[k];
        }
      }
      return displayStr;
    };
    // **************************************************
    this.methods.toRecord = function () {
    // **************************************************
      var mODELcLASS = mODELcLASS || undefined;
      var obj = this, rec={}, propDecl={}, valuesToConvert=[], range, val;
      Object.keys( obj).forEach( function (p) {
        if (p !== "type" && obj[p] !== undefined) {
          val = obj[p];
          propDecl = obj.type.properties[p];
          range = propDecl.range;
          if (propDecl.maxCard && propDecl.maxCard > 1) {
            if (mODELcLASS && range instanceof mODELcLASS) { // object reference(s)
              if (Array.isArray( val)) {
                valuesToConvert = val.slice(0);  // clone;
              } else {  // map from ID refs to obj refs
                valuesToConvert = Object.values( val);
              }
            } else if (Array.isArray( val)) {
              valuesToConvert = val.slice(0);  // clone;
            } else console.log("Invalid non-array collection in to Record!");
          } else {  // maxCard=1
            valuesToConvert = [val];
          }
          valuesToConvert.forEach( function (v,i) {
            // alternatively: enum literals as labels
            // if (range instanceof eNUMERATION) rec[p] = range.labels[val-1];
            if (["number","string","boolean"].includes( typeof(v)) || !v) {
              valuesToConvert[i] = String( v);
            } else if (range === "Date") {
              valuesToConvert[i] = util.createIsoDateString( v);
            } else if (range instanceof cOMPLEXdATAtYPE) {
              valuesToConvert[i] = v.toRecord();
            } else if (mODELcLASS && range instanceof mODELcLASS) { // object reference(s)
              valuesToConvert[i] = v[range.standardId];
            } else if (Array.isArray( v)) {  // JSON-compatible array
              valuesToConvert[i] = v.slice(0);  // clone
            } else valuesToConvert[i] = JSON.stringify( v);
          });
          if (!propDecl.maxCard || propDecl.maxCard <= 1) {
            rec[p] = valuesToConvert[0];
          } else {
            rec[p] = valuesToConvert;
          }
        }
      });
      return rec;
    };
  }
}
eNTITYtYPE.prototype = Object.create( cOMPLEXtYPE.prototype);
eNTITYtYPE.prototype.constructor = eNTITYtYPE;

/**
 * Convert property value to form field value.
 * @method
 * @author Gerd Wagner
 * @param {eNTITYtYPE} ET  The domain of the property.
 * @param {string} prop  The property name.
 * @param {?} v  The value to be converted.
 * @return {boolean}
 */
eNTITYtYPE.getValAsString = function ( ET, prop, v) {
  var mODELcLASS = mODELcLASS || undefined;
  var displayStr="",
      propDecl = ET.properties[prop], range = propDecl.range;
    if (v === undefined) return "";
    if (range instanceof eNUMERATION) return range.labels[v-1];
    if (["number","string","boolean"].includes( typeof(v))) {
        return String( v);
    }
    if (range === "Date") return util.createIsoDateString( v);
    if (mODELcLASS && range instanceof mODELcLASS) return v[range.standardId];
    return JSON.stringify( v);
};
/**
 * Generic method for checking the integrity constraints defined in property declarations.
 * The values to be checked are first parsed/converted if provided as strings.
 *
 * min/max: numeric (or string length) minimum/maximum
 * optional: true if property is single-valued and optional (false by default)
 * range: String|NonEmptyString|Integer|...
 * pattern: a regular expression to be matched
 * minCard/maxCard: minimum/maximum cardinality of a multi-valued property
 *     By default, maxCard is 1, implying that the property is single-valued, in which
 *     case minCard is meaningless/ignored. maxCard may be Infinity.
 * unique: property is unique
 * isStandardId: property is standard identifier
 *
 * @method
 * @author Gerd Wagner
 * @param {string} prop  The property for which a value is to be checked.
 * @param {string|number|boolean|object} val  The value to be checked.
 * @return {ConstraintViolation}  The constraint violation object.
 */
eNTITYtYPE.prototype.check = function (prop, val) {
  var mODELcLASS = mODELcLASS || undefined;
  var propDeclParams = this.properties[prop],
      constrVio=null, valuesToCheck=[], i=0, keys=[], range,
      maxCard = 1;  // by default, a property is single-valued

  // invoke the cOMPLEXtYPE check method since an eNTITYtYPE is a cOMPLEXtYPE
  constrVio = cOMPLEXtYPE.prototype.check.call( this, prop, val);
  if (!(constrVio instanceof NoConstraintViolation)) return constrVio;

  range = propDeclParams.range;
  maxCard = propDeclParams.maxCard || maxCard;
  if (maxCard === 1) {  // single-valued property
    valuesToCheck = [val];
  } else {  // multi-valued property
    // can be array-valued or map-valued
    if (Array.isArray(val) ) {
      valuesToCheck = val;
    } else if (typeof(val) === "object" && Object.keys( val).length > 0) {
      valuesToCheck = Object.keys( val); // TODO
    } else if (typeof(val) === "string") {
      valuesToCheck = val.split(",").map(function (el) {
        return el.trim();
      });
    } else {
      return new RangeConstraintViolation("Values for "+ prop +
      " must be arrays or maps!");
    }
  }
  if (mODELcLASS && range instanceof mODELcLASS) {
    valuesToCheck.forEach( function (v,i) {
      if (Number.isInteger(v)) v = String(v);
      if (typeof(v) === "string") {
        if (range.instances[v] !== undefined) {
          v = range.instances[v];
          valuesToCheck[i] = v;
        } else {
          constrVio = new RangeConstraintViolation( v +" is not a valid ID ref in "+
              range.name +".instances, as required for "+ prop +" !");
        }
      }
      if (typeof(v) !== "object" || v.type === undefined || v.type !== range) {
        constrVio = new RangeConstraintViolation("The value of "+ prop +
            " must reference an existing "+ range.name +"! "+
            JSON.stringify(v) +" does not!");
      }
    });
  }
  // return constraint violation if there is any
  if (constrVio) return constrVio;
  /********************************************************
   ***  Check uniqueness constraints  *********************
   ********************************************************/
  if (propDeclParams.unique && this.instances) {
    keys = Object.keys( this.instances);
    for (i=0; i < keys.length; i++) {
      if (this.instances[keys[i]][prop] === val) {
        return new UniquenessConstraintViolation("There is already a "+
            this.name +" with a(n) "+ prop +" value "+ val +"!");
      }
    }
  }
  if (propDeclParams.isStandardId) {
    if (val === undefined) {
      return new MandatoryValueConstraintViolation("A value for the " +
          "standard identifier attribute "+ prop +" is required!");
    }
    /*TODO
    else if (this.instances && this.instances[val]) {
      return new UniquenessConstraintViolation("There is already a "+
          this.name +" with a(n) "+ prop +" value "+ val +"!");
    }
    */
  }
  val = maxCard === 1 ? valuesToCheck[0] : valuesToCheck;
  return new NoConstraintViolation( val);
};
/**
 * ??? Really Needed ???
 * Generic method for checking uniqueness/stdid constraints defined in
 * property declarations with
 * unique: property is unique
 * isStandardId: property is standard identifier
 *
 * @method
 * @author Gerd Wagner
 * @param {string} prop  The property for which a value is to be checked.
 * @param {string|number|object} val  The value to be checked.
 * @return {ConstraintViolation}  The constraint violation object.
 */
eNTITYtYPE.prototype.checkUniqueness = function (prop, val) {
  var propDeclParams = this.properties[prop],
      i=0, keys=[];
  if (propDeclParams.unique && this.instances) {
    keys = Object.keys( this.instances);
    for (i=0; i < keys.length; i++) {
      if (this.instances[keys[i]][prop] === val) {
        return new UniquenessConstraintViolation("There is already a "+
            this.name +" with a(n) "+ prop +" value "+ val +"!");
      }
    }
  }
  if (propDeclParams.isStandardId) {
    if (val === undefined) {
      return new MandatoryValueConstraintViolation("A value for the " +
          "standard identifier attribute "+ label +" is required!");
    } else if (this.instances && this.instances[val]) {
      return new UniquenessConstraintViolation("There is already a "+
          this.name +" with a(n) "+ label +" value "+ val +"!");
    }
  }
  return new NoConstraintViolation();
};
/**
 * Generic method for converting rows/records to model objects
 * @method
 * @author Gerd Wagner
 * @param {object} record  The record/row to be converted
 */
eNTITYtYPE.prototype.convertRec2Obj = function (record) {
  var obj={};
  try {
    obj = this.create( record);
  } catch (e) {
    console.log( e.constructor.name + " while deserializing a "+
        this.name +" record: " + e.message);
    obj = null;
  }
  return obj;
};

/**
* @fileOverview  This file contains the definition of the meta-class mODELcLASS.
* @author Gerd Wagner
* @copyright Copyright 2015 Gerd Wagner, Chair of Internet Technology,
*   Brandenburg University of Technology, Germany.
* @license The MIT License (MIT)
*/
/**
 * Meta-class for creating factory classes with a create method for object creation.
 * Adds the following meta-properties to eNTITYtYPE:
 * + attributesToDisplayInLists: the names of attributes for which a column is
 *       to be created in showAll lists/tables
 * TODO: attributesToDisplayInIdRefColumns: the names of attributes for which a
 *       value is to be included in an IdRef columns. By default [:id[,"name"]].
 *
 * @constructor
 * @this {mODELcLASS}
 * @param {{name: string, supertype: string?, supertypes: string?,
 *          properties: Map, 
 *          methods: Map, staticMethods: Map}} slots  The object type definition slots.
 */
function mODELcLASS( slots) {
  var properties = {};
  // Any supertype of a mODELcLASS must be a mODELcLASS
  if (slots.supertype && !(slots.supertype instanceof mODELcLASS)) {
    throw new ModelClassConstraintViolation("Supertype "+ slots.supertype +
        " of mODELcLASS "+ this.name +" is not a mODELcLASS!");
  }
  // invoke the eNTITYtYPE constructor since a mODELcLASS is an eNTITYtYPE
  eNTITYtYPE.call( this, slots);
  // Any root mODELcLASS for persistent objects must have a standard ID attribute
  if (!this.supertype && !this.supertypes && !this.isNonPersistent) {
    if (!this.standardId) {
      throw new ModelClassConstraintViolation("Model class "+ this.name +
      " must define a standard identifier attribute!");
    }
  }
  // attributes to display in lists must be properties of the model class
  properties = this.properties;
  if (slots.attributesToDisplayInLists) {
    if (!slots.attributesToDisplayInLists.every( function (attr) {
      return (properties[attr]);
    })) {
      throw new ModelClassConstraintViolation("There is an attribute " +
        "to display in lists, which is not a property of mODELcLASS "+ 
        this.name +" !");
    } else {
      this.attributesToDisplayInLists = slots.attributesToDisplayInLists;
    }
  }
  // define a property for holding the population in main memory
  this.instances = {};  // a map of all objects of this type
  // add new model class to the mODELcLASS.classes map
  mODELcLASS.classes[this.name] = this;
}
mODELcLASS.prototype = Object.create( eNTITYtYPE.prototype);
mODELcLASS.prototype.constructor = mODELcLASS;
mODELcLASS.classes = {};  // a map of all model classes
/**
 * Factory method for creating new objects as instances of a mODELcLASS.
 * Create new object by assigning the methods defined for the object's type to 
 * the object's prototype and using the property descriptors of the property  
 * declarations of the object's type for defining its property slots.
 *
 * @method 
 * @author Gerd Wagner
 * @param {object} initSlots  The object initialization slots.
 * @return {object}  The new object.
 */
mODELcLASS.prototype.create = function (initSlots) {
  var obj = Object.create( this.methods, this.properties),
      props = this.properties, msg="";
  // add predefined property "type" for direct type
  Object.defineProperty( obj, "type", 
      {value: this, writable: false, enumerable: true});
  // initialize object
  Object.keys( initSlots).forEach( function (prop) {
    var val = initSlots[prop];
    // assign property and check its constraints if defined in object type
    if (props[prop]) obj.set( prop, val);
    else if (prop !== 'type') {
      // the pre-defined 'type' property is ignored
      console.log("Undefined object construction property: "+ prop);
    }
  });
  // object-level validation
  if (this.methods.validate) {
    msg = obj.validate();
    if (msg) throw new ConstraintViolation( this.name +
        obj[this.standardId] +": "+ msg);
  }
  // assign initial value to unassigned properties
  Object.keys( props).forEach( function (prop) {
    if (obj[prop] === undefined &&
        props[prop].initialValue !== undefined) {
      obj[prop] = props[prop].initialValue;
    }
  });
  return obj;
};exports.mODELcLASS = mODELcLASS;exports.util = util;
var Book = new mODELcLASS(
  { name: "Book",
    properties: {
      "isbn": { range:"String", isStandardId: true, label:"ISBN"},
      "title": { range:"String", label:"Title"}, 
      "year": { range:"Integer", label:"Year"}
    }
  }
);
exports.Book = Book;

exports.$appNS = $appNS;

// $appNS, $appNS.model, $appNS.view and $appNS.ctrl are fixed names
// and changing them may cause the application to run incorrect!
var $appNS = { model:{}, view:{}, ctrl:{}};
$appNS.ctrl = {
  // use this flag to enable/disable HTML5 form input auto-validation feature
  validateOnInput: false,
  // optional, used to define the app path (e.g., /nodeapps/myApp), used with proxied app paths
  appPath: "",
  // define the storage manager for this application
  storageManager: new sTORAGEmANAGER( 
    { name: "MariaDB", // required
      host: 'localhost', // optional host, defaults to localhost
      port: 3306, // optional DB engine port, defaults to 3306
      user: 'minimalapp', // required
      database: 'minimalapp', // required
      password: 'minimalapp' // required
    })
   /** additional/custom code comes here, e.g., the createTestData method, etcetera **/
};
/** The following code is injected by the gulp script (create-app task)**/
/** NOTE: changing this code may cause the application to run incorrect! **/
/** Initialize model vies **/
if ( typeof exports != "object") {
  window.addEventListener( "load", mODELvIEW.setupUI);
  /** title creation code: added at application creation time by the gulp script **/
  $appNS.ctrl.title = "MinimalApp";
  document.title = $appNS.ctrl.title;
} else {
  exports.$appNS = $appNS;
}
/************* End: gulp injected code (create-app task) *************/
exports.$appNS = $appNS;
