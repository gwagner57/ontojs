var Book = new mODELcLASS(
  { name: "Book",
    properties: {
      "isbn": { range:"String", isStandardId: true, label:"ISBN"},
      "title": { range:"String", label:"Title"}, 
      "year": { range:"Integer", label:"Year"}
    }
  }
);