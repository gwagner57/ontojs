const app = require( './app.js'),
      mODELcLASS = app.mODELcLASS,
      util = app.util;


/**
 * Process errors that may occur from requests and DB 
 * access and transform them into corresponding HTML codes.
 */
function processErrors( err) {
  /**
   * NOTE:
   * 1) err.code is a string, a kind of cut version 
   *    error message, e.g., ERR_BAD_NULL_ERROR
   * 2) err.errno is a number representing the SQL error
   *    code, e.g. 1048 means column can't be null
   * 3) err.sqlstate is a number representing the SQL state
   *    after an error, e.g., 23000 is used in combination with 
   *    error code 1022, 1048, 1052, for validation errors
   * 
   */
  if ( !err) {
    return { code: 200, details: { message: "OK"}};
  } else if ( err.code === "ECONNREFUSED") {
    err.message = "Service Unavailable";
    return { code: 503, details: err};
  } 
  // SQL integrity constraint validation error
  // Error code 23000 according with: https://docs.oracle.com/cd/B14117_01/appdev.101/a58231/appd.htm
  // '==' not '===' because sometime is '23000' and sometime is 23000 (probably a bug?!...) !
  else if ( err.sqlState == 23000) { 
    err.message = "Bad Request";
    return { code: 400, details: err};
  } else {
    err.message = "Internal Server Error";
    return { code: 500, details: err};
  } 
};   

/**
 * Create the POST routes corresponding to the application model classes. 
 * This corresponds to the 'add' method of the storage manager.
 * @param route 
 *          reference to the router object where to add the routes
 */
function createDefaultPostRoutes( router) {
  if ( !router || typeof router['get'] !== 'function') {
    throw "app.createDefaultPostRoutes: cannot create 'POST' route because the 'router' parameter is null or invalid!";
  }
  Object.keys( mODELcLASS.classes).forEach( function ( className) {
    var classRef = mODELcLASS.classes[className], 
        tableName = util.getTableName( className);
    router.post( '/' + tableName, function ( req, rsp) {
      var slots = {};
      Object.keys( req.body).forEach( function ( pName) {
        slots[pName] = req.body[pName];
      });
      try {
        app.$appNS.ctrl.storageManager['add']( classRef, slots, function ( response, err) {
          var pRes = null; 
          if ( err) {  
            console.log( "Router error on 'createDefaultPostRoutes':");          
            console.log( err);        
            pRes = processErrors( err);
            rsp.status( pRes.code).json( pRes.details);
          } else {
            rsp.status( 201).send( 'OK');
          }
        });
      } catch ( e) {
        console.log( e.constructor.name + ": " + e.message);
      }  
    });
  });
};  

/**
 * Create the PUT routes corresponding to the application model classes. 
 * This corresponds to the 'update' method of storage manager.
 * @param route 
 *          reference to the router object where to add the routes
 */
function createDefaultPutRoutes( router) {
  if ( !router || typeof router['get'] !== 'function') {
    throw "app.createDefaultPutRoutes: cannot create 'PUT' route because the 'router' parameter is null or invalid!";
  }
  Object.keys( mODELcLASS.classes).forEach( function ( className) {
    var classRef = mODELcLASS.classes[className], 
        tableName = util.getTableName( className);
        
    router.put( '/' + tableName + '/:id', function ( req, rsp) {
      var slots = {};
      Object.keys( req.body).forEach( function ( pName) {
        slots[pName] = req.body[pName];
      });
      try {
        app.$appNS.ctrl.storageManager['update']( classRef, req.params.id, slots, function ( obj, err) {
          var pRes = null; 
          if ( err) {   
            console.log( "Router error on 'createDefaultPutRoutes':");          
            console.log( err);        
            pRes = processErrors( err);
            rsp.status( pRes.code).json( pRes.details);
          } else {
            rsp.status( 200).send( 'OK');
          }
        });
      } catch ( e) {
        console.log( e.constructor.name + ": " + e.message);
      }  
    });
  });
}; 

/**
 * Create the DELETE routes corresponding to the application model classes. 
 * This corresponds to the 'destroy' method of the storage manager.
 * @param route 
 *          reference to the router object where to add the routes
 */
function createDefaultDeleteRoutes( router) {
  if ( !router || typeof router['get'] !== 'function') {
    throw "app.createDefaultDeleteRoutes: cannot create 'DELETE' route because the 'router' parameter is null or invalid!";
  }
  Object.keys( mODELcLASS.classes).forEach( function ( className) {
    var classRef = mODELcLASS.classes[className], 
        tableName = util.getTableName( className);
    router.delete( '/' + tableName + '/:id', function ( req, rsp) {
      try {
        app.$appNS.ctrl.storageManager['destroy']( classRef, req.params.id, function ( response, err) {
          var pRes = null; 
          if ( err) { 
            console.log( "Router error on 'createDefaultDeleteRoutes':");          
            console.log( err);          
            pRes = processErrors( err);
            rsp.status( pRes.code).json( pRes.details);
          } else {
            rsp.status( 200).send( 'OK');
          }
        });
      } catch ( e) {
        console.log( e.constructor.name + ": " + e.message);
      }  
    });
  });
};

/**
 * Create the DELETE (-ALL) routes corresponding to the application model classes. 
 * This corresponds to the 'destroy' method of the storage manager.
 * @param route 
 *          reference to the router object where to add the routes
 */
function createDefaultDeleteAllRoutes( router) {
  if ( !router || typeof router['get'] !== 'function') {
    throw "app.createDefaultDeleteAllRoutes: cannot create 'DELETE' route because the 'router' parameter is null or invalid!";
  }
  Object.keys( mODELcLASS.classes).forEach( function ( className) {
    var classRef = mODELcLASS.classes[className], 
        tableName = util.getTableName( className);
    router.delete( '/' + tableName + '/', function ( req, rsp) {
      try {
        app.$appNS.ctrl.storageManager['clearData']( classRef, function ( response, err) {
          var pRes = null; 
          if ( err) { 
            console.log( "Router error on 'createDefaultDeleteAllRoutes':");          
            console.log( err);          
            pRes = processErrors( err);
            rsp.status( pRes.code).json( pRes.details);
          } else {
            rsp.status( 200).send( 'OK');
          }
        });
      } catch ( e) {
        console.log( e.constructor.name + ": " + e.message);
      }  
    });
  });
};
 
/**
 * Create the GET (-ALL) routes corresponding to the application model classes. 
 * This corresponds to the 'retrieveAll' method of the storage manager.
 * @param route 
 *          reference to the router object where to add the routes
 */
function createDefaultGetAllRoutes( router) {
  if ( !router || typeof router['get'] !== 'function') {
    throw "app.createDefaultGetAllRoutes: cannot create 'GET' route because the 'router' parameter is null or invalid!";
  }
  Object.keys( mODELcLASS.classes).forEach( function ( className) {
    var classRef = mODELcLASS.classes[className], 
        tableName = util.getTableName( className);
    router.get( '/' + tableName, function ( req, rsp) {
      try {
        app.$appNS.ctrl.storageManager['retrieveAll']( classRef, function ( response, err) {
          var pRes = null;
          if ( err) {  
            console.log( "Router error on 'createDefaultGetAllRoutes':");          
            console.log( err);
            pRes = processErrors( err);
            rsp.status( pRes.code).json( pRes.details);
          } else {
            rsp.status( 200).json( response);
          }
        });
      } catch ( e) {
        console.log( e.constructor.name + ": " + e.message);
      }  
    });
  });
};

/**
 * Create the GET routes corresponding to the application model classes. 
 * This corresponds to 'retrieve' method of the storage manager.
 * @param route 
 *          reference to the router object where to add the routes
 */
function createDefaultGetRoutes( router) {
  if ( !router || typeof router['get'] !== 'function') {
    throw "app.createDefaultGetRoutes: cannot create 'GET' route because the 'router' parameter is null or invalid!";
  }
  Object.keys( mODELcLASS.classes).forEach( function ( className) {
    var classRef = mODELcLASS.classes[className], 
        tableName = util.getTableName( className);
    router.get( '/' + tableName + '/:id', function ( req, rsp) {
      try {
        app.$appNS.ctrl.storageManager['retrieve']( classRef, req.params.id, function ( response, err) {
          var pRes = null;
          if ( err) {     
            console.log( "Router error on 'createDefaultGetRoutes':");           
            console.log( err);
            pRes = processErrors( err);
            rsp.status( pRes.code).json( pRes.details);
          } else if ( !response) {
            rsp.status( 404).send( "Not Found");
          } else {
            rsp.status( 200).json( response);
          }
        });
      } catch ( e) {
        console.log( e.constructor.name + ": " + e.message);
      }  
    });
  });
};

/**
 * Create all the default routes (getAll, getByStdId, create, update and destroy)
 * corresponding to the application model classes.
 * @param router
 *          reference to the router object where to add the routes
 */
exports.createDefaultRoutes = function( router) {
  createDefaultGetAllRoutes( router);
  createDefaultGetRoutes( router);
  createDefaultDeleteRoutes( router);
  createDefaultPostRoutes( router);
  createDefaultPutRoutes( router);
  // deliver the start page (index.html)
  router.get('/', function (req, res) {
    res.sendFile( 'index.html', {root: './views'});
  });
  // deliver static files
  router.get('/:view', function (req, res) {
    if ( req.params.view === 'app-client.js') {
      res.sendFile( req.params.view, {root: './'});
    } else {
      res.sendFile( req.params.view, {root: './views'});
    }
  });
};