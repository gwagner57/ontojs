CREATE TABLE IF NOT EXISTS books (
  isbn CHAR(10) NOT NULL PRIMARY KEY,
  title VARCHAR(50),
  year SMALLINT CHECK ( year > 1459 AND year <= YEAR( CURDATE()) + 1),
  edition TINYINT DEFAULT 1 CHECK ( edition > 0)
);