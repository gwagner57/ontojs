/**
 * @fileOverview  App-level controller code
 * @author Gerd Wagner
 */
// main namespace and subnamespace definitions
var ex = { model:{}, view:{}, ctrl:{}};
var $appNS = ex;

ex.ctrl = {
  title: "Example App",
  storageManager: new sTORAGEmANAGER(),  // define a localStorage manager
  validateOnInput: false,
  createTestData: function() {
    ex.ctrl.storageManager.add( Person,{personId: 11, name:"Maria Anderson",
        birthDate: new Date("1977-05-05")});
    ex.ctrl.storageManager.add( Person,{personId: 12, name:"Richard Wagner",
        birthDate: new Date("1821-12-05"), deathDate: new Date("1891-12-05")});
    ex.ctrl.storageManager.add( Person,{personId: 13, name:"Tom Sawyer",
        birthDate: new Date("1961-12-05")});
  }
};
