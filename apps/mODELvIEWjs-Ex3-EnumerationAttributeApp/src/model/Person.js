/**
 * @fileOverview  The model class Person with attribute definitions and storage management methods
 * @author Gerd Wagner
 * @copyright Copyright � 2013-2014 Gerd Wagner, Chair of Internet Technology, Brandenburg University of Technology, Germany. 
 * @license This code is licensed under The Code Project Open License (CPOL), implying that the code is provided "as-is", 
 * can be modified to create derivative works, can be redistributed, and can be used in commercial applications.
 */
/**
 * Enumeration types
 * @global
 * 
*/
var GenderEL = new eNUMERATION("GenderEL", ["male","female","undetermined"]);
var MonthEL = new eNUMERATION("MonthEL", 
    ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]);
var MusicPreferencesEL = new eNUMERATION("MusicPreferencesEL",  
        ["Jazz","Classical","Rock","Country","Pop"]);
/**
 * Model class Person 
 * @class
 */
var Person = null;
try {
  Person = new mODELcLASS({
    name: "Person",
    properties: {
      "personId": {range:"NonNegativeInteger", isStandardId: true, label:"Person ID"},
      "name": {range:"NonEmptyString", min: 2, max: 20, label:"Name"},
      "gender": {range: GenderEL, label:"Gender"},
      "favoriteMonth": {range: MonthEL, label:"Favorite month", optional: true},
      "musicPreferences": {range: MusicPreferencesEL, optional: true, 
        maxCard: Infinity, label:"Music preferences"}
    },
    //attributesToDisplayInLists: ["lastName","firstName"],
  });

} catch (e) {
  console.log( e.constructor.name +": "+ e.message);
}
