/**
 * @fileOverview  App-level controller code
 * @author Gerd Wagner
 */
// main namespace and subnamespace definitions
var ex = { model:{}, view:{}, ctrl:{}};
var $appNS = ex;

ex.ctrl = {
  title: "Example App 3",
  // define a localStorage manager
  storageManager: new sTORAGEmANAGER(),
  validateOnInput: false,
  createTestData: function() {
    try {
      ex.ctrl.storageManager.add( Person, {personId: 11, name:"Maria Anderson", gender: GenderEL.FEMALE,
        favoriteMonth: MonthEL.MAY, musicPreferences: [2,3,4,5]});
      ex.ctrl.storageManager.add( Person, {personId: 12, name:"Richard Wagner", gender: GenderEL.MALE,
        musicPreferences: [2,5]});
      ex.ctrl.storageManager.add( Person, {personId: 13, name:"Tom Sawyer", gender: GenderEL.MALE});
    } catch (e) {
      if (e.constructor.name) {
        console.log( e.constructor.name + ": " + e.message);        
      } else {
        console.log( e.message);        
      }
    }
  }
};
