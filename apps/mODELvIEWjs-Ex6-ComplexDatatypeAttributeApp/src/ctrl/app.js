/**
 * @fileOverview  App-level controller code
 * @author Gerd Wagner
 */
// main namespace and subnamespace definitions
var ex = { model:{}, view:{}, ctrl:{}};
var $appNS = ex;

ex.ctrl = {
  title: "Example App 6-0",
  // define a localStorage manager
  storageManager: new sTORAGEmANAGER(),
  validateOnInput: false,
  createTestData: function() {
    try {
      ex.ctrl.storageManager.add( Person, {personId: 11, name:"Maria Anderson",
        address: {addressLine1:"77 Market Street", city:"San Francisco", postalCode:"CA 77235"}});
      ex.ctrl.storageManager.add( Person, {personId: 12, name:"Richard Wagner",
        address:{addressLine1:"123 Beluga Rd.", city:"Kokomo", postalCode:"IN 53421"}});
      ex.ctrl.storageManager.add( Person, {personId: 13, name:"Tom Sawyer",
        address: {addressLine1:"51 Colley Ave.", city:"Norfolk", postalCode:"VA 31189"}});
    } catch (e) {
      if (e.constructor.name) {
        console.log( e.constructor.name + ": " + e.message);        
      } else {
        console.log( e.message);        
      }
    }
  }
};
