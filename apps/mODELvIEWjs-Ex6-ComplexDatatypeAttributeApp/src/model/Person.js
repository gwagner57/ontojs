/**
 * @fileOverview  The model class Person with attribute definitions
 * @author Gerd Wagner
 * @copyright Copyright 2015 Gerd Wagner, Chair of Internet Technology, Brandenburg University of Technology, Germany.
 * @license This code is licensed under The Code Project Open License (CPOL), implying that the code is provided "as-is", 
 * can be modified to create derivative works, can be redistributed, and can be used in commercial applications.
 */
/**
 * Model class Person 
 * @class
 */
var Person = null;
try {
  Person = new mODELcLASS({
    name: "Person",
    properties: {
      "personId": {range:"NonNegativeInteger", isStandardId: true, label:"Person ID"},
      "name": {range:"NonEmptyString", min: 2, max: 20, label:"Name"},
      "address": {range: Address, label:"Address"}
    },
    //attributesToDisplayInLists: ["lastName","firstName"],
  });

} catch (e) {
  console.log( e.constructor.name +": "+ e.message);
}
