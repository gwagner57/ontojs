/**
 * @fileOverview  The model class Book with attribute definitions and storage management methods
 * @author Gerd Wagner
 * @copyright Copyright � 2013-2014 Gerd Wagner, Chair of Internet Technology, Brandenburg University of Technology, Germany. 
 * @license This code is licensed under The Code Project Open License (CPOL), implying that the code is provided "as-is", 
 * can be modified to create derivative works, can be redistributed, and can be used in commercial applications.
 */
/**
 * Object type Book 
 * @class
 */
try {

Book = new mODELcLASS({
  name: "Book",
  properties: {
    "isbn": {range:"NonEmptyString", isStandardId: true, label:"ISBN", pattern:/\b\d{9}(\d|X)\b/, 
        patternMessage:'The ISBN must be a 10-digit string or a 9-digit string followed by "X"!'},
    "title": {range:"NonEmptyString", min: 2, max: 50, label:"Title"}, 
    "year": {range:"Integer", min: 1459, max: util.nextYear(), label:"Year"},
    "edition": {range:"PositiveInteger", optional: true, label:"Edition"}
  },
  attributesToDisplayInLists: ["title"],
  storageAdapter: {
    name:'Parse-REST-API',
    predefinedAttributes: ["objectId","createdAt","updatedAt"],
    applicationId: "m9kU3Qj41JHn0FJvixCUK7yxAcwJxp9R0btxfIY6",
    restApiKey: "NyQt8WpyaORnCVbArqhYsjav9FQlPMnGDuja1cIy"
  }
/*
  adapter: {
    name:'Parse-REST-API',
    predefinedAttributes: ["objectId","createdAt","updatedAt"],
    applicationId: "m9kU3Qj41JHn0FJvixCUK7yxAcwJxp9R0btxfIY6",
    restApiKey: "NyQt8WpyaORnCVbArqhYsjav9FQlPMnGDuja1cIy"
  }
*/
});

} catch (e) {
  console.log( e.constructor.name +": "+ e.message);
}
