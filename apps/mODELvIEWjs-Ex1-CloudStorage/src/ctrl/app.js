/**
 * @fileOverview  App-level controller code
 * @author Gerd Wagner
 */
// main namespace and subnamespace definitions
var pl = { model:{}, view:{}, ctrl:{}};

var $appNS = pl;

pl.ctrl = {
  title: "Public Library",
  storageManager: new sTORAGEmANAGER({
    name:"ParseRestAPI",
    predefinedAttributes: ["objectId","createdAt","updatedAt"],
    applicationId: "...", // your Parse Application Id
    restApiKey: "..."  // your Parse Rest Api Key
  }),
  validateOnInput: false,
  createTestData: function () {
    try {
      pl.ctrl.storageManager.add( Book, {isbn: "006251587X",
        title: "Weaving the Web", year: 2000, edition: 2});
      pl.ctrl.storageManager.add( Book, {isbn: "0465026567",
        title: "Gödel, Escher, Bach", year: 1999});
      pl.ctrl.storageManager.add( Book, {isbn: "0465030793",
        title: "I Am A Strange Loop", year: 2008});
      //Book.saveAll();
    } catch (e) {
      if (e.constructor.name) {
        console.log( e.constructor.name + ": " + e.message);        
      } else {
        console.log( e.message);        
      }
    }
  }
};
