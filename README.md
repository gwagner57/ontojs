# mODELcLASSjs, mODELvIEWjs and oNTOjs #

This project includes three (related) model-based JavaScript libraries/frameworks: mODELcLASSjs, mODELvIEWjs and oNTOjs. 

***mODELcLASSjs*** can be used for the model layer of a MVC JavaScript web app, which may be built, for instance, with the help of a view-oriented framework such as *AngularJS*, *KnockoutJS* or *ReactJS*. It provides a meta-class for creating JavaScript model classes (like `Book`, `Author` and `Publisher`) with declarative constraint validation, storage management, and the option of multiple inheritance and object pools. See [this tutorial](http://web-engineering.info/tech/JsFrontendApp/mODELcLASS-validation-tutorial.html) for more. 

mODELcLASSjs is based on the following libraries and metaclasses of the oNTOjs framework: errorTypes.js, util.js, dom.js, xhr.js, eNUMERATION.js, eNTITYtYPE.js, sTORAGEmANAGER.js and mODELcLASS.js.

Currently, the following storage technologies are supported by mODELcLASSjs: 

1. local storage with the Local Storage API, 
2. remote storage with the Parse.com cloud storage service via XHR, 
3. remote storage with MariaDB/MySQL/NodeJS via XHR. 

Support of local storage with IndexedDB as well as support for other storage technologies and object pools will come soon.

***mODELvIEWjs*** is based on mODELcLASSjs and provides a meta-class vIEWcLASS for creating view classes (like `BookView`, `AuthorView` and `PublisherView`), which are then instantiated by CRUD views (representing "view models"). mODELvIEWjs supports the automated creation of various forms of model-based user interfaces for data management operations with declarative and responsive constraint validation. It helps to avoid much of the boilerplate code typically needed for the user interface.

See the [overview slides](https://oxygen.informatik.tu-cottbus.de/webeng/slides/mODELvIEWjs).

***oNTOjs*** is based on the ***Unified Foundational Ontology*** ([UFO](http://ufo-ontology.info)). It allows the definition of kinds (like `Person`) and roles (like `Author` and `Publisher`) with multiple and dynamic role classification. Important UFO type categories are implemented in the form of JavaScript meta-classes, such as eNTITYtYPE, oBJECTtYPE, eVENTtYPE and aGENTtYPE. An overview of these meta-classes is given in the following diagram:

![The oNTOjs meta-model](Overview.png)

### Contribution guidelines ###

If you would like to join this project and help, e.g., with writing tests, please contact G.Wagner (at) b-tu.de