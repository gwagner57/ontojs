const argv = require( 'yargs').argv,
    gulp = require( 'gulp'),
    jshint = require('gulp-jshint'),	
    gulpif = require( 'gulp-if'),
    concat = require( 'gulp-concat'),
    replace= require( 'gulp-replace'),
    merge = require( 'gulp-merge'),
    notify = require( 'gulp-notify'),
    uglify = require( 'gulp-uglify'),
    gulpSeq = require('gulp-sequence'),
    tap = require( 'gulp-tap'),
    path = require( 'path');
  
/**
 * Create and return an array containing all the source
 * files which are needed for/from the core. It DOES NOT 
 * include client specific core files, like the one responsible
 * for view creation and processing
 * @return an array with all the files (relative path to gulp script) 
 */
function getCoreSrc() {
  return [
    "lib/browserShims.js",
    "lib/util.js",
    "lib/errorTypes.js",
    "src/storage/sTORAGEmANAGER.js",
    "src/storage/sTORAGEmANAGER_MariaDB.js",
    "src/eNUMERATION.js",
    "src/cOMPLEXtYPE.js",
    "src/cOMPLEXdATAtYPE.js",
    "src/eNTITYtYPE.js",
    "src/mODELcLASS.js",
  ]
};  

/**
 * Create and return an array containing all the source
 * files which are needed for/from the core. It include 
 * client specific core files, like the one responsible
 * for view creation and processing
 * @return an array with all the files (relative path to gulp script) 
 */
function getClientCoreSrc() {
  return getCoreSrc().concat ([
    "src/ui/mODELvIEW.js",
    "src/ui/vIEWcLASS.js"
  ]);
};  

/**
 * Create and return an array containing all the source
 * library files which are needed by the client side. 
 * @return an array with all the files (relative path to gulp script) 
 */
function getClientLibSrc() {
  return [
    "lib/xhr.js",
    "lib/dom.js",
  ]
}; 

/**
 * Create and return an array containing all the source
 * files which represents the model class files.
 * @param path
 *          the application path (where the source files
 *          are to be found)
 * @return an array with all the model files (relative 'path' 
 *         parameter) created for the custom application.
 */
function getAppModelSrc( path) {
  return [
    path + "model/*.js"
  ]
};  

/** take care of core files processing */
gulp.task( 'build-node-core', function () {
  var appPath = argv.appfolder || './',
      src = getCoreSrc();
  return gulp.src( src)
    .pipe( concat( 'app.js'))
    .pipe( gulpif( argv.production, uglify()))
    .pipe( gulp.dest( appPath + 'dist/'))
    .pipe( notify( { message: '"Process core files" task completed.' }));
});

/** take care of app model files processing */
gulp.task( 'build-node-model', function () {
  var appPath = argv.appfolder || './',
      destFileName = 'app';
  return gulp.src( [ appPath + "dist/app.js"].concat( getAppModelSrc( appPath)))
    .pipe( tap( function ( file, t) {
        var fName = path.basename( file.path, '.js');
        if ( fName !== destFileName) {
          file.contents = Buffer.concat([
            file.contents,
            new Buffer( '\r\nexports.' + fName + ' = ' + fName + ';\r\n')
          ]);
        } else {
          file.contents = Buffer.concat([
            new Buffer( '\r\nvar mariaDBConnector = require( "mysql");'),
            file.contents,
            new Buffer( '\r\exports.mODELcLASS = mODELcLASS;'),
            new Buffer( '\r\exports.util = util;')
          ]);
        }
     }))
    .pipe( concat( destFileName + '.js'))
    .pipe( gulpif( argv.production, uglify()))
    .pipe( gulp.dest( appPath + 'dist/'))
    .pipe( notify( { message: '"Process model files" task completed.' }));
});

/** take care of ctrl files processing */
gulp.task( 'build-node-ctrl', function () {
  var appPath = argv.appfolder || './',
      destFileName = 'app';
  return gulp.src( [ appPath + "dist/app.js"].concat( appPath + '/bootstrap.js'))
    .pipe( tap( function ( file, t) {
      file.contents = Buffer.concat([
        file.contents,
        new Buffer( '\r\nexports.$appNS = $appNS;\r\n')
      ]);
     }))
    .pipe( concat( destFileName + '.js'))
    .pipe( gulpif( argv.production, uglify()))
    .pipe( gulp.dest( appPath + 'dist/'))
    .pipe( notify( { message: '"Process ctrl files" task completed.' }));
});

/** take care of app server files processing */
gulp.task( 'build-node-server', function () {
  var appPath = argv.appfolder || './',
      src = [appPath + 'start.js', './node/builder/router.js'];
  return gulp.src( src)
    .pipe( gulpif( argv.production, uglify()))
    .pipe( gulp.dest( appPath + 'dist/'))
    .pipe( notify( { message: '"Process server files" task completed.' }));
});

/** take care of views related files processing */
gulp.task( 'build-node-views', function () {
  var appPath = argv.appfolder || './',
      css = [
        'dist/mODELvIEWjs/css/normalize.css',
        'dist/mODELvIEWjs/css/vIEW.css'
      ],
      html = [
        './node/builder/views/index.html'
      ],
      media = [
        './node/builder/views/favicon.ico'
      ];
  return gulp.src( html.concat( css).concat( media))
    .pipe( gulp.dest( appPath + 'dist/views'))
    .pipe( notify( { message: '"Process view files" task completed.' }));
});

/** take care of pre-processing of the model files delivered to client */
// THIS task is temporarily not used, just ignore it but do NOT delete it!
gulp.task( 'build-client-preprocess-model', function () {
  var appPath = argv.appfolder || './',
      src = getAppModelSrc( appPath);
  return gulp.src( src)
    .pipe( concat( 'app-client.js'))
    .pipe( gulpif( argv.production, uglify()))
    .pipe( gulp.dest( appPath + 'dist/'))
    .pipe( notify( { message: '"Process client files" task completed.' }));
});

/** take care of the files delivered to client */
gulp.task( 'build-client-combine-files', function () {
  var appPath = argv.appfolder || './',
      src = getClientLibSrc().concat( getClientCoreSrc());
      src.push(appPath + 'dist/' + 'app-client.js');
      src.push( appPath + '/bootstrap.js');
  return gulp.src( src)
    .pipe( tap( function ( file, t) {
        var fName = path.basename( file.path, '.js'), 
            content = '', startIndex = 0, endIndex = 0
            i = 0, openQb = 0, closedQb = 0;
        if ( fName === "bootstrap") {
          content = file.contents.toString();
          // replace "privat" data of the storage manager
          startIndex = content.indexOf( "sTORAGEmANAGER") + 14;
          for ( i = startIndex; i < content.length; i++) {
            if ( content.charAt( i) === '(') openQb++;
            if ( content.charAt( i) === ')') closedQb++;
            if ( openQb === closedQb && openQb !== 0)  {
              endIndex = i + 1;
              break;
            }
          }
          content = content.substring( 0, startIndex) 
            + "({ name: \"MariaDB\"})\r\n" + content.substring( endIndex, content.length);
          file.contents = new Buffer( content);
        } 
     }))
    .pipe( concat( 'app-client.js'))
    .pipe( gulpif( argv.production, uglify()))
    .pipe( gulp.dest( appPath + 'dist/'))
    .pipe( notify( { message: '"Process client files" task completed.' }));
});

/** take care of building the client application distribution */
gulp.task( 'build-client', gulpSeq( 'build-client-preprocess-model', 'build-client-combine-files'));

/** take care of creating the node application distribution */
gulp.task( 'build-node', gulpSeq( 'build-node-core', 'build-node-model', 'build-node-ctrl', 'build-node-server', 'build-node-views'));

/**
 * Build the minimal structure for the application. 
 * Call this task when a new application is to be created
 * so it takes care of creating the minimal structure 
 * and template files required by the application.
 *
 * Parameters:
 *  --appfolder: the path to the folder of application to build
 *               (defaults to current folder!!!, and requires the trailing slash)
 *  --appname: the title/name of the application to be used 
 *             as the application root folder, HTML page title 
 *             and any other places where this is required. 
 *             (defaults to: 'myNewApp')
 * NOTE: be sure that a sub-folder with name provided by 'appname' and 
 *       does not exists in the 'appfolder' folder, otherwise the content 
 *       of the existing application is mixed with the new one!
 *
 * USAGE example: gulp create-app --appfolder="./apps/" --appname="node-mODELcLASS-ValidationAPP"
 */
gulp.task( 'create-app', function () {
  var appPath = argv.appfolder || './',
      appName = argv.appname || 'myNewApp',
      srcViews = [
        './node/builder/views/index.html',
        './node/builder/views/favicon.ico',
        './dist/mODELvIEWjs/css/normalize.css',
        './dist/mODELvIEWjs/css/vIEW.css'
      ],
      srcServerCode = [ 
        './node/builder/bootstrap.js',
        './node/builder/router.js',
        './node/builder/start.js'
      ],
      srcModelCode = [
        './node/builder/model/readme.md'
      ],
      srcServerCodePipe = gulp.src( srcServerCode)
        .pipe( tap( function ( file, t) {
          var fName = path.basename( file.path, '.js');
          if ( fName === "bootstrap") {
            file.contents = Buffer.concat([
              file.contents,
              new Buffer( '\r\n/** The following code is injected by the gulp script (create-app task)**/'),
              new Buffer( '\r\n/** NOTE: changing this code may cause the application to run incorrect! **/'),
              new Buffer( '\r\n/** Initialize model vies **/'),
              new Buffer( '\r\nif ( typeof exports != "object") {'),
              new Buffer( '\r\n  window.addEventListener( "load", mODELvIEW.setupUI);'),
              new Buffer( '\r\n  /** title creation code: added at application creation time by the gulp script **/'),
              new Buffer( '\r\n  $appNS.ctrl.title = "' + appName + '";'),
              new Buffer( '\r\n  document.title = $appNS.ctrl.title;'),
              new Buffer( '\r\n} else {'),
              new Buffer( '\r\n  exports.$appNS = $appNS;\r\n}'),
              new Buffer( '\r\n/************* End: gulp injected code (create-app task) *************/')
            ]);
          }
         }))
        .pipe( gulp.dest( appPath + '/' + appName + '/'))
        .pipe( notify( { message: ' The application "' + appPath + '/' + appName + '/' + '" source code was created.' })),
      srcViewsPipe = gulp.src( srcViews)
        .pipe( gulp.dest( appPath + '/' + appName + '/views/'))
        .pipe( notify( { message: ' The application "' + appPath + '/' + appName + '/' + '" views were created.' })),
      srcModelPipe = gulp.src( srcModelCode)
        .pipe( gulp.dest( appPath + '/' + appName + '/model/'))
        .pipe( notify( { message: ' The application "' + appPath + '/' + appName + '/' + '" model folder was created.' }));
  return merge( srcServerCodePipe, srcViewsPipe, srcModelPipe);
});

/** 
 * Build the application distribution. It depends on several other 
 * tasks which are NOT supposed to be called manually.
 * Please call this task to create a distribution and not the 
 * sub-sub tasks otherwise the result may not work properly.
 *
 * Parameters:
 *  --production : uses 'uglify' over the application source code
 *  --appfolder : the path to the folder of application to build
 *                 (defaults to current folder, and requires the trailing slash)
 *               
 * USAGE example: 
 *   i) gulp build --appfolder="./apps/node-mODELcLASS-ValidationAPP/" 
 *  ii) gulp build --appfolder="./apps/node-mODELcLASS-ValidationAPP/" --production
 */
gulp.task( 'build', gulpSeq( 'build-client', 'build-node'));

/***************************************************************************************/
/***************************** ADDITIONAL TASKS ****************************************/
/***************************************************************************************/
gulp.task('dev', function () {
  gulp.src(["lib/browserShims.js","lib/util.js",'lib/dom.js','lib/xhr.js', "lib/errorTypes.js",
    "src/eNUMERATION.js",
    "src/cOMPLEXtYPE.js",
    "src/cOMPLEXdATAtYPE.js",
    "src/eNTITYtYPE.js",
    "src/mODELcLASS.js",
    "src/storage/sTORAGEmANAGER.js",
    "src/ui/vIEWcLASS.js", 
    "src/ui/mODELvIEW.js"
  ])
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'));
});
gulp.task( 'build-modelclassjs', function () {
  var src = [
    'lib/browserShims.js',
    'lib/util.js', 
    'lib/dom.js', 
    'lib/xhr.js', 
    'lib/errorTypes.js', 
    'src/eNUMERATION.js', 
    'src/cOMPLEXtYPE.js',
    'src/cOMPLEXdATAtYPE.js',
    'src/eNTITYtYPE.js', 
    'src/mODELcLASS.js',
    'src/storage/sTORAGEmANAGER.js'
  ];
  return gulp.src( src)
    .pipe( concat( 'mODELcLASSjs.min.js'))
    .pipe( uglify())
    .pipe( gulp.dest( 'dist/mODELcLASSjs/'))
    .pipe( notify( { message: '"Build mODELcLASSjs.min.js" task completed.' }));
});

gulp.task( 'build-modelviewjs', function () {
  var src = [
    'lib/browserShims.js', 
    'lib/util.js', 
    'lib/dom.js', 
    'lib/xhr.js', 
    'lib/errorTypes.js', 
    'src/eNUMERATION.js', 
    'src/cOMPLEXtYPE.js',
    'src/cOMPLEXdATAtYPE.js',
    'src/eNTITYtYPE.js', 
    'src/mODELcLASS.js', 
    'src/storage/sTORAGEmANAGER.js', 
    'src/ui/vIEWcLASS.js', 
    'src/ui/mODELvIEW.js'
  ];
  return gulp.src( src)
    .pipe( concat( 'mODELvIEWjs.min.js'))
    .pipe( uglify())
    .pipe( gulp.dest( 'dist/mODELvIEWjs/'))
    .pipe( notify( { message: '"Build mODELcLASSjs.min.js" task completed.' }));
});

gulp.task( 'build-ontojs', function () {
  var src = [
    'lib/browserShims.js',
    'lib/util.js', 
    'lib/dom.js', 
    'lib/errorTypes.js', 
    'src/oBJECTtYPE.js',
    'src/aGENTtYPE.js',
    'src/cOMPLEXdATAtYPE.js', 
    'src/cOMPLEXtYPE.js', 
    'src/eNTITYtYPE.js', 
    'src/eNUMERATION.js', 
    'src/eVENTtYPE.js', 
    'src/ui/oBJECTvIEW.js'
  ];
  return gulp.src( src)
    .pipe( concat( 'oNTOjs.min.js'))
    .pipe( uglify())
    .pipe( gulp.dest( 'dist/oNTOjs/'))
    .pipe( notify( { message: '"Build oNTOjs.min.js" task completed.' }));
});


