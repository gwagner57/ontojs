﻿/**
 * @fileOverview  This file contains the definition of the meta-class cOMPLEXdATAtYPE.
 * @author Gerd Wagner
 * @copyright Copyright 2015 Gerd Wagner, Chair of Internet Technology,
 *   Brandenburg University of Technology, Germany.
 * @license The MIT License (MIT)
 */
 /**
 * Meta-class for creating (complex) datatypes with a create method.
 * cOMPLEXdATAtYPE defines the following type features:
 * name, supertype(s), properties, methods (including the
 * predefined methods toDisplayString, toRecord, set and isInstanceOf)
 * TODO: staticMethods
 *
 * By default, properties are mandatory, if there is no declaration slot
 * "optional:true". The property declaration parameter "minCard" is only
 * meaningful for multi-valued properties, for which "maxCard" has been
 * set to a value greater than 1.
 *
 * On datatype creation, the constructor argument "methods" may include
 * a custom data-value-level "validate" method returning an error message 
 * in case of an data-value-level constraint violation.
 *
 * @constructor
 * @this {cOMPLEXdATAtYPE}
 * @param {{name: string, supertype: string?, supertypes: string?,
 *          properties: Map, methods: Map?, staticMethods: Map?,
 *          reactionRules: Map?, internalEventTypes: Map?}}
 *        slots  The entity type definition slots.
 */
function cOMPLEXdATAtYPE( slots) {
  var k=0, keys=[], supertype=null,
      featureMaps = {properties:{}, methods:{}, staticMethods:{}};

  // Any supertype of a datatype must be a datatype
  if (slots.supertype && !(slots.supertype instanceof cOMPLEXdATAtYPE)) {
    throw new cOMPLEXdATAtYPEconstraintViolation("Supertype "+ slots.supertype +
        " of cOMPLEXdATAtYPE "+ this.name +" is not a cOMPLEXdATAtYPE!");
  }
  // Check datatype-specific constraints for property declarations
  Object.keys( slots.properties).forEach( function (prop) {
    var propDeclParams = slots.properties[prop];  // the property declaration slots
    // A datatype does not have an ID attribute
    if (propDeclParams.isStandardId || prop === "id")
      throw new cOMPLEXdATAtYPEconstraintViolation("A datatype must not have an ID attribute");
    // check if datatype property has a meaningful range
    if (!propDeclParams.range)
      throw new cOMPLEXdATAtYPEconstraintViolation(
          "There must be a range definition for "+ prop +" !");
    if (eNTITYtYPE && propDeclParams.range instanceof eNTITYtYPE)
      throw new cOMPLEXdATAtYPEconstraintViolation(
          "A datatype property must not have an entity type as range!");
  }, this);

  // invoke the cOMPLEXtYPE constructor since a cOMPLEXdATAtYPE is a cOMPLEXtYPE
  cOMPLEXtYPE.call( this, slots);

  // add predefined methods such that they will be inherited by all subtypes
  if (!slots.supertype && !slots.supertypes) {  // root class (no inheritance)
    // add predefined methods such that they will be inherited by all subtypes
    // ***** toDisplayString **********************
    this.methods.toDisplayString = function () {
      var str="";
      Object.keys( this).forEach( function (p) {
        if (p !== "type" && this[p] !== undefined) {
          str += this.getValAsString( p) + ", ";
        }
      }, this);  // pass context object reference
      return str;
    };
    // **************************************************
    this.methods.getValAsString = function (p) {
    // **************************************************
      var obj = this, valuesToConvert=[], displayStr="", k=0,
          listSep = ", ",
          propDecl = obj.type.properties[p],
          range = propDecl.range,
          val = obj[p];
      if (val === undefined || val === null) return "";
      if (propDecl.maxCard && propDecl.maxCard > 1) {
        if (Array.isArray( val)) {
          valuesToConvert = val.slice(0);  // clone;
        } else console.log("The value of a multi-valued " +
            "datatype property must be an array!");
      } else valuesToConvert = [val];
      valuesToConvert.forEach( function (v,i) {
        if (range instanceof eNUMERATION) {
          valuesToConvert[i] = range.labels[v-1];
        } else if (["number","string","boolean"].includes( typeof v) || !v) {
          valuesToConvert[i] = String( v);
        } else if (range === "Date") {
          valuesToConvert[i] = util.createIsoDateString( v);
        } else if (Array.isArray( v)) {  // JSON-compatible array
          valuesToConvert[i] = v.slice(0);  // clone
        } else valuesToConvert[i] = JSON.stringify( v);
      });
      displayStr = valuesToConvert[0];
      if (propDecl.maxCard && propDecl.maxCard > 1) {
        displayStr = "[" + displayStr;
        for (k=1; k < valuesToConvert.length; k++) {
          displayStr += listSep + valuesToConvert[k];
        }
        displayStr = displayStr + "]";
      }
      return displayStr;
    };
    // **************************************************
    this.methods.toRecord = function () {
    // **************************************************
      var obj = this, rec={}, propDecl={}, valuesToConvert=[], range, val;
      Object.keys( obj).forEach( function (p) {
        if (p !== "type" && obj[p] !== undefined) {
          val = obj[p];
          propDecl = obj.type.properties[p];
          range = propDecl.range;
          if (propDecl.maxCard && propDecl.maxCard > 1) {
            if (Array.isArray( val)) {
              valuesToConvert = val.slice(0);  // clone;
            } else console.log("The value of a multi-valued " +
                "datatype property must be an array!");
          } else valuesToConvert = [val];
          valuesToConvert.forEach( function (v,i) {
            // alternatively: enum literals as labels
            // if (range instanceof eNUMERATION) rec[p] = range.labels[val-1];
            if (["number","string","boolean"].includes( typeof(v)) || !v) {
              valuesToConvert[i] = String( v);
            } else if (range === "Date") {
              valuesToConvert[i] = util.createIsoDateString( v);
            } else if (Array.isArray( v)) {  // JSON-compatible array
              valuesToConvert[i] = v.slice(0);  // clone
            } else valuesToConvert[i] = JSON.stringify( v);
          });
          if (!propDecl.maxCard || propDecl.maxCard <= 1) {
            rec[p] = valuesToConvert[0];
          } else {
            rec[p] = valuesToConvert;
          }
        }
      });
      return rec;
    };
  }
  // add new datatype to the cOMPLEXdATAtYPE.types map
  cOMPLEXdATAtYPE.types[this.name] = this;
}

cOMPLEXdATAtYPE.prototype = Object.create( cOMPLEXtYPE.prototype);
cOMPLEXdATAtYPE.prototype.constructor = cOMPLEXdATAtYPE;

cOMPLEXdATAtYPE.types = {};  // a map of all datatypes

/**
 * Factory method for creating new complex data values as instances
 * of a cOMPLEXdATAtYPE.
 *
 * Create new complex data value by assigning the methods defined
 * for the cOMPLEXdATAtYPE to the data value's prototype and using the
 * property descriptors of the property declarations of the
 * cOMPLEXdATAtYPE for defining its property slots.
 *
 * @method
 * @author Gerd Wagner
 * @param {object} initSlots  The object initialization slots.
 * @return {object}  The new object.
 */
cOMPLEXdATAtYPE.prototype.create = function (initSlots) {
  var dv = Object.create( this.methods, this.properties),
      props = this.properties, msg="";
  // add predefined property "type" for direct type
  Object.defineProperty( dv, "type",
      {value: this, writable: false, enumerable: true});
  // initialize object
  Object.keys( initSlots).forEach( function (prop) {
    var val = initSlots[prop];
    // check property constraints only when property is defined in cOMPLEXdATAtYPE
    if (props[prop]) dv.set( prop, val);
    else if (prop !== 'type') {  // 'type' is ignored
      console.log("Undefined property: "+ prop);
    }
  });
  // validation at the aggregate level of the complex data value
  if (this.methods.validate) {
    msg = dv.validate();
    if (msg) throw new ConstraintViolation( this.name +
        dv.getValAsString +": "+ msg);
  }
  // assign initial value to unassigned properties
  Object.keys( props).forEach( function (prop) {
    if (dv[prop] === undefined &&
        props[prop].initialValue !== undefined) {
      dv[prop] = props[prop].initialValue;
    }
  });
  return dv;
};