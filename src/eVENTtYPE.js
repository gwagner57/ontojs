/**
 * Meta-class for creating event types as factory classes.
 * periodicity
 * @copyright Copyright 2014-2015 Gerd Wagner, Chair of Internet Technology,
 *   Brandenburg University of Technology, Germany.
 * @license The MIT License (MIT)
 * @author Gerd Wagner
 * @constructor
 * @this {eVENTtYPE}
 * @param {{name: string, supertype: string?, supertypes: string?, 
 *          properties: Map, methods: Map}} slots  The object type definition slots.
 */
function eVENTtYPE( slots) {
  // Any supertype of an eVENTtYPE must be an eVENTtYPE
  if (slots.supertype) {
    if (typeof( slots.supertype)==="string") {
      if (eVENTtYPE.types[slots.supertype]) {
        slots.supertype = eVENTtYPE.types[slots.supertype];
      } else {
        throw new ConstraintViolation("Supertype "+ slots.supertype +" of eVENTtYPE "+
            slots.name +" has not been defined!");
      }
    }
    if (!(slots.supertype instanceof eVENTtYPE)) {
      throw new ConstraintViolation("Supertype "+ slots.supertype +" of eVENTtYPE "+
          this.name +" is not an eVENTtYPE!");
    }
  }
  // invoke the eNTITYtYPE constructor since an eVENTtYPE is an eNTITYtYPE
  eNTITYtYPE.call( this, slots);
  // define a property for holding the population in main memory
  this.instances = {};  // a map of all events of this type
  // add new event type to the eVENTtYPE.types map
  eVENTtYPE.types[this.name] = this;
};
eVENTtYPE.prototype = Object.create( eNTITYtYPE.prototype);
eVENTtYPE.prototype.constructor = eVENTtYPE;
eVENTtYPE.types = {};  // a map of all event types
/**
 * Factory method for creating new events as instances of an eVENTtYPE.
 * Create new event by assigning the methods defined for the event's type to
 * the event's prototype and using the property descriptors of the property
 * declarations of the event's type for defining its property slots.
 *
 * @method
 * @author Gerd Wagner
 * @param {object} initSlots  The initialization slots.
 * @return {object}  The new event.
 */
eVENTtYPE.prototype.create = function (initSlots) {
  var evt = Object.create( this.methods, this.properties),
      props = this.properties, msg="";
  // add predefined property "type" for the direct type of an event
  Object.defineProperty( evt, "type",
      {value: this, writable: false, enumerable: true});
  // initialize object
  Object.keys( initSlots).forEach( function (prop) {
    var val = initSlots[prop];
    // assign property and check its constraints if defined in object type
    if (props[prop]) evt.set( prop, val);
    else if (prop !== 'type') {
      // the pre-defined 'type' property is ignored
      console.log("Undefined object construction property: "+ prop);
    }
  });
  // event-level validation
  if (this.methods.validate) {
    msg = evt.validate();
    if (msg) throw new ConstraintViolation( this.name +
        evt[this.standardIdAttr] +": "+ msg);
  }
  // assign initial value to unassigned properties
  Object.keys( props).forEach( function (prop) {
    if (evt[prop] === undefined &&
        props[prop].initialValue !== undefined) {
      evt[prop] = props[prop].initialValue;
    }
  });
  return evt;
};