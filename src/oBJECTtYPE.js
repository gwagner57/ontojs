/**
 * Meta-class for creating object types as factory classes with a create method 
 * for object creation. oBJECTtYPE defines the meta-property "instances", but does 
 * not define a isStandardId.
 * @copyright Copyright 2014 Gerd Wagner, Chair of Internet Technology, 
 *   Brandenburg University of Technology, Germany.
 * @license The MIT License (MIT)
 * @author Gerd Wagner
 * @constructor
 * @this {oBJECTtYPE}
 * @param {{name: string, supertype: string?, supertypes: string?, 
 *          properties: Map, 
 *          methods: Map, staticMethods: Map}} slots  The object type definition slots.
 *
 * TODO: New features for managing events in the context of observer objects:
 * (preferably return context entity, so calls can be chained)
 * + addEventListener( eventTypeName,[ targetObject,] listenerMethod)
 *       If no targetObject provided: targetObject = observerObject
 * + removeEventListener( eventTypeName,[ targetObject,] listenerMethod)
 * + eventListeners( eventTypeName)
 *       Returns an array of listeners for the specified event type.
 * + emitEvent( eventTypeName[, arg1][, arg2][, ...])
 *       Execute all listeners in order with the supplied arguments.
 * + Built-in events:
 *   + Object lifecycle events (Creation, Change, Destruction)
 *   + Collection events (Addition, Removal)
 *   + I/O events ...?
 */
function oBJECTtYPE( slots) {
  // Any supertype of an oBJECTtYPE must be an oBJECTtYPE
  if (slots.supertype) {
    if (typeof( slots.supertype)==="string") {
      if (oBJECTtYPE.types[slots.supertype]) {
        slots.supertype = oBJECTtYPE.types[slots.supertype];
      } else {
        throw new ConstraintViolation("Supertype "+ slots.supertype +" of oBJECTtYPE "+
            slots.name +" has not been defined!");
      }
    }
    if (!(slots.supertype instanceof oBJECTtYPE)) {
      throw new ConstraintViolation("Supertype "+ slots.supertype +" of oBJECTtYPE "+
          this.name +" is not an oBJECTtYPE!");
    }
  }
  // if a category has been defined, check the implied constraints
  if (slots.category !== undefined) {
    if (["kIND","rOLE","pHASEtYPE"].indexOf( slots.category) === -1) {
      throw new ObjectTypeConstraintViolation("oBJECTtYPE "+ this.name +
      " is defined with an invalid category: "+ slots.category);          
    }
    switch (slots.category) {
    case "kIND": 
      if (slots.supertype && !slots.supertype.category === "kIND") {
        throw new KindConstraintViolation("Supertype "+ slots.supertype +
            " of kIND "+ this.name +" must be a kIND!");
      } else if (slots.supertypes) {
        throw new KindConstraintViolation("kIND "+ this.name +
            " must not specialize more than one other kind!");        
      }
      break;
    case "rOLE":
      if (!slots.supertype && !slots.supertypes) {
        throw new RoleConstraintViolation("Role "+ this.name +
           " must have a supertype!");   
      } else if (false) {  //TODO if there is no kind among the supertypes
        throw new RoleConstraintViolation("Role "+ this.name +
            " must have a kind as supertype!");
      }
      break;
    }
    this.category = slots.category;
  }
  // invoke the eNTITYtYPE constructor since an oBJECTtYPE is an eNTITYtYPE
  eNTITYtYPE.call( this, slots);
  this.instances = {};  // a map of all objects of this type
  // add new object type to the oBJECTtYPE.types map
  oBJECTtYPE.types[this.name] = this;
};
oBJECTtYPE.prototype = Object.create( eNTITYtYPE.prototype);
oBJECTtYPE.prototype.constructor = oBJECTtYPE;
oBJECTtYPE.types = {};  // a map of all object types
/**
 * Factory method for creating new objects as instances of an oBJECTtYPE.
 * @method 
 * @author Gerd Wagner
 * @param {object} initSlots  The object initialization slots.
 * @return {object}  The new object.
 */
oBJECTtYPE.prototype.create = function (initSlots) {
  var obj=null, k=0, keys=[], key="", 
      directTypePropName="",  // "type" or "kind"
      constraintViolation=null;
  // create object by assigning the methods defined for the object's type to the object's prototype
  // and using the property descriptors of the property declarations of the object's type for defining 
  // its property slots
  obj = Object.create( this.methods, this.properties);
  // add predefined property "type" or "kind" for direct type
  if (this.category === undefined) directTypePropName = "type";
  else directTypePropName = "kind";
  //TODO: handle roles (where kind != this)
  Object.defineProperty( obj, directTypePropName, 
      {value: this, writable: false, enumerable: true});
  // initialize object
  keys = Object.keys( initSlots);
  for (k=0; k < keys.length; k++) {
    key = keys[k];
    if (this.properties[key]) {  // property defined in object type
      constraintViolation = this.check( key, initSlots[key]);
      if (constraintViolation instanceof NoConstraintViolation) {
        obj[key] = initSlots[key];
      } else {
        throw constraintViolation;
      }
    } else if (["index"].indexOf( key) === -1) {  // allow auxiliary properties
      throw new ObjectTypeConstraintViolation("Property "+ key +" is not defined for object type"+ 
          this.name);
    }
  }
  return obj;
};
