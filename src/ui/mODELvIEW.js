/**
 * @fileOverview  App-level view code
 * @author Gerd Wagner
 */
var mODELvIEW = {
  crudVerbs: {'R':'List', 'C':'Create', 'U':'Update', 'D':'Delete'},
  uiPages: null  // a cache
};
/**
 * Set up the model-based UI
 * @method 
 * @author Gerd Wagner
 */
mODELvIEW.setupUI = function () {
  if (!window.localStorage || !("classList" in document.createElement("a"))) {
    alert("The browser you are using is not able to run this app! "+
    		"Update your browser or install a recent version of Firefox.");
    return;
  }
  mODELvIEW.setupIndexUI();
  Object.keys( mODELcLASS.classes).forEach( function (className) {
    var View=null, 
        modelClass = mODELcLASS.classes[className];
    if (vIEWcLASS.classes[className+"View"]) {
      View = vIEWcLASS.classes[className+"View"];        
    } else {
      View = new vIEWcLASS({modelClass: mODELcLASS.classes[className]});
    }
    // set up the CRUD menu UI page
    mODELvIEW.setupManageDataUI( className);
    ["R","C","U","D"].forEach( function (crudCode) {
      var viewSlots={};
      switch (crudCode) {
      case "R":  //================ READ ============================
        viewSlots.userActions = {
          "back": function () { mODELvIEW.refreshUI( className +"-M");}
        };
        break;
      case "C":  //================ CREATE ============================
        viewSlots.userActions = {
          "createRecord": function (slots) {
              $appNS.ctrl.storageManager.add( modelClass, slots);},
          "back": function () { mODELvIEW.refreshUI( className +"-M");}
        };
        break;
      case "U":  //================ UPDATE ============================
        viewSlots.userActions = {
          "setViewModelObject": function (id) {  // ON object selection
            View.instances["U"].setModelObject( id);
          },
          "updateRecord": function (id, slots) {
              $appNS.ctrl.storageManager.update( modelClass, id, slots);},
          "back": function () { mODELvIEW.refreshUI( className +"-M");}
        };
        break;
      case "D":  //================ DELETE ============================
        viewSlots.userActions = {
          "setViewModelObject": function (id) {  // ON object selection
            View.instances["D"].setModelObject( id);
          },
          "deleteRecord": function (id) {
              $appNS.ctrl.storageManager.destroy( modelClass, id);},
          "back": function () { mODELvIEW.refreshUI( className +"-M");}
        };
        break;
      }
      // create and render view
      viewSlots.viewMode = crudCode;
      View.create( viewSlots).render();
    });
  });
  mODELvIEW.refreshUI("I");
};
/**
 * Set up the index/start UI
 * @method 
 * @author Gerd Wagner
 */
mODELvIEW.setupIndexUI = function () {
  var uiContainerEl = document.querySelector("section.UI#I"),
      footerEl = document.querySelector("html>body>footer"),
      menuEl=null, liEl=null;
  if (!uiContainerEl) {
    uiContainerEl = dom.createElement("section", {classValues:"UI", id:"I",
        content:"<p>This app supports the following operations:</p>"
    });
    if (footerEl) {
      document.body.insertBefore( uiContainerEl, footerEl);
    } else {
      document.body.appendChild( uiContainerEl);
    }
  }
  menuEl = uiContainerEl.querySelector("menu");
  if (!menuEl) {
    menuEl = document.createElement("menu");
    uiContainerEl.appendChild( menuEl);
  }
  menuEl.addEventListener( 'click', function (e) { 
    var menuOption="", className="", selectedMenuItemEl=null;
    if (e.target.tagName === "LI") {
      selectedMenuItemEl = e.target;
    } else if (e.target.parentNode.tagName === "LI") {
      selectedMenuItemEl = e.target.parentNode;
    } else return;
    menuOption = selectedMenuItemEl.id;
    switch (menuOption) {
    case "createTestData":
      $appNS.ctrl.createTestData();
      break;
    case "clearDatabase":
      if ($appNS.ctrl.clearDatabase) {
        $appNS.ctrl.clearDatabase();
      } else {
        Object.keys( mODELcLASS.classes).forEach( function (className) {
          $appNS.ctrl.storageManager.clearData( mODELcLASS.classes[className]);
        });
      }
      break;
    default:  // a model class has been selected
      className = selectedMenuItemEl.id;
      mODELvIEW.refreshUI( className +"-M");
    }
  });
  Object.keys( mODELcLASS.classes).forEach( function (className) {
    if (!menuEl.querySelector("li#"+ className)) {
      liEl = dom.createMenuItem({
        id: className,
        content:"Manage "+ className.toLowerCase() +" data"
      });
      menuEl.appendChild( liEl);        
    }
  });
  if ($appNS.ctrl.createTestData) {
    if (!menuEl.querySelector("li#createTestData")) {
      liEl = dom.createMenuItem({
        id:"createTestData",
        content:"Create test data"
      });
      menuEl.appendChild( liEl);        
    }      
  }
  if (!menuEl.querySelector("li#clearDatabase")) {
    liEl = dom.createMenuItem({
      id:"clearDatabase",
      content:"Clear database"
    });
    menuEl.appendChild( liEl);        
  }            
};
/**
 * Set up the manage data UI for selecting a CRUD operation
 * @method 
 * @author Gerd Wagner
 * @param {string} className  The name of the model class.
 */
mODELvIEW.setupManageDataUI = function (className) {
  var manageSectEl = document.querySelector("section.UI#"+ className +"-M"),
      footerEl = document.querySelector("html>body>footer"),
      menuEl=null, liEl=null;
  if (!manageSectEl) {
    manageSectEl = dom.createElement("section", { classValues:"UI", 
        id: className +"-M",
        content:"<h1>Manage "+ className.toLowerCase() +" data</h1>"
    });
    if (footerEl) {
      document.body.insertBefore( manageSectEl, footerEl);
    } else {
      document.body.appendChild( manageSectEl);
    }
  }
  menuEl = manageSectEl.querySelector("menu");
  if (!menuEl) {
    menuEl = document.createElement("menu");
    manageSectEl.appendChild( menuEl);
  }
  ["R","C","U","D"].forEach( function (crudCode) {
    var inbetween = (crudCode !== "R") ? " a " : " ",
        suffix = (crudCode === "R") ? "s" : "";
    liEl = dom.createMenuItem({
      id: className +"-"+ crudCode ,
      content: mODELvIEW.crudVerbs[crudCode] + inbetween + 
               className.toLowerCase() +" record"+ suffix
    });
    menuEl.appendChild( liEl);
  });
  menuEl.addEventListener( 'click', function (e) { 
    var className="", crudCode="", selectedMenuItemEl=null, sepPos=0;
    if (e.target.tagName === "LI") {
      selectedMenuItemEl = e.target;
    } else if (e.target.parentNode.tagName === "LI") {
      selectedMenuItemEl = e.target.parentNode;
    } else return;
    sepPos = selectedMenuItemEl.id.indexOf('-');
    className = selectedMenuItemEl.id.substr( 0, sepPos);
    crudCode = selectedMenuItemEl.id.substr( sepPos+1);
    mODELvIEW.refreshUI( className +"-"+ crudCode);
  });
  manageSectEl.appendChild( dom.createBackButton({
    label:"Back to main menu",
    handler: function () { mODELvIEW.refreshUI("I");}
  }));
},
/**
 * Refresh the contents of, and show, the specified UI section and hide all others
 * @method 
 * @author Gerd Wagner
 * @param {string} userInterfaceId  The name of the model class.
 */
mODELvIEW.refreshUI = function (userInterfaceId) {
  var i=0, selectEl=null, formEl=null, uiPages = mODELvIEW.uiPages;
  var sepPos = userInterfaceId.indexOf('-');
  var className = userInterfaceId.substr( 0, sepPos);
  var opCode = userInterfaceId.substr( sepPos+1);
  var modelClass = mODELcLASS.classes[className];
  // store the UI elements in a cache variable
  if (!uiPages) uiPages = document.querySelectorAll("section.UI");
  for (i=0; i < uiPages.length; i++) {
    if (uiPages[i].id === userInterfaceId) {
      uiPages[i].style.display = "block";
    } else {
      uiPages[i].style.display = "none";        
    }
  }
  switch (opCode) {
  case "M":
    break;
  case "R":
    $appNS.ctrl.storageManager.retrieveAll( modelClass,
        function () {mODELvIEW.fillTable( className);});
    break;
  case "U":
    formEl = document.querySelector("section#"+ className +"-U > form");
    formEl.reset();
    selectEl = formEl["select"+ className];
    $appNS.ctrl.storageManager.retrieveAll( modelClass,
        function () {
          dom.fillSelectWithOptionsFromEntityTable( selectEl,
              modelClass.instances,
              modelClass.standardIdAttr,
              modelClass.attributesToDisplayInLists);
    });
    break;
  case "D":
    formEl = document.querySelector("section#"+ className +"-D > form");
    formEl.reset();
    selectEl = formEl["select"+ className];
    $appNS.ctrl.storageManager.retrieveAll( modelClass,
        function () {
          dom.fillSelectWithOptionsFromEntityTable( selectEl,
              modelClass.instances,
              modelClass.standardIdAttr,
              modelClass.attributesToDisplayInLists);
    });
    break;
  }
};
/**
 * Fill a DOM table bound to a model class having column headers 
 * bound to its properties.  
 * @method 
 * @author Gerd Wagner
 * @param {string} className  The name of the model class.
 */
mODELvIEW.fillTable = function (className) {
  var mc = mODELcLASS.classes[className],
      columns=[], keys=[], obj=null, 
      rowEl=null, i=0, nmrOfInstances=0;
  var tblEl = document.querySelector("html>body>section#"+ className +"-R>table");
  if (!tblEl) {
    console.log("No table in "+ className +"-R user interface section found!");
    return;
  }
  if (!(tblEl.tBodies[0] && tblEl.tHead && tblEl.tHead.rows[0])) {
    console.log("Table in "+ className +"-R user interface section " +
    		"does not have the required structure!");
    return;    
  }
  tblEl.tBodies[0].innerHTML = "";
  columns = tblEl.tHead.rows[0].cells;
  keys = Object.keys( mc.instances);
  nmrOfInstances = keys.length;
  for (i=0; i < nmrOfInstances; i++) {
    obj = mc.instances[keys[i]];
    rowEl = tblEl.tBodies[0].insertRow(-1);
    // create cell content for each column
    Array.prototype.forEach.call( columns, function (col) {
      var cellContent="", properties=[], propSep="", 
          dataBinding = col.getAttribute("data-bind");
      if (!dataBinding) {
        console.log("A column in the table in the "+ className +
            "-R UI page does not have any data binding!");
        return;
      }
      if (dataBinding.indexOf(" ") === -1) {  // single field column
        properties = [dataBinding];
      } else {  // field group column
        properties = dataBinding.split(" ");
      }
      propSep = col.getAttribute("data-separator") || ", ";
      properties.forEach( function (prop, k) {
        var cont, range = mc.properties[prop].range;
        if (obj[prop] === undefined) return;
        if (range === "Date") {
          cont = dom.createTime( obj[prop]).outerHTML;
        } else {
          cont = obj.getValAsString( prop);
        }
        if (k===0) cellContent = cont;
        else cellContent += propSep + cont;
      });
      rowEl.insertCell(-1).innerHTML = cellContent;
    });
    //TODO add edit and delete buttons
  }
};