/**
 * Meta-class for creating agent types as factory classes. 
 * aGENTtYPE admits the feature maps "reactionRules" and "internalEventTypes".
 * @copyright Copyright � 2014 Gerd Wagner, Chair of Internet Technology, 
 *   Brandenburg University of Technology, Germany.
 * @license [%LICENSE-TEXT%]
 * @author Gerd Wagner
 * @constructor
 * @this {aGENTtYPE}
 * @param {{name: string, supertype: string?, supertypes: string?, 
 *          properties: Map, 
 *          methods: Map, staticMethods: Map,
 *          reactionRules: Map, 
 *          internalEventTypes: Map,}} 
 *     slots  The agent type definition slots.
 */
function aGENTtYPE( slots) {
  // invoke the oBJECTtYPE constructor since an aGENTtYPE is an oBJECTtYPE
  oBJECTtYPE.call( this, slots);
  if (this.reactionRules === undefined || this.reactionRules === {}) {
    throw new AgentTypeConstraintViolation("aGENTtYPE "+ this.name +
        " must define or inherit reaction rules!");          
  }
};
aGENTtYPE.prototype = Object.create( oBJECTtYPE.prototype);
aGENTtYPE.prototype.constructor = aGENTtYPE;
