﻿/**
 * @fileOverview  The Parse REST API storage adapter method library
 * @author Bo Li
 * @author Gerd Wagner
 * @copyright Copyright 2015 Bo Li & Gerd Wagner, Chair of Internet Technology,
 *   Brandenburg University of Technology, Germany.
 * @license The MIT License (MIT)
 */
sTORAGEmANAGER.adapters["ParseRestAPI"] = {
  /**
   * Generic method for loading/retrieving a model object
   * @method
   * @param {object} mc  The model class concerned
   * @param {string|number} id  The object ID value
   * @param {function} continueProcessing  The function that is invoked after
   * the async operation has completed. Its first parameter is the resulting data,
   * if any, and its second optional parameter is an error message/object.
   */
  //------------------------------------------------
  retrieve: function ( mc, id, continueProcessing) {
  //------------------------------------------------

    var obj = mc.properties, includeParams = "", pointerString = "",
      objectId = mc.instances[id].objectId, self = this;
    Object.getOwnPropertyNames(obj).forEach( function (val) {
      if (obj[val].range === "SingleValuedRef") { pointerString += (val + ',');}
    });
    if (pointerString !== "") {
      includeParams = '?' + encodeURIComponent('include=' + pointerString);
    }

    xhr.GET({
      url: 'https://api.parse.com/1/classes/' + mc.name + '/' + objectId  + includeParams,
      requestHeaders: {
        'X-Parse-Application-Id': this.currentAdapter.applicationId,
        'X-Parse-REST-API-Key': this.currentAdapter.restApiKey
      },
      handleResponse: function ( r) {
        if ( r.status === 200 || r.status === 201 || r.status === 304) {
          answer = JSON.parse(r.responseText);
          var containParseRelation = false;
          // check whether there is ParseRelation or not
          Object.getOwnPropertyNames(answer).forEach(function(val){
            if ( answer[val] && answer[val].__type === "Relation") {
              containParseRelation = true;
              var relatedString = '?' + encodeURIComponent(
                  'where={"$relatedTo":{"object":{"__type":"Pointer","className":"' +
                  mc.name + '","objectId":"' + answer.objectId + '"},"key":"' + val + '"}}'
                );
              xhr.GET({
                url: 'https://api.parse.com/1/classes/' + answer[val].className + relatedString,
                requestHeaders: {
                  'X-Parse-Application-Id': self.currentAdapter.applicationId,
                  'X-Parse-REST-API-Key': self.currentAdapter.restApiKey
                },
                handleResponse: function (rR) {
                  var rResponse = JSON.parse(rR.responseText), cname = answer[val].className;
                  answer[val] = rResponse.results;
                  answer[val].forEach(function(a) {a.className = cname;});
                  continueProcessing( answer);
                }
              });
            }
          });
          // normal continueProcessing when there is no ParseRelation
          if ( !containParseRelation) {
            continueProcessing( answer);
          }
        } else {
          continueProcessing( null, "Error " + r.status +": "+ r.statusText);
          console.log("Error " + r.status +": "+ r.statusText, r);
        }
      }
    });
  },
  //------------------------------------------------
  retrieveAll: function ( mc, continueProcessing) {
  //------------------------------------------------
    var obj = mc.properties, stdid = mc.standardIdAttr, includeParams = "",
     pointerString = "", self = this;
    Object.getOwnPropertyNames(obj).forEach( function (val) {
      if (obj[val].range === "SingleValuedRef") { pointerString += (val + ',');}
    });
    if (pointerString !== "") {
      includeParams = '?' + encodeURIComponent('include=' + pointerString);
    }
    xhr.GET({
      url: 'https://api.parse.com/1/classes/' + mc.name + includeParams,
      requestHeaders: {
        'X-Parse-Application-Id': this.currentAdapter.applicationId,
        'X-Parse-REST-API-Key': this.currentAdapter.restApiKey
      },
      handleResponse: function ( r) {
        response = JSON.parse( r.responseText), answers = response.results, obj=null;
        var containParseRelation = false;
        if ( r.status === 200 || r.status === 201 || r.status === 304) {
          console.log( answers.length +" "+ mc.name +" records loaded.");
          // check whether there is ParseRelation attribute
          answers.forEach( function ( answer) {
            Object.getOwnPropertyNames(answer).forEach(function(val){
              if ( answer[val] && answer[val].__type === "Relation") {
                containParseRelation = true;
                var relatedString = '?' + encodeURIComponent(
                    'where={"$relatedTo":{"object":{"__type":"Pointer","className":"' +
                    mc.name + '","objectId":"' + answer.objectId + '"},"key":"' + val + '"}}'
                  );
                xhr.GET({
                  url: 'https://api.parse.com/1/classes/' + answer[val].className + relatedString,
                  requestHeaders: {
                    'X-Parse-Application-Id': self.currentAdapter.applicationId,
                    'X-Parse-REST-API-Key': self.currentAdapter.restApiKey
                  },
                  handleResponse: function (rR) {
                    var rResponse = JSON.parse(rR.responseText), cname = answer[val].className;
                    answer[val] = rResponse.results;
                    answer[val].forEach(function (a) {a.className = cname;});
                    obj = mc.convertRec2Obj( answer);
                    if ( obj) mc.instances[answer[stdid]] = obj;
                    continueProcessing( mc.instances);
                  }
                });
              }
            });
          });
          // normal continueProcessing when there is no ParseRelation
          if ( !containParseRelation) {
            for ( i = 0; i < answers.length; i++) {
              obj = mc.convertRec2Obj( answers[i]);
              if ( obj) mc.instances[answers[i][stdid]] = obj;
            }
            continueProcessing( mc.instances);
          }
        } else console.log("Error " + r.status +": "+ r.statusText);
      }
    });
  },
  //------------------------------------------------
  add: function ( mc, slots, newObj, continueProcessing) {
  //------------------------------------------------
    var obj = mc.properties;
    Object.getOwnPropertyNames(obj).forEach( function (val) {
      if (obj[val].range === "SingleValuedRef") {
        if ( slots[val] !== null && slots[val] !== undefined) {
          slots[val] = {
            __type: "Pointer",
            className: newObj[val].type.name,
            objectId: newObj[val].objectId
          };
        }
      } else if (obj[val].range === "MultiValuedRef") {
        if ( slots[val] !== null && slots[val] !== undefined
          && Array.isArray( slots[val]) && slots[val].length > 0) {
          slots[val] = {
            __op:'AddRelation',
            objects:[]
          };
          newObj[val].forEach( function ( item) {
            slots[val].objects.push({
              __type: 'Pointer',
              className: item.type.name,
              objectId: item.objectId
            });
          });
        } else {
          slots[val] = undefined;
        }
      }
    });
    xhr.POST({
      url: 'https://api.parse.com/1/classes/' + mc.name,
      reqFormat: 'application/json',
      msgBody: JSON.stringify( slots),
      requestHeaders: {
        'X-Parse-Application-Id': this.currentAdapter.applicationId,
        'X-Parse-REST-API-Key': this.currentAdapter.restApiKey
      }
    });
    console.log( newObj.toString() + " created!");
  },

  //------------------------------------------------
  update: function ( mc, id, slots, objToUpdate, objectBeforeUpdate, continueProcessing) {
  //------------------------------------------------
    var ET = mc, obj = ET.properties, self = this;
    var updateNormal = util.cloneObject(objToUpdate),
      updatePointer = util.cloneObject(objToUpdate),
      updatePR = util.cloneObject(objToUpdate);
    Object.getOwnPropertyNames(obj).forEach( function (val) {
      if (obj[val].range === "MultiValuedRef") {
        delete updatePointer[val];
        delete updateNormal[val];
      }
    });
    Object.getOwnPropertyNames(obj).forEach( function (val) {
      if (obj[val].range === "SingleValuedRef") {
        delete updatePR[val];
        delete updateNormal[val];
      }
    });
    // normal update
    xhr.OPTIONS( {
      url: 'https://api.parse.com/1/classes/' + ET.name
    });
    xhr.PUT( {
      url: 'https://api.parse.com/1/classes/' + ET.name + '/' + objToUpdate.objectId,
      reqFormat: 'application/json',
      msgBody: JSON.stringify(updateNormal),
      requestHeaders: {
        'X-Parse-Application-Id': self.currentAdapter.applicationId,
        'X-Parse-REST-API-Key': self.currentAdapter.restApiKey
      }
    });
    // Update Pointer
    Object.getOwnPropertyNames(obj).forEach( function (val) {
      if (obj[val].range === "SingleValuedRef") {
        if ( !((objectBeforeUpdate[val] === null || objectBeforeUpdate[val] === undefined) &&
          (objToUpdate[val] === null || objToUpdate[val] === undefined))) {
          if (objToUpdate[val] === null || objToUpdate[val] === undefined) {
            updatePointer[val] = { __op: 'Delete'};
          } else {
            updatePointer[val] = {
              __type: "Pointer",
              className: objToUpdate[val].className || objToUpdate[val].type.name,
              objectId: objToUpdate[val].objectId
            };
          }
          xhr.OPTIONS( {
            url: 'https://api.parse.com/1/classes/' + ET.name
          });
          xhr.PUT( {
            url: 'https://api.parse.com/1/classes/' + ET.name + '/' + objToUpdate.objectId,
            reqFormat: 'application/json',
            msgBody: JSON.stringify(updatePointer),
            requestHeaders: {
              'X-Parse-Application-Id': self.currentAdapter.applicationId,
              'X-Parse-REST-API-Key': self.currentAdapter.restApiKey
            }
          });
        }
      }
    });
    // Update ParseRelation
    Object.getOwnPropertyNames(obj).forEach( function (val) {
      if (obj[val].range === "MultiValuedRef") {
        if ( (objectBeforeUpdate[val] === null || objectBeforeUpdate[val] === undefined
          || (Array.isArray( objectBeforeUpdate[val]) && objectBeforeUpdate[val].length === 0)) &&
          (objToUpdate[val] === null || objToUpdate[val] === undefined
          || (Array.isArray( objToUpdate[val]) && objToUpdate[val].length === 0))) {
            // if 0 to 0, no change, then assign undefined
            objToUpdate[val] = undefined;
        } else { // check changes
          if ( (objectBeforeUpdate[val] === null || objectBeforeUpdate[val] === undefined
            || (Array.isArray( objectBeforeUpdate[val]) && objectBeforeUpdate[val].length === 0)) &&
            !(objToUpdate[val] === null || objToUpdate[val] === undefined
            || (Array.isArray( objToUpdate[val]) && objToUpdate[val].length === 0))) {
              // if 0 to many, addRelation
              updatePR[val] = {
                __op: 'AddRelation',
                objects:[]
              };
              objToUpdate[val].forEach( function ( item) {
                updatePR[val].objects.push({
                  __type: 'Pointer',
                  className: item.className || item.type.name,
                  objectId: item.objectId
                });
              });
              xhr.OPTIONS( {
                url: 'https://api.parse.com/1/classes/' + ET.name
              });
              xhr.PUT( {
                url: 'https://api.parse.com/1/classes/' + ET.name + '/' + objToUpdate.objectId,
                reqFormat: 'application/json',
                msgBody: JSON.stringify(updatePR),
                requestHeaders: {
                  'X-Parse-Application-Id': self.currentAdapter.applicationId,
                  'X-Parse-REST-API-Key': self.currentAdapter.restApiKey
                }
              });
          } else if ( !(objectBeforeUpdate[val] === null || objectBeforeUpdate[val] === undefined
            || (Array.isArray( objectBeforeUpdate[val]) && objectBeforeUpdate[val].length === 0)) &&
            (objToUpdate[val] === null || objToUpdate[val] === undefined
            || (Array.isArray( objToUpdate[val]) && objToUpdate[val].length === 0))) {
              // if many to 0, removeRelation
              updatePR[val] = {
                __op: 'RemoveRelation',
                objects:[]
              };
              objectBeforeUpdate[val].forEach( function ( item) {
                updatePR[val].objects.push({
                  __type: 'Pointer',
                  className: item.className,
                  objectId: item.objectId
                });
              });
              xhr.OPTIONS( {
                url: 'https://api.parse.com/1/classes/' + ET.name
              });
              xhr.PUT( {
                url: 'https://api.parse.com/1/classes/' + ET.name + '/' + objToUpdate.objectId,
                reqFormat: 'application/json',
                msgBody: JSON.stringify(updatePR),
                requestHeaders: {
                  'X-Parse-Application-Id': self.currentAdapter.applicationId,
                  'X-Parse-REST-API-Key': self.currentAdapter.restApiKey
                }
              });
          } else if ( !(objectBeforeUpdate[val] === null || objectBeforeUpdate[val] === undefined
            || (Array.isArray( objectBeforeUpdate[val]) && objectBeforeUpdate[val].length === 0)) &&
            !(objToUpdate[val] === null || objToUpdate[val] === undefined
            || (Array.isArray( objToUpdate[val]) && objToUpdate[val].length === 0))) {
              // if many to many, first remove old, then add new
              updatePR[val] = {
                __op: 'RemoveRelation',
                objects:[]
              };
              objectBeforeUpdate[val].forEach( function ( item) {
                updatePR[val].objects.push({
                  __type: 'Pointer',
                  className: item.className,
                  objectId: item.objectId
                });
              });
              xhr.OPTIONS( {
                url: 'https://api.parse.com/1/classes/' + ET.name
              });
              xhr.PUT( {
                url: 'https://api.parse.com/1/classes/' + ET.name + '/' + objToUpdate.objectId,
                reqFormat: 'application/json',
                msgBody: JSON.stringify(updatePR),
                requestHeaders: {
                  'X-Parse-Application-Id': self.currentAdapter.applicationId,
                  'X-Parse-REST-API-Key': self.currentAdapter.restApiKey
                }
              });
              updatePR[val] = {
                __op: 'AddRelation',
                objects:[]
              };
              objToUpdate[val].forEach( function ( item) {
                updatePR[val].objects.push({
                  __type: 'Pointer',
                  className: item.className || item.type.name,
                  objectId: item.objectId
                });
              });
              xhr.OPTIONS( {
                url: 'https://api.parse.com/1/classes/' + ET.name
              });
              xhr.PUT( {
                url: 'https://api.parse.com/1/classes/' + ET.name + '/' + objToUpdate.objectId,
                reqFormat: 'application/json',
                msgBody: JSON.stringify(updatePR),
                requestHeaders: {
                  'X-Parse-Application-Id': self.currentAdapter.applicationId,
                  'X-Parse-REST-API-Key': self.currentAdapter.restApiKey
                }
              });
          }
        }
      }
    });
  },
  //------------------------------------------------
  destroy: function ( mc, id, continueProcessing) {
  //------------------------------------------------
    var objectId = mc.instances[id].objectId;
    xhr.DELETE({
      url: 'https://api.parse.com/1/classes/' + mc.name + '/' + objectId,
      requestHeaders: {
        'X-Parse-Application-Id': this.currentAdapter.applicationId,
        'X-Parse-REST-API-Key': this.currentAdapter.restApiKey
      }
    });
  },
  //------------------------------------------------
  /* Parse.com REST API does not offer the function of "delete all objects" or "drop class",
   * currently we can first get all datas from Parse.com, then delete them one by one using forEach().
   */
  clearData: function ( mc, continueProcessing) {
  //------------------------------------------------
    var applicationId = this.currentAdapter.applicationId,
        restApiKey = this.currentAdapter.restApiKey;
    xhr.GET({
      url: 'https://api.parse.com/1/classes/' + mc.name,
      requestHeaders: {
        'X-Parse-Application-Id': applicationId,
        'X-Parse-REST-API-Key': restApiKey
      },
      handleResponse: function (r) {
        JSON.parse( r.responseText).results.forEach( function (ob) {
          xhr.DELETE({
            url: 'https://api.parse.com/1/classes/' + mc.name + '/' + ob.objectId,
            requestHeaders: {
              'X-Parse-Application-Id': applicationId,
              'X-Parse-REST-API-Key': restApiKey
            }
          });
        });
      }
    });
  }
};

/**
 * XHR methods
 */
xhr = {
 /**
  * Make an XHR GET request
  * @param params  Contains parameter slots
  */
 OPTIONS: function (params) {
   params.method = "OPTIONS";
   xhr.makeRequest( params);
 },
 /**
  * Make an XHR GET request
  * @param params  Contains parameter slots
  */
 GET: function (params) {
   params.method = "GET";
   xhr.makeRequest( params);
 },
 /**
  * Make an XHR POST request
  * @param params  Contains parameter slots
  */
 POST: function (params) {
   params.method = "POST";
   xhr.makeRequest( params);
 },
 /**
  * Make an XHR PUT request
  * @param params  Contains parameter slots
  */
 PUT: function (params) {
   params.method = "PUT";
   xhr.makeRequest( params);
 },
 /**
  * Make an XHR DELETE request
  * @param params  Contains parameter slots
  */
 DELETE: function (params) {
   params.method = "DELETE";
   xhr.makeRequest( params);
 },
 /**
  * Make an XHR request
  * @param {{method: string?, url: string,
  *          reqFormat: string?, respFormat: string?,
  *          handleResponse: function?,
  *          requestHeaders: Map?}
  *        } params  The parameter slots.
  */
 makeRequest: function (params) {
   var xhr=null, url="", method="",
       reqFormat="", respFormat="",
       handleResponse=null;
   //TODO: move this constant declaration to a central lib
   var URL_PATTERN = /\b(https?):\/\/[\-A-Za-z0-9+&@#\/%?=~_|!:,.;]*[\-A-Za-z0-9+&@#\/%=~_|??]/;
   if (!params.url) {
     console.log("Missing value for url parameter in XHR GET request!");
     return;
   } else if (!URL_PATTERN.test( params.url)) {
     console.log("Invalid URL in XHR GET request!");
     return;
   } else {
     url = params.url;
     xhr = new XMLHttpRequest();
     method = (params.method) ? params.method : "GET";  // default
     reqFormat = (params.reqFormat) ? params.reqFormat :
         "application/x-www-form-urlencoded";  // default
     respFormat = (params.respFormat) ? params.respFormat : "application/json";  // default
     if (!params.handleResponse) {
       // define a default response handler
       handleResponse = function (r) {
         if (r.status === 200 || r.status === 201 || r.status === 304)
           console.log("Response: " + r.responseText);
         else console.log("Error " + r.status +": "+ r.statusText, r);
       };
     } else { handleResponse = params.handleResponse;}
   }
   xhr.open( method, url, true);
   xhr.onreadystatechange = function () {
     if (xhr.readyState === 4) { handleResponse( xhr);}
   };
   // params.requestHeaders: Map?
   if ( params.requestHeaders) {
     Object.keys(params.requestHeaders).forEach(function(rH) {
       xhr.setRequestHeader(rH, params.requestHeaders[rH]);
     });
   }
   xhr.setRequestHeader("Accept", respFormat);
   if (method === "GET" || method === "DELETE") {
     xhr.send("");
   } else {  // POST and PUT
     xhr.setRequestHeader("Content-Type", reqFormat);
     xhr.send( params.msgBody);
   }
 }
};
