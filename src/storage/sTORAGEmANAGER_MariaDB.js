﻿sTORAGEmANAGER.adapters["MariaDB"] = {
  pool: null,
  /**
   * Get the connection with the database (async method).
   * @method
   * @param {function} callback The function that is invoked after
   * the async operation has completed.
   */
  getConnection: function ( callback) {
    var sqlStatement = '', storageAdapter = this.currentAdapter;
    if ( !this.pool) {
      this.pool = mariaDBConnector.createPool({
        host     : this.currentAdapter.host || 'localhost',
        port     : this.port || 3306,
        user     : this.currentAdapter.user,
        password : this.currentAdapter.password
      });
    } 
    this.pool.getConnection( function ( err, connection) {
      if ( err) {
        callback( err, null);
      } else {
        sqlStatement = 'USE ' + storageAdapter.database + ';';
        connection.query( sqlStatement, function ( err) {
          if ( err) {
            callback( err, null);
          } else {
            callback( null, connection);
          }
        });
      }
    });
  },
  
  /**
   * Helper method which transforms the DB enum values into 
   * values required by the eNUMERATION.
   * @param enumClass 
   *    the specific eNUMERATION class
   * @param labels 
   *    the DB enumeration literals to be converted
   * @param multivalued 
   *    flag that specifies if the property is multivalued or not
   */
  convertDBEnumToJSEnum: function (enumClass, literals, multivalued) {
    var tLits = null, indexes = [];
    if (!literals) return multivalued ? [] : undefined;
    // multivalued
    if (multivalued) {
      tLits = literals.split(',');
      tLits.forEach( function (lit) {
        indexes.push(enumClass[lit.toUpperCase()]);
      });
      return indexes;
    } 
    // single value
    else {
      return enumClass[literals.toUpperCase()];
    }
  },
  
   /**
   * Helper method which transforms the eNUMERATION enum 
   * values into DB enum values.
   * @param enumClass 
   *    the specific eNUMERATION class
   * @param ordinals 
   *    the JS enumeration ordinals to be converted
   * @param multivalued 
   *    flag that specifies if the property is multivalued or not
   */
  convertJSEnumToDBEnum: function (enumClass, ordinals, multivalued) {
    var tLits = null, litListAsString = "";
    if (!ordinals) return null;
    if (Array.isArray(ordinals) && ordinals.length < 1) return null;
    // multivalued
    if (multivalued) {
      litListAsString = enumClass.enumIndexesToNames(ordinals);
      litListAsString = litListAsString.replace(/ /g,'');
      return litListAsString;
    } else {
      return enumClass.enumLitNames[ordinals - 1];
    }
  },

  /**
   * Generic method for loading/retrieving a model object
   * @method
   * @param {object} mc  The model class concerned
   * @param {string|number} id  The object ID value
   * @param {function} continueProcessing  The function that is invoked after
   * the async operation has completed. Its first parameter is the resulting data,
   * if any, and its second optional parameter is an error message/object.
   */
  //------------------------------------------------
  retrieve: function ( mc, id, continueProcessing) {
  //------------------------------------------------
    var tableName = util.getTableName( mc.name),
      stdid = mc.standardIdAttr, url = '', 
      appPath = $appNS && $appNS.ctrl && $appNS.ctrl.appPath ? $appNS.ctrl.appPath : '';
    if ( id === null || id === undefined) {
      throw "sTORAGEmANAGER['MariaDB'].retrieve: undefined or null object ID!";
    }
    // client side: make GET request
    if ( typeof exports !== 'object') {
      if ( typeof continueProcessing !== 'function') {
        continueProcessing = function ( obj, error) {};
      }
      url = window.location.protocol + '//' + window.location.hostname;
      if ( window.location.port > 0 && window.location.port !== 80) {    
        url += ':' + window.location.port;
      }
      url += appPath;
      xhr.GET({
        url: url + '/' + tableName + '/' + id,
        handleResponse: function ( r) {
          var response = null, obj = null;
          try {
            if ( r.status === 200 || r.status === 201 || r.status === 304) {
              response = JSON.parse( r.responseText);
              obj = mc.convertRec2Obj( response);
              continueProcessing( obj, null);
            } else {
              console.log( "Error " + r.status + ": " + r.statusText);
              continueProcessing( null, "Error " + r.status + ": " + r.statusText);
            }
          } catch ( e) {
            console.log( e);
            throw "sTORAGEmANAGER['MariaDB'].retrieve: fatal exception " 
              + "occurred during the 'retrieve' MariaDB request!";
          } 
        }
      });
    } 
    // server side: make MariaDB query
    else {
      // makes no sense to execute SQL statements if is nothing to do with the response...
      if ( typeof continueProcessing === 'function') {
        this.getConnection( function ( err, dbConnection) {
          var sqlStatement = '';
          if ( err) {
            continueProcessing( null, err);
          } else {
            sqlStatement = "SELECT * FROM " + tableName + " WHERE `" + util.getColumnName( stdid) + "` = ?;";
            dbConnection.query( sqlStatement, [id], function ( err, rows) {
              var row = {}, p = null, pName = "", multivalued = false, enumIndexes = null;
              if ( err) {
                continueProcessing( null, err);
              } else {
                try {
                  if ( rows.length > 0) {
                    // convert property names from underscore notation to CamelCase notation
                    Object.keys( rows[0]).forEach( function( pName) { 
                      row[util.getPropertyName( pName)] = rows[0][pName];
                    });
                     // take care of enumeration conversion
                    for ( pName in mc.properties) {
                      p = mc.properties[pName];
                      multivalued = p.maxCard > 1 || p.minCard > 1;
                      enumIndexes = null;
                      if (p.range.enumLitNames) {
                        enumIndexes = sTORAGEmANAGER.adapters["MariaDB"]
                          .convertDBEnumToJSEnum(p.range, row[pName], multivalued);
                        row[pName] = enumIndexes;
                      }
                    };
                    // *** Enumeration Properties Parse END ***/
                    continueProcessing( row, null);
                  } else {
                    continueProcessing( null, null);
                  }
                } catch ( e) {
                  continueProcessing( null, e);
                }
              }
            });
          }
        });
      }
    }
  },
 /**
  * Generic method for loading/retrieving all instances for a model object
  * @method
  * @param {object} mc  The model class concerned
  * @param {function} continueProcessing  The function that is invoked after
  * the async operation has completed. Its first parameter is the resulting data,
  * if any, and its second optional parameter is an error message/object.
  */
  //------------------------------------------------
  retrieveAll: function ( mc, continueProcessing) {
  //------------------------------------------------
    var tableName = util.getTableName( mc.name),
      stdid = mc.standardIdAttr, url = '', 
      appPath = $appNS && $appNS.ctrl && $appNS.ctrl.appPath ? $appNS.ctrl.appPath : '';
    // client side: make GET request
    if ( typeof exports !== 'object') {
      if ( typeof continueProcessing !== 'function') {
        continueProcessing = function ( obj, error) {};
      }
      url = window.location.protocol + '//' + window.location.hostname;
      if ( window.location.port > 0 && window.location.port !== 80) {    
        url += ':' + window.location.port;
      }
      url += appPath;
      xhr.GET({
        url: url + '/' + tableName,
        handleResponse: function ( r) {
          var response = null, objs = [], p = null, 
              pName = "", multivalued = false, enumIndexes = null;
          try {
            if ( r.status === 200 || r.status === 201 || r.status === 304) {
              response = JSON.parse( r.responseText);
              for ( i = 0; i < response.length; i++) {
                // take care of enumeration conversion
                for ( pName in mc.properties) {
                  p = mc.properties[pName];
                  multivalued = p.maxCard > 1 || p.minCard > 1;
                  enumIndexes = null;
                  if (p.range.enumLitNames) {
                    enumIndexes = sTORAGEmANAGER.adapters["MariaDB"]
                      .convertDBEnumToJSEnum(p.range, response[i][pName], multivalued);
                    response[i][pName] = enumIndexes;
                  }
                };
                // *** Enumeration Properties Parse END ***/
                objs[i] = mc.convertRec2Obj( response[i]);
                if ( objs[i]) mc.instances[response[i][stdid]] = objs[i];
              }
              continueProcessing( objs, null);
            } else {
              console.log( "Error " + r.status + ": " + r.statusText);
              continueProcessing( null, "Error " + r.status + ": " + r.statusText);
            }
          } catch ( e) {
            console.log( e);
            throw "sTORAGEmANAGER['MariaDB'].retrieveAll: fatal exception " 
              + "occurred during the 'retrieveAll' MariaDB request!";
          } 
        }
      });
    } 
    // server side: make MariaDB query
    else {
      // makes no sense to execute SQL statements if is nothing to do with the response...
      if ( typeof continueProcessing === 'function') {
        this.getConnection( function ( err, dbConnection) {
          var sqlStatement = '';
          if ( err) {
            continueProcessing( null, err);
          } else {
            sqlStatement = "SELECT * FROM " + tableName + ";";
            dbConnection.query( sqlStatement, function ( err, rows) {
              var objs = [];
              if ( err) {
                continueProcessing( null, err);
              } else {
                for ( i = 0; i < rows.length; i++) {
                  // convert property names from underscore notation to CamelCase notation
                  objs[i] = {};
                  Object.keys( rows[i]).forEach( function ( pName) {
                    objs[i][util.getPropertyName( pName)] = rows[i][pName];                 
                  });
                }
                continueProcessing( objs, null);
              }
            });
          }
        });
      }
    }
  },
  /**
   * Generic method for creating a new instance for a model object
   * @method
   * @param {object} mc  The model class concerned
   * @param {object} slots  The slots for object creation
   * @param {object} newObj  The new object
   * @param {function} continueProcessing  The function that is invoked after
   * the async operation has completed. Its first parameter is the resulting data,
   * if any, and its second optional parameter is an error message/object.
   */
  //------------------------------------------------
  add: function ( mc, slots, newObj, continueProcessing) {
  //------------------------------------------------    
    var tableName = util.getTableName( mc.name), url = ''
        tSlots = JSON.parse(JSON.stringify( slots)), 
        appPath = $appNS && $appNS.ctrl && $appNS.ctrl.appPath ? $appNS.ctrl.appPath : '';
    // client side: make POST request
    if ( typeof exports !== 'object') {
      url = window.location.protocol + '//' + window.location.hostname;
      if ( window.location.port > 0 && window.location.port !== 80) {    
        url += ':' + window.location.port;
      }
      url += appPath;
      xhr.POST({
        url: url + '/' + tableName,
        msgBody: JSON.stringify( tSlots),
        reqFormat: 'application/json',
        handleResponse: function ( r) {
          try {
            if ( r.status === 200 || r.status === 201 || r.status === 304) {
              if ( typeof continueProcessing === 'function') continueProcessing( newObj, null);
            } else {
              console.log( "Error " + r.status + ": " + r.statusText);
              if ( typeof continueProcessing === 'function') {
                continueProcessing( newObj, "Error " + r.status + ": " + r.statusText);
              }
            }
          } catch ( e) {
            console.log( e);
            throw "sTORAGEmANAGER['MariaDB'].add: fatal exception " 
                + "occurred during the 'add' MariaDB request!";
          }
        } 
      });
    } 
    // server side: make MariaDB query
    else {
      // makes no sense to execute SQL statements if is nothing to do with the response...
      if ( typeof continueProcessing === 'function') {
        this.getConnection( function (err, dbConnection) {
          var sqlStatement = '', params = [], qM = '',
              p = null, pName = "", multivalued = false, enumLits = null;
          if ( err) {
            continueProcessing( null, err);
          } else {
            /** take care of JS to DB enumeration conversion */
            for ( pName in newObj.type.properties) {
              p = newObj.type.properties[pName];
              multivalued = p.maxCard > 1 || p.minCard > 1;
              enumLits = null;
              if (p.range.enumLitNames) {
                enumLits = sTORAGEmANAGER.adapters["MariaDB"]
                  .convertJSEnumToDBEnum(p.range, newObj[pName], multivalued);
                newObj[pName] = enumLits;
              }
            };
            /********* END JS to DB Enum conversion **********/
            // create the INSERT statement
            sqlStatement = "INSERT INTO " + tableName + " ( ";
            Object.keys( newObj.type.properties).forEach( function ( propName, index) {
              if ( index > 0) {
                sqlStatement += ', ';
                qM += ', ';
              }
              sqlStatement += "`" + util.getColumnName( propName) + "`";
              qM += '?';
              // property value
              params.push( newObj[propName]);
            });
            sqlStatement += ') VALUE ( ';
            sqlStatement += qM + ');';
            dbConnection.query( sqlStatement, params, function ( err) {
              if ( err) {
                continueProcessing( null, err);
              } else {
                continueProcessing( null, null);
              }
            });
          }
        });
      }
    }
  },
  /**
   * Generic method for update a instance for a model object
   * @method
   * @param {object} mc  The model class concerned
   * @param {string|number} id  The object ID value
   * @param {object} slots  The slots for object update
   * @param {object} newObj  The updated object
   * @param {function} continueProcessing  The function that is invoked after
   * the async operation has completed. Its first parameter is the resulting data,
   * if any, and its second optional parameter is an error message/object.
   */
  //------------------------------------------------
  /***** newObj includes validated update slots *****/
  update: function ( mc, id, slots, newObj, continueProcessing) {
  //------------------------------------------------
    var tableName = util.getTableName( mc.name), url = '', 
        appPath = $appNS && $appNS.ctrl && $appNS.ctrl.appPath ? $appNS.ctrl.appPath : '';
    // client side: make PUT request
    if ( typeof exports !== 'object') {
      url = window.location.protocol + '//' + window.location.hostname;
      if ( window.location.port > 0 && window.location.port !== 80) {    
        url += ':' + window.location.port;
      }
      url += appPath;
      xhr.PUT({
        url: url + '/' + tableName + '/' + id,
        msgBody: JSON.stringify( slots),
        reqFormat: 'application/json',
        handleResponse: function ( r) {
          try {
            if ( r.status === 200 || r.status === 201 || r.status === 304) {
              if ( typeof continueProcessing === 'function') continueProcessing( newObj, null);
            } else {
              console.log( "Error " + r.status +": "+ r.statusText);
              if ( typeof continueProcessing === 'function') {
                continueProcessing( newObj, "Error " + r.status +": "+ r.statusText);
              }
            }
          } catch ( e) {
            console.log( e);
            throw "sTORAGEmANAGER['MariaDB'].update: fatal exception " 
                + "occurred during the 'update' MariaDB request!";
          }
        }
      });
    } 
    // server side: make MariaDB query
    else {
      // makes no sense to execute SQL statements if is nothing to do with the response...
      if ( typeof continueProcessing === 'function') {
        this.getConnection( function (err, dbConnection) {
          var sqlStatement = '', params = [], qM = '', i = 0, 
              p = null, pName = "", multivalued = false, enumLits = null;
          if ( err) {
            continueProcessing( null, err);
          } else {
            /** take care of JS to DB enumeration conversion */
            for ( pName in slots) {
              p = newObj.type.properties[pName];
              multivalued = p.maxCard > 1 || p.minCard > 1;
              enumLits = null;
              if (p.range.enumLitNames) {
                enumLits = sTORAGEmANAGER.adapters["MariaDB"]
                  .convertJSEnumToDBEnum(p.range, slots[pName], multivalued);
                slots[pName] = enumLits;
              }
            };
            /********* END JS to DB Enum conversion **********/
            // create the UPDATE statement
            sqlStatement = "UPDATE " + tableName + " SET ";
            Object.keys( slots).forEach( function ( propName, index) {
              if ( typeof slots[propName] !== 'function') {
                if ( i > 0) {
                  sqlStatement += ', ';
                }
                sqlStatement += "`" + util.getColumnName( propName) + "`" + ' = ?';
                // property value
                params.push( slots[propName]);
                i++;
              }
            });
            sqlStatement += " WHERE `" + util.getColumnName( mc.standardIdAttr) + "`= ?" ;
            params.push( id);
            dbConnection.query( sqlStatement, params, function ( err) {
              if ( err) {
                continueProcessing( null, err);
              } else {
                continueProcessing( null, null);
              }
            });
          }
        });
      }
    }
  },
  /**
   * Generic method for deletion of an instance for a model object
   * @method
   * @param {object} mc  The model class concerned
   * @param {string|number} id  The object ID value
   * @param {function} continueProcessing  The function that is invoked after
   * the async operation has completed. Its first parameter is the resulting data,
   * if any, and its second optional parameter is an error message/object.
   */
  //------------------------------------------------
  destroy: function ( mc, id, continueProcessing) {
  //------------------------------------------------
    var tableName = util.getTableName( mc.name), url = '', 
        appPath = $appNS && $appNS.ctrl && $appNS.ctrl.appPath ? $appNS.ctrl.appPath : '';
    if ( id === null || id === undefined) {
      throw "sTORAGEmANAGER['MariaDB'].destroy: undefined or null object ID!";
    }
    // client side: make DELETE request
    if ( typeof exports !== 'object') {
      if ( typeof continueProcessing !== 'function') {
        continueProcessing = function ( obj, error) {};
      }
      url = window.location.protocol + '//' + window.location.hostname;
      if ( window.location.port > 0 && window.location.port !== 80) {    
        url += ':' + window.location.port;
      }
      url += appPath;
      xhr.DELETE({
        url: url + '/' + tableName + '/' + id,
        handleResponse: function ( r) {
          var response = null;
          try {
            if ( r.status === 200 || r.status === 201 || r.status === 304) {
              continueProcessing( null, null);
            } else {
              console.log( "Error " + r.status + ": " + r.statusText);
              continueProcessing( null, "Error " + r.status + ": " + r.statusText);
            }
          } catch ( e) {
            console.log( e);
            throw "sTORAGEmANAGER['MariaDB'].destroy: fatal exception " 
                + "occurred during the 'destroy' MariaDB request!";
          }
        }
      });
    } 
    // server side: make MariaDB query
    else {
      // makes no sense to execute SQL statements if is nothing to do with the response...
      if ( typeof continueProcessing === 'function') {
        this.getConnection( function ( err, dbConnection) {
          var sqlStatement = '';
          if ( err) {
            continueProcessing( null, err);
          } else {
            sqlStatement = "DELETE FROM " + tableName + " WHERE `" + util.getColumnName( mc.standardIdAttr) + "` = ?;";
            dbConnection.query( sqlStatement, [id], function ( err, row) {
              if ( err) {
                continueProcessing( null, err);
              } else {
                continueProcessing( row, null);
              }
            });
          }
        });
      }
    }
  },
  /**
   * Generic method for deletion of al instances for a model object
   * @method
   * @param {object} mc  The model class concerned
   * @param {function} continueProcessing  The function that is invoked after
   * the async operation has completed. Its first parameter is the resulting data,
   * if any, and its second optional parameter is an error message/object.
   */
  //------------------------------------------------
  /* Delete all the entries for the specified type.
   */
  clearData: function ( mc, continueProcessing) {
  //------------------------------------------------
  var tableName = util.getTableName( mc.name), url = '', 
      appPath = $appNS && $appNS.ctrl && $appNS.ctrl.appPath ? $appNS.ctrl.appPath : '';
    // client side: make DELETE request
    if ( typeof exports !== 'object') {
      if ( typeof continueProcessing !== 'function') {
        continueProcessing = function ( obj, error) {};
      }
      url = window.location.protocol + '//' + window.location.hostname;
      if ( window.location.port > 0 && window.location.port !== 80) {    
        url += ':' + window.location.port;
      }
      url += appPath;
      xhr.DELETE({
        url: url + '/' + tableName + '/',
        handleResponse: function ( r) {
          var response = null;
          try {
            if ( r.status === 200 || r.status === 201 || r.status === 304) {
              continueProcessing( null, null);
            } else {
              console.log( "Error " + r.status + ": " + r.statusText);
              continueProcessing( null, "Error " + r.status + ": " + r.statusText);
            }
          } catch ( e) {
            console.log( e);
            throw "sTORAGEmANAGER['MariaDB'].clearData: fatal exception " 
                + "occurred during the 'clearData' MariaDB request!";
          }
        }
      });
    } 
    // server side: make MariaDB query
    else {
      // makes no sense to execute SQL statements if is nothing to do with the response...
      this.getConnection( function ( err, dbConnection) {
        var sqlStatement = '';
        if ( err) {
          continueProcessing( null, err);
        } else {
          sqlStatement = "DELETE FROM " + tableName + ";";
          dbConnection.query( sqlStatement, function ( err, row) {
            if ( err) {
              if ( typeof continueProcessing === 'function') continueProcessing( null, err);
            } else {
              if ( typeof continueProcessing === 'function') continueProcessing( null, null);
            }
          });
        }
      }); 
    }
  }
};