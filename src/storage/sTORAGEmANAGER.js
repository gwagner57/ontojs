﻿/**
 * @fileOverview  This file contains the definition of the library class
 * sTORAGEmANAGER.
 * @author Gerd Wagner
 * @copyright Copyright 2015 Gerd Wagner, Chair of Internet Technology,
 *   Brandenburg University of Technology, Germany.
 * @license The MIT License (MIT)
 */
/**
 * Library class providing storage management methods for a number of predefined
 * storage adapters
 *
 * @constructor
 * @this {sTORAGEmANAGER}
 * @param storageAdapter: object
 */
function sTORAGEmANAGER( storageAdapter) {
  if (!storageAdapter) this.adapter = {name:"LocalStorage"};
  else if (typeof( storageAdapter) === 'object' &&
      storageAdapter.name !== undefined &&
      ["LocalStorage","ParseRestAPI","MariaDB"].includes( storageAdapter.name)) {
    this.adapter = storageAdapter;
  } else {
    throw new ConstraintViolation("Invalid storageAdapter name");
  }
  // copy storage adapter to corresponding storage management method library
  sTORAGEmANAGER.adapters[this.adapter.name].currentAdapter = storageAdapter;
}
/**
 * Generic method for loading/retrieving a model object
 * @method
 * @param {object} mc  The model class concerned
 * @param {string|number} id  The object ID value
 * @param {function} continueProcessing  The continueProcessing function
 */
sTORAGEmANAGER.prototype.retrieve = function (mc, id, continueProcessing) {
  if (typeof( continueProcessing) !== 'function') {
    throw "sTORAGEmANAGER.prototype.retrieve: " +
        "the 'continueProcessing' parameter is required!";
  }
  sTORAGEmANAGER.adapters[this.adapter.name].retrieve( mc, id, continueProcessing);
};
/**
 * Generic method for loading all table rows and converting them
 * to model objects
 *
 * @method
 * @param {object} mc  The model class concerned
 * @param {function} continueProcessing  The continueProcessing function
 */
sTORAGEmANAGER.prototype.retrieveAll = function (mc, continueProcessing) {
  mc.instances = {};  // clear main memory cache
  if (typeof( continueProcessing) !== 'function') {
    throw "sTORAGEmANAGER.prototype.retrieveAll: " +
        "the 'continueProcessing' parameter is required!";
  }
  sTORAGEmANAGER.adapters[this.adapter.name].retrieveAll( mc, continueProcessing);
};
/**
 * Generic method for creating and "persisting" new model objects
 * @method
 * @param {object} mc  The model class concerned
 * @param {object} slots  The object creation slots
 * @param {function} continueProcessing  The continueProcessing function
 */
sTORAGEmANAGER.prototype.add = function (mc, slots, continueProcessing) {
  var newObj = null;
  if (!continueProcessing) {
    continueProcessing = function (obj, error) {
      if (error) {
        if (typeof(error) === 'string') console.log( error);
        else console.log( error.constructor.name +": "+ error.message);
      } else {
        console.log("New "+ obj.toString() +" saved.");
      }
    };
  } else if (typeof( continueProcessing) !== 'function') {
    throw "sTORAGEmANAGER.prototype.add: 'continueProcessing' must be a function!";
  }
  try {
    newObj = mc.create( slots);
    if (newObj) {
      sTORAGEmANAGER.adapters[this.adapter.name].add( mc,
          slots, newObj, continueProcessing);
    }
  } catch (e) {
    if (e instanceof ConstraintViolation) {
      console.log( e.constructor.name +": "+ e.message);
    } else console.log( e);
  }
};
/**
 * Generic method for updating model objects
 * @method
 * @param {object} mc  The model class concerned
 * @param {string|number} id  The object ID value
 * @param {object} slots  The object's update slots
 * @param {function} continueProcessing  The continueProcessing function
 */
sTORAGEmANAGER.prototype.update =
    function (mc, id, slots, continueProcessing) {
  var objectBeforeUpdate = null,
      properties = mc.properties, updatedProperties=[],
      noConstraintViolated=true, adapter = this.adapter.name;
  function afterRetrieveDo (objToUpdate, error) {
    if (error) {
      if (continueProcessing) {
        continueProcessing( null, "There is no "+ mc.name +
            " with "+ mc.standardIdAttr +" "+ id +" in the database!");
      } else {
        console.log("There is no "+ mc.name +" with "+
            mc.standardIdAttr +" "+ id +" in the database!");
      }
    } else {
      // this is a special case when the object comes as serialized as for
      // example from an XHR request. In this case we create the corresponding
      // model instance from the slots ( serialized object)
      if( typeof objToUpdate === "object" && !objToUpdate.type) {
        objToUpdate = mc.convertRec2Obj( objToUpdate);
      }
      objectBeforeUpdate = util.cloneObject( objToUpdate);
      try {
        Object.keys( slots).forEach( function (attr) {
          var oldVal = objToUpdate[attr],
              newVal = slots[attr];
          if (properties[attr] && !properties[attr].isStandardId) {
            if (properties[attr].maxCard === undefined ||
                properties[attr].maxCard === 1) {  // single-valued
              if (Number.isInteger( oldVal) && newVal !== "") {
                newVal = parseInt( newVal);
              } else if (typeof( oldVal)==="number" && newVal !== "") {
                newVal = parseFloat( newVal);
              } else if (oldVal===undefined && newVal==="") {
                newVal = undefined;
              }
              if (newVal !== oldVal) {
                updatedProperties.push( attr);
                objToUpdate.set( attr, newVal);
              }
            } else {   // multi-valued
              if (oldVal.length !== newVal.length ||
                  oldVal.some( function (vi,i) { return (vi !== newVal[i]);})) {
                objToUpdate.set(attr, newVal);
                updatedProperties.push(attr);
              }
            }
          }
        });
      } catch (e) {
        console.log( e.constructor.name +": "+ e.message);
        noConstraintViolated = false;
        // restore object to its state before updating
        objToUpdate = objectBeforeUpdate;
      }
      if (noConstraintViolated) {
        if (updatedProperties.length > 0) {
          console.log( "Properties " + updatedProperties.toString() +
              " modified for "+ mc.name +" "+ id);
          sTORAGEmANAGER.adapters[adapter].update( mc, id, slots,
              objToUpdate, continueProcessing);
        } else {
          console.log("No property value changed for "+ mc.name +" "+
              id +" !");
        }
      }
    }
  }
  // first check if object exists
  this.retrieve( mc, id, afterRetrieveDo);
};
/**
 * Generic method for deleting model objects
 * @method
 * @param {object} mc  The model class concerned
 * @param {string|number} id  The object ID value
 * @param {function} continueProcessing  The continueProcessing function
 */
sTORAGEmANAGER.prototype.destroy = function (mc, id, continueProcessing) {
  var adapter = this.adapter.name;
  this.retrieve( mc, id, function( object, err){
    if (object) {
      sTORAGEmANAGER.adapters[adapter].destroy( mc, id, continueProcessing);
    } else {
      err = "There is no "+ mc.name +" with "+ mc.standardIdAttr +
          " "+ id +" in the database!";
      console.log( err);
    }
  });
};
/**
 * Generic method for clearing the database table, or object store,
 * of a model class
 * @method
 */
sTORAGEmANAGER.prototype.clearData = function (mc, continueProcessing) {
  if (typeof confirm === "function") {
    if (confirm("Do you really want to delete all data?")) {
      sTORAGEmANAGER.adapters[this.adapter.name].clearData( mc, continueProcessing);
    }
  } else sTORAGEmANAGER.adapters[this.adapter.name].clearData( mc, continueProcessing);
};

sTORAGEmANAGER.adapters = {};

/**************************************************************************
 * Storage management methods for the "LocalStorage" adapter
 **************************************************************************/
sTORAGEmANAGER.adapters["LocalStorage"] = {
  //------------------------------------------------
  retrieve: function (mc, id, continueProcessing) {
  //------------------------------------------------
    //this.retrieveAll();             //TODO: Needed?
    continueProcessing( mc.instances[id]);
  },
  //------------------------------------------------
  retrieveAll: function (modelClass, continueProcessing) {
  //------------------------------------------------
    function retrieveAll (mc) {
      var key = "", keys = [], i = 0,
          tableString = "", table={},
          tableName = util.getTableName( mc.name);
      Object.keys( mc.properties).forEach( function (p) {
        var range = mc.properties[p].range;
        if (range instanceof mODELcLASS) retrieveAll( range);
      });
      try {
        if (localStorage[tableName]) {
          tableString = localStorage[tableName];
        }
      } catch (e) {
        console.log( "Error when reading from Local Storage\n" + e);
      }
      if (tableString) {
        table = JSON.parse( tableString);
        keys = Object.keys( table);
        console.log( keys.length + " " + mc.name + " records loaded.");
        for (i=0; i < keys.length; i++) {
          key = keys[i];
          mc.instances[key] = mc.convertRec2Obj( table[key]);
        }
      }
    }
    retrieveAll( modelClass);
    continueProcessing();
  },
  //------------------------------------------------
  add: function (mc, slots, newObj, continueProcessing) {
  //------------------------------------------------
    mc.instances[newObj[mc.standardIdAttr]] = newObj;
    this.saveAll( mc);
    //console.log( newObj.toString() + " created!");
    continueProcessing( newObj, null);  // no error
  },
  //------------------------------------------------
  /***** newObj includes validated update slots *****/
  update: function (mc, id, slots, newObj, continueProcessing) {
  //------------------------------------------------
    this.saveAll( mc);  //TODO: save only on leaving page or closing browser tab/window
  },
  //------------------------------------------------
  destroy: function (mc, id, continueProcessing) {
  //------------------------------------------------
    delete mc.instances[id];
    this.saveAll( mc);  //TODO: save only on app exit
  },
  //------------------------------------------------
  clearData: function (mc, continueProcessing) {
    //------------------------------------------------
    var tableName = util.getTableName( mc.name);
    mc.instances = {};
    try {
      localStorage[tableName] = JSON.stringify({});
      console.log("Table "+ tableName +" cleared.");
    } catch (e) {
      console.log("Error when writing to Local Storage\n" + e);
    }
  },
  //------------------------------------------------
  saveAll: function (mc) {
  //------------------------------------------------
    var id="", table={}, rec={}, obj=null, i=0,
        keys = Object.keys( mc.instances),
        tableName = util.getTableName( mc.name);
    // convert to a 'table' as a map of entity records
    for (i=0; i < keys.length; i++) {
      id = keys[i];
      obj = mc.instances[id];
      table[id] = obj.toRecord();
    }
    try {
      localStorage[tableName] = JSON.stringify( table);
      console.log( keys.length +" "+ mc.name +" records saved.");
    } catch (e) {
      console.log("Error when writing to Local Storage\n" + e);
    }
  }
};