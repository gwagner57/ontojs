// $appNS, $appNS.model, $appNS.view and $appNS.ctrl are fixed names
// and changing them may cause the application to run incorrect!
var $appNS = { model:{}, view:{}, ctrl:{}};
$appNS.ctrl = {
  // use this flag to enable/disable HTML5 form input auto-validation feature
  validateOnInput: false,
  // optional, used to define the app path (e.g., /nodeapps/myApp), used with proxied app paths
  appPath: "",
  // define the storage manager for this application
  storageManager: new sTORAGEmANAGER( 
    { name: "MariaDB", // required
      host: 'localhost', // optional host, defaults to localhost
      port: 3306, // optional DB engine port, defaults to 3306
      user: 'the-user-name', // required
      database: 'the-database-name', // required
      password: 'the-user-password' // required
    })
   /** additional/custom code comes here, e.g., the createTestData method, etcetera **/
};