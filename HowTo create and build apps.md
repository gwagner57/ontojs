________________________________________
Step 1: Install node dependency modules
----------------------------------------
First it is required to install all the Node modules required to 
use gulp, gulp utility modules and other libraries such as express
to be able to create and run the application. 
This step presumes that you already have installed NodeJS and npm.

Run the following command in the root folder (where the package.json file resides):

  npm install

Be patient while this may take some time to complete.
If errors appear during the installation of the modules, delete 
the auto-created node_modules folder and try again.

NOTE: the file package.json contains the definition of all the NodeJS 
       modules to be installed.

___________________________________________________
Step 2: Create the folder structure for a new application
---------------------------------------------------
From the root folder, where the gulpfile.js file resides, using 
your favourite command line tool (e.g., cmd on Windows, terminal on Linux) 
type the following command:

  gulp create-app --appfolder="./apps/" --appname="your-application-name"
 
This creates a new a folder named "your-application-name" within "./apps/" folder 
and creates the structure and files required as base for the Web Application.

NOTE:
   i) be sure that you have the appropriate rights to create files and folders
  ii) if --appfolder parameter is not provided, the root folder is used
 iii) the --appfolder parameter requires the trailing slash (the last slash after the path)
  iv) if --appname parameter is not provided, then 'myNewApp' is used by default
   v) the start.js file contains the express code, eventually you may want to change the listen port number there!
 
___________________________________________
Step 3: Build the application distribution
------------------------------------------- 
After writing the application code (e.g. creating the model classes, etc) the 
application distribution has to be created. To do this, execute the following 
command:

  gulp build --appfolder="./apps/your-application-folder-name/" --production

This creates a 'dist' sub-folder inside the --appfolder, prepare the required 
structure and merge the files for having an executable application.

NOTE:
   i) the --appfolder parameter requires the trailing slash (the last slash after the path)
  ii) the --production parameter uses uglify on the source code (skip it if you like)

____________________________
Step 4: Run the application
----------------------------
To run the application, navigate to the 'dist' sub-folder of your application using 
your favorite command line tool and execute the following command:
  
  node start.js
  
This is all, now use the browser to test your application.